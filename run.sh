#!/bin/sh
export MAVEN_OPTS="-Xmx512m"
mvn clean
mvn spring-boot:run -Denv=light -Dblockchain=mock -Dldap=mock
#mvn spring-boot:run -Denv=light -Dblockchain=ethereum -Dldap=mock

