# PARTAGE'ON - Collaborative Mentoring - REST Application
Purple Chain : Déploiement d’une BlockChain privée à Natixis Intégrée.
Collaborative Mentoring : Déploiement d’une application autonome permettant la publication des « ressources » partagées par les collaborateurs, et assurant la mise en relation.

## REST Application

### Objectifs
- Gestion des identités NATIXIS (Groupe BPCE) par le biais de la FED (Fédération d'identité) proposé par le Groupe


### Bénéfices de l’approche
- Développer l’expertise BlockChain au sein des DSI;
- Permettre à chaque Métier/DSI de développer leur propre politique de solutions collaboratives via les Smart Contracts (Petits programmes gérant les règles des transactions inter-personnelles) tout en partageant les bénéfices d’une infrastructure transversale (la gestion des portefeuilles et des « monnaies »).

