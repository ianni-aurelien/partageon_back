package com.natixis.partageon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
public class PartageonBackApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
	    // Allowed INSERT File with HSQLDB
        System.setProperty("textdb.allow_full_path", "true");
        SpringApplication.run(PartageonBackApplication.class, args);
	}
}
