package com.natixis.partageon.controller;

import com.natixis.partageon.exception.UserException;
import com.natixis.partageon.model.client.*;
import com.natixis.partageon.model.system.Theme;
import com.natixis.partageon.service.PartageService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author aurelien
 * @version 1.0
 * @apiNote CRUD shared system
 */

@RestController
@RequestMapping("annonces")
public class PartageController {
	
	private static final Logger logger = LoggerFactory.getLogger(PartageController.class);

	private final PartageService partageService;
    
    @Autowired
    public PartageController(PartageService partageService) {
        this.partageService = partageService;
    }
    
	@RequestMapping(value = "listUserAnnonces",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Success", response = WMiseEnRelation.class),
	})
	public @ResponseBody ResponseEntity<?> listUserAnnonces(
			@RequestParam("federationId") String fedUserId) {
		try {
            List<WAnnonce> annonceList = partageService.listAnnonces(fedUserId);
            return new ResponseEntity<>(annonceList, HttpStatus.OK);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<>(
			    new UserException("500", ex.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "listAnnonces",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Success", response = WAnnonce.class),
	})
	public @ResponseBody ResponseEntity<?> listAnnonces() {
		try {
            List<WAnnonce> annonceList = partageService.listAnnonces();
            return new ResponseEntity<>(annonceList, HttpStatus.OK);
        } catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<>(
			    new UserException("500", ex.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "createAnnonce",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Created", response = WAnnonce.class),
	})
	public @ResponseBody ResponseEntity<?> createAnnonce(@RequestBody WAnnonceCreation annonce) {
		try {
            WAnnonce annonceRes = partageService.createAnnonce(annonce);
            return new ResponseEntity<>(annonceRes, HttpStatus.CREATED);
        } catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<>(
			    new UserException("500", ex.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "lieAnnonce",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Success", response = WMiseEnRelation.class),
	})
	public @ResponseBody ResponseEntity<?> lieAnnonce(@RequestBody WAnnonceReponse request) {
		try {
            WMiseEnRelation miseEnRelation = partageService.lieAnnonce(request);
            return new ResponseEntity<>(miseEnRelation, HttpStatus.OK);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<>(
			    new UserException("500", ex.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "modifyAnnonce",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 202, message = "Accepted", response = WAnnonce.class),
	})
	public @ResponseBody ResponseEntity<?> modifyAnnonce(@RequestBody WAnnonceModification aAnnonce) {
		try {
            WAnnonce annonce = partageService.modifyAnnonce(aAnnonce);
            return new ResponseEntity<>(annonce, HttpStatus.ACCEPTED);
        } catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<>(
			    new UserException("500", ex.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "abandonneMiseEnRelation",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 202, message = "Abandon DONE", response = WMiseEnRelation.class),
	})
	public @ResponseBody ResponseEntity<?> abandonneMiseEnRelation(@RequestBody WMiseEnRelationAbandon mer) {
		try {
            partageService.abandonneMiseEnRelation(mer);
            return null;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<>(
			    new UserException("500", ex.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "archiveMiseEnRelation",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 202, message = "Archive DONE", response = WMiseEnRelation.class),
	})
	public @ResponseBody ResponseEntity<?> archiveMiseEnRelation(@RequestBody WMiseEnRelationArchivage mer) {
		try {
			partageService.archiveMiseEnRelation(mer);
			return null;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<>(
			    new UserException("500", ex.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "clotureAnnonce",
        method = RequestMethod.POST, produces =
        MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 202, message = "Close DONE", response = WAnnonce.class),
	})
	public @ResponseBody ResponseEntity<?> clotureAnnonce(@RequestBody WAnnonceCloture cloture) {
		try {
			partageService.clotureAnnonce(cloture);
			return null;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<>(
			    new UserException("500", ex.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "evalueMiseEnRelation",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 202, message = "Evaluation DONE", response = WMiseEnRelation.class),
	})
	public @ResponseBody ResponseEntity<?> evalueMiseEnRelation(@RequestBody WMiseEnRelationEvaluation evaluation) {
		try {
			partageService.evalueMiseEnRelation(evaluation);
			return null;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<>(
			    new UserException("500", ex.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "listThemes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Success", response = Theme.class),
	})
	public @ResponseBody ResponseEntity<?> listThemes() {
		try {
            List<Theme> themeList = partageService.listThemes();
            return new ResponseEntity<>(themeList, HttpStatus.OK);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<>(
			    new UserException("500", ex.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
