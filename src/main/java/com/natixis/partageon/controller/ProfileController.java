package com.natixis.partageon.controller;

import com.natixis.partageon.exception.*;
import com.natixis.partageon.model.client.*;
import com.natixis.partageon.model.system.Avatar;
import com.natixis.partageon.service.ProfileService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @author aurelien
 * @version 1.0
 * @apiNote resource to manage user auth & profile
 */

@RestController
@RequestMapping("user")
public class ProfileController {
    
    private static final Logger logger = LoggerFactory.getLogger(ProfileController.class);
    
    private final ProfileService profileService;
    
    private final DozerBeanMapper mapper;
    
    @Autowired
    public ProfileController(DozerBeanMapper mapper, ProfileService profileService) {
        this.mapper = mapper;
        this.profileService = profileService;
    }
    @RequestMapping(value = "getProfil",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK", response = User.class),
    })
    public @ResponseBody ResponseEntity<?> getUserProfil(
        @RequestParam("federationId") String fedUserId) {
        try {
            User user = profileService.getUserProfil(fedUserId);
            if (user != null && user.getNeedAuthentication()) {
                return new ResponseEntity<>(new UserException("401", "Authentication Expected"),
                    HttpStatus.UNAUTHORIZED);
            }
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
            return new ResponseEntity<>(new UserException("500", ex.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @RequestMapping(value = "authenticate",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK", response = User.class),
    })
    public @ResponseBody ResponseEntity<?> authenticate(@RequestBody NotAuthenticatedUser authUser) {
        try {
            User user = new User();
            mapper.map(authUser, user);
            user = profileService.authenticate(user.getFedUserId(), user.getPassword());
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (LdapException e) {
            logger.error(e.getMessage(),e);
            return new ResponseEntity<>(new UserException("401", e.getMessage()), HttpStatus.UNAUTHORIZED);
        } catch (TechnicalException e) {
            logger.error(e.getMessage(),e);
            return new ResponseEntity<>(new UserException("500", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @RequestMapping(value = "createOrModifyProfil",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK", response = User.class),
    })
    public @ResponseBody ResponseEntity<?> createOrModifyUserProfile(@RequestBody UserProfile userProfile) {
        try {
            User user = mapper.map(userProfile, User.class);
            profileService.createOrModifyUserProfile(user);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (UnknownUserException e) {
            logger.error(e.getMessage(),e);
            return new ResponseEntity<>(new UserException("500", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (AuthenticationRequiredException e) {
            logger.error(e.getMessage(),e);
            return new ResponseEntity<>(new UserException("401", e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }
    
    @RequestMapping(value = "avatars",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK", response = WAvatar.class),
    })
    public @ResponseBody ResponseEntity<?> listAvatars() {
        List<WAvatar> WAvatarList = profileService.listAvatars();
        return new ResponseEntity<>(WAvatarList, HttpStatus.OK);
    }
    
    @RequestMapping(value = "avatar",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Created", response = WAvatar.class),
    })
    public @ResponseBody ResponseEntity<?> setAvatar(@RequestParam("file") MultipartFile file) {
        try {
            WAvatar WAvatar = file_to_avatar(file);
            profileService.setAvatar(WAvatar);
            return new ResponseEntity<>(WAvatar, HttpStatus.CREATED);
        } catch (TechnicalException e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity<>(new UserException("500", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @RequestMapping(value = "avatar/{federationId}",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = {MediaType.MULTIPART_FORM_DATA_VALUE,MediaType.APPLICATION_JSON_VALUE})
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Created", response = WAvatar.class),
    })
    public @ResponseBody ResponseEntity<?> setAvatarByFederationId(
        @PathVariable String federationId,
        @RequestParam("file") MultipartFile file) {
        try {
            if(file != null) {
                WAvatar WAvatar = file_to_avatar(file);
                profileService.setAvatarToUserByFedId(federationId, WAvatar);
                HttpHeaders headers = new HttpHeaders();
                headers.setCacheControl(CacheControl.noCache().getHeaderValue());
                return new ResponseEntity<>(WAvatar, headers, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new UserException("401", "File not Found"), HttpStatus.BAD_REQUEST);
            }
        } catch (TechnicalException | UnknownUserException e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity<>(
                new UserException("500", e.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (AuthenticationRequiredException e) {
            logger.error(e.getMessage(),e);
            return new ResponseEntity<>(new UserException("401", e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }
    
    @RequestMapping(value = "avatar/{federationId}",
        method = RequestMethod.GET,
        produces = MediaType.IMAGE_PNG_VALUE)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = Byte.class),
    })
    public @ResponseBody ResponseEntity<?> getAvatarByUserId(@PathVariable String federationId) {
        try {
            WAvatar WAvatar = new WAvatar();
            Avatar avatar = profileService.getAvatarByUserFedId(federationId);
            WAvatar = mapper.map(avatar, WAvatar.class);
            final HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.IMAGE_PNG);
            return new ResponseEntity<>(WAvatar.getContent(), headers, HttpStatus.OK);
        } catch (UnknownUserException e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity<>(new UserException("500", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (AuthenticationRequiredException e) {
            logger.error(e.getMessage(),e);
            return new ResponseEntity<>(new UserException("401", e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }
    
    @RequestMapping(value = "getAvatar/{avatarName}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = WAvatar.class),
    })
    public @ResponseBody ResponseEntity<?> getAvatar(@PathVariable String avatarName) {
        WAvatar WAvatar = profileService.getAvatar(avatarName);
        return new ResponseEntity<>(WAvatar, HttpStatus.OK);
    }
    
    @RequestMapping(value = "listMediaAndSites",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = MediaAndSites.class),
    })
    public @ResponseBody ResponseEntity<?> mediaAndSites() {
        MediaAndSites mediaAndSiteList = profileService.listMediaAndSites();
        return new ResponseEntity<>(mediaAndSiteList, HttpStatus.OK);
    }
    
    private WAvatar file_to_avatar(MultipartFile file) throws TechnicalException {
        try {
            WAvatar WAvatar = new WAvatar();
            byte[] data = file.getBytes();
            WAvatar.setFilename(file.getOriginalFilename());
            WAvatar.setContent(data);
            return WAvatar;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new TechnicalException(e);
        }
    }
}
