package com.natixis.partageon.controller;

import com.natixis.partageon.config.ApplicationSecurityConfig;
import com.natixis.partageon.model.client.User;
import com.natixis.partageon.useridentity.FederationIdentiteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by ianniau on 07/03/2017.
 */
@RestController
@RequestMapping("saml")
@Profile("fednxs")
public class SAMLController {

	private static final Logger LOG = LoggerFactory.getLogger(ApplicationSecurityConfig.class);

	@Autowired
	private FederationIdentiteService federationIdentiteService;

	@RequestMapping("landing")
	public String getLanding(HttpServletRequest request) {
		LOG.debug("landing");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User principal = (User)auth.getPrincipal();
		String redirectUrl;
		try {
			federationIdentiteService.retrieveUserInfo(principal.getFedUserId(),"null");
			redirectUrl = request.getScheme() + "://gtbmut.dev.intranatixis.com/partageon/#!/monprofil?user="+principal.getFedUserId();
			return "redirect:" + redirectUrl;
		} catch (NamingException e) {
			redirectUrl = request.getScheme() + "://gtbmut.dev.intranatixis.com/partageon/404.html";
			return "redirect:" + redirectUrl;
		}
	}
}
