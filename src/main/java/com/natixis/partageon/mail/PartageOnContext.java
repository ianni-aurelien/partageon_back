package com.natixis.partageon.mail;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;

/**
 * @author chaigneauam
 *
 */
public class PartageOnContext implements Context {
	
	private VelocityContext ctxt;
	
	static {
		Velocity.init();
	}
	/**
	 * 
	 */
    private PartageOnContext() {
		ctxt = new VelocityContext();
	}

	/* (non-Javadoc)
	 * @see org.apache.velocity.context.Context#put(java.lang.String, java.lang.Object)
	 */
	@Override
	public Object put(String key, Object value) {
		return ctxt.put(key, value);
	}

	/* (non-Javadoc)
	 * @see org.apache.velocity.context.Context#get(java.lang.String)
	 */
	@Override
	public Object get(String key) {
        return ctxt.get(key);
	}

	/* (non-Javadoc)
	 * @see org.apache.velocity.context.Context#containsKey(java.lang.Object)
	 */
	@Override
	public boolean containsKey(Object key) {
		return ctxt.containsKey(key);
	}

	/* (non-Javadoc)
	 * @see org.apache.velocity.context.Context#getKeys()
	 */
	@Override
	public Object[] getKeys() {
		return ctxt.getKeys();
	}

	/* (non-Javadoc)
	 * @see org.apache.velocity.context.Context#remove(java.lang.Object)
	 */
	@Override
	public Object remove(Object key) {
		return ctxt.remove(key);
	}

	/**
	 * @param mailinfo
	 * @return
	 */
	public static PartageOnContext initContext(MailInfo mailinfo, boolean previsu) {
		PartageOnContext ctxt = new PartageOnContext();
		ctxt.put("user1", mailinfo.getU1().getU());
		ctxt.put("user2", mailinfo.getU2().getU());
		if(!previsu){
			ctxt.put("mail1", mailinfo.getU1().getMail());
			ctxt.put("mail2", mailinfo.getU2().getMail());
		} else{
			ctxt.put("mail1", mailinfo.getU1().getU().getPseudo());
			ctxt.put("mail2", mailinfo.getU2().getU().getPseudo());
		}
		// ctxt.put("pseudo1", mailinfo.getU1().getU().getPseudo());
		// ctxt.put("avatar1", mailinfo.getU1().getU().getUriAvatar());
		// ctxt.put("pseudo2", mailinfo.getU2().getU().getPseudo());
		// ctxt.put("avatar2", mailinfo.getU2().getU().getUriAvatar());
		ctxt.put("annonce", mailinfo.getBodyInfo().getAnnonce());
		ctxt.put("title", mailinfo.getBodyInfo().getTitle());
		ctxt.put("reponse", mailinfo.getBodyInfo().getCommentaire());
		// ctxt.put("mail1", mailinfo.getU1().getMail());
		// ctxt.put("mail2", mailinfo.getU2().getMail());
		return ctxt;
	}

}
