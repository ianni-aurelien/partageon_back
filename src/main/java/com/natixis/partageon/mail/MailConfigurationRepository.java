package com.natixis.partageon.mail;

import java.util.Map;

/**
 * @author chaigneauam
 *
 */
public class MailConfigurationRepository {

	private Map<String, EventMailConfiguration> maps;

	/**
	 * @return the maps
	 */
	public Map<String, EventMailConfiguration> getMaps() {
		return maps;
	}

	/**
	 * @param maps the maps to set
	 */
	public void setMaps(Map<String, EventMailConfiguration> maps) {
		this.maps = maps;
	}
	
}
