package com.natixis.partageon.mail;

import com.natixis.partageon.model.client.WMail;
import com.natixis.partageon.utils.FileUtils;
import lombok.Setter;
import org.apache.velocity.app.Velocity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.StringWriter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author chaigneauam
 *
 */
@Service
@Setter
public class MailComposerImpl implements MailComposer {

	private MailConfigurationRepository configuration;

	private MailSender sender;
    
    @Autowired
    public MailComposerImpl(MailSender sender, MailConfigurationRepository configuration) {
        this.sender = sender;
        this.configuration = configuration;
    }
    
    /**
	 * @param context 
	 * @param mailConf
	 * @return
	 */
	private String getMessageContent(PartageOnContext context, MailConfiguration mailConf) {
		String contentTemplate = FileUtils.getFileContent(mailConf.getBody());
        return valorise(context, contentTemplate);
	}

	/**
	 * @param context
	 * @param mail
	 * @return Message
	 * @throws Exception
	 */
	private Message composeMessage(PartageOnContext context, WMail mail)
			throws Exception {
		Session session = Session.getDefaultInstance(sender.getProperties());
		MimeMessage msg = new MimeMessage(session);
		msg.setText(mail.getContent(), "UTF8", "html");
		String emetteurStr = mail.getEmetteur();
		InternetAddress emetteur = new InternetAddress(emetteurStr);
		msg.addFrom(new Address[] { emetteur });
		{
			InternetAddress[] destinataires = new InternetAddress[mail
					.getDestinataires().size()];
			int cpt = 0;
			for (String destinataireStr : mail.getDestinataires()) {
				String destinataireV = valorise(context, destinataireStr);
				InternetAddress destinataire = new InternetAddress(
						destinataireV);
				destinataires[cpt++] = destinataire;
			}
			msg.addRecipients(RecipientType.TO, destinataires);
		}
		{
			InternetAddress[] copies = new InternetAddress[mail.getCopies()
					.size()];
			int cpt = 0;
			for (String copiesStr : mail.getCopies()) {
				String copieV = valorise(context, copiesStr);
				InternetAddress copie = new InternetAddress(copieV);
				copies[cpt++] = copie;
			}
			msg.addRecipients(RecipientType.CC, copies);
		}
		String subject = valorise(context, mail.getSubject());
		msg.setSubject(subject);
		return msg;
	}

	/**
	 * @param mailConf
	 * @param context
	 * @return WMail
	 * @throws Exception
	 */
	private WMail composeWMail(PartageOnContext context,
			MailConfiguration mailConf) throws Exception {
		WMail mail = new WMail();
		String content = getMessageContent(context, mailConf);
		mail.setContent(content);
		String emetteurStr = valorise(context, mailConf.getEmetteur());
		mail.setEmetteur(emetteurStr);
		List<String> dests = valorise(context, mailConf.getDestinatairesAsList());
		List<String> copies = valorise(context, mailConf.getCopiesAsList());
		mail.setDestinataires(dests);
		mail.setCopies(copies);
		String subject = valorise(context, mailConf.getSubject());
		mail.setSubject(subject);
		return mail;
	}

	/**
	 * @param context
	 * @param content
	 * @return String
	 */
	private String valorise(PartageOnContext context, String content) {
		StringWriter writer = new StringWriter();
		Velocity.evaluate(context, writer, "body1", content);
		return writer.toString();
	}
	
	/**
	 * @param context
	 * @param content
	 * @return List
	 */
	private List<String> valorise(PartageOnContext context, List<String> content) {
        return content.stream().map(str -> valorise(context, str)).collect(Collectors.toList());
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.mail.MailComposer#compose(com.natixis.partageon
	 * .mail.MailObject, com.natixis.partageon.mail.MailInfo)
	 */
	@Override
	public void compose(MailObject eventType, MailInfo mailinfo) {
		EventMailConfiguration conf = configuration.getMaps().get(
				eventType.toString());
		PartageOnContext context = PartageOnContext.initContext(mailinfo, false);
		try {
			MailConfiguration mailConf1 = conf.getConfiguration("1");
			if (mailConf1 != null) {
				WMail mail1 = composeWMail(context, mailConf1);
				Message msg1 = composeMessage(context, mail1);
				sender.send(msg1);
			}
			MailConfiguration mailConf2 = conf.getConfiguration("2");
			if (mailConf2 != null) {
				WMail mail2 = composeWMail(context, mailConf2);
				Message msg2 = composeMessage(context, mail2);
				sender.send(msg2);
			}
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.mail.MailComposer#preVisualisation(com.natixis.
	 * partageon.mail.MailObject, com.natixis.partageon.mail.MailInfo)
	 */
	@Override
	public WMail preVisualisation(MailObject eventType, MailInfo mailinfo) {
		EventMailConfiguration conf = configuration.getMaps().get(
				eventType.toString());
		PartageOnContext context = PartageOnContext.initContext(mailinfo, true);
		try {
			MailConfiguration mailConf1 = conf.getConfiguration("1");
			if (mailConf1 != null) {
				return composeWMail(context, mailConf1);
			}
			MailConfiguration mailConf2 = conf.getConfiguration("2");
			if (mailConf2 != null) {
				return composeWMail(context, mailConf2);
			}
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
		return null;
	}

}
