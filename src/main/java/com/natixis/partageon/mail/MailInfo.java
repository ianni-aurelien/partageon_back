package com.natixis.partageon.mail;


/**
 * @author chaigneauam
 *
 */
public class MailInfo {

	private UserMail u1;

	private UserMail u2;

	private BodyInfo partage;

	/**
	 * @return the u1
	 */
	public UserMail getU1() {
		return u1;
	}

	/**
	 * @param u1 the u1 to set
	 */
	public void setU1(UserMail u1) {
		this.u1 = u1;
	}

	/**
	 * @return the u2
	 */
	public UserMail getU2() {
		return u2;
	}

	/**
	 * @param u2 the u2 to set
	 */
	public void setU2(UserMail u2) {
		this.u2 = u2;
	}

	/**
	 * @return the partage
	 */
	public BodyInfo getBodyInfo() {
		return partage;
	}

	/**
	 * @param partage the partage to set
	 */
	public void setBodyInfo(BodyInfo partage) {
		this.partage = partage;
	}

}
