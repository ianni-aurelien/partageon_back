package com.natixis.partageon.mail;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author chaigneauam
 *
 */
@Setter
@Component
public class MailSenderImpl implements MailSender {

	@Value("${smtp.configuration}")
	private String smtp;

	private Properties props;
	
	private static class AsyncLaunch implements Runnable {
		private final Message msg;

		/**
		 * 
		 */
		public AsyncLaunch(Message msg) {
			this.msg = msg;
		}

		public void run() {
			try {
				Transport.send(msg);
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.mail.MailSender#send(javax.mail.Message)
	 */
	@Override
	public void send(Message msg) {
		String sendmail = getProperties().getProperty("mail");
		if (sendmail == null || sendmail.equalsIgnoreCase("true")||sendmail.equalsIgnoreCase("yes")) {
			AsyncLaunch r = new AsyncLaunch(msg);
			new Thread(r).start();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.mail.MailSender#getProperties()
	 */
	@Override
	public Properties getProperties() {
		if (props == null) {
			props = new Properties();
			InputStream inStream = MailSenderImpl.class.getClassLoader()
					.getResourceAsStream(smtp);
			try {
				props.load(inStream);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return props;
	}

}
