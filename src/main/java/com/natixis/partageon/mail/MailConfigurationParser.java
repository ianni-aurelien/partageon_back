package com.natixis.partageon.mail;

import org.apache.commons.digester3.Digester;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author chaigneauam
 *
 */
@Component
class MailConfigurationParser implements FactoryBean<MailConfigurationRepository>{

	private String configuration;
	
	/**
	 * @param configuration the configuration to set
	 */
	@Value("${mail.configuration}")
	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.FactoryBean#getObject()
	 */
	@Override
	public MailConfigurationRepository getObject() throws Exception {
		return MailConfigurationParser.parseConfiguration(configuration);
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.FactoryBean#getObjectType()
	 */
	@Override
	public Class<?> getObjectType() {
		return MailConfigurationRepository.class;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.FactoryBean#isSingleton()
	 */
	@Override
	public boolean isSingleton() {
		return true;
	}
	
	/**
	 * @param configuration
	 * @return MailConfigurationRepository
	 * @throws SAXException 
	 * @throws IOException 
	 */
	public static MailConfigurationRepository parseConfiguration(
			String configuration) throws IOException, SAXException {
		InputStream in=MailConfigurationParser.class.getClassLoader().getResourceAsStream(configuration);
		Digester d=new Digester();
		d.addObjectCreate("mail-configuration", HashMap.class);
		d.addCallMethod("mail-configuration/event", "put", 2);
		d.addCallParam("mail-configuration/event", 0, "type");
		d.addObjectCreate("mail-configuration/event", EventMailConfiguration.class);
		d.addSetProperties("mail-configuration/event", "type", "type");
		
		d.addCallMethod("mail-configuration/event/mail", "put", 2, new Class[]{String.class, MailConfiguration.class});
		d.addCallParam("mail-configuration/event/mail", 0, "type");
		
		d.addObjectCreate("mail-configuration/event/mail", MailConfiguration
				.class);
		
		d.addBeanPropertySetter("mail-configuration/event/mail/subject");
		d.addBeanPropertySetter("mail-configuration/event/mail/body");
		d.addBeanPropertySetter("mail-configuration/event/mail/destinataires");
		d.addBeanPropertySetter("mail-configuration/event/mail/copies");
		d.addBeanPropertySetter("mail-configuration/event/mail/emetteur");
		
		d.addCallParam("mail-configuration/event/mail", 1, true);
		
		
		d.addCallParam("mail-configuration/event", 1, true);
		
		
		@SuppressWarnings("unchecked")
		Map<String, EventMailConfiguration> map= d.parse(in);
		MailConfigurationRepository conf=new MailConfigurationRepository();
		conf.setMaps(map);
		return conf;
	}


}
