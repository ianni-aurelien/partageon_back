package com.natixis.partageon.mail;

import javax.mail.Message;
import java.util.Properties;

/**
 * @author chaigneauam
 *
 */
public interface MailSender {

	/**
	 * @param msg
	 */
	void send(Message msg);

	/**
	 * @return
	 */
	Properties getProperties();

}
