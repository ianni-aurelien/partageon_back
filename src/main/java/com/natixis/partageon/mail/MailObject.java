package com.natixis.partageon.mail;

/**
 * @author chaigneauam
 *
 */
public enum MailObject {
	REPONSE_DEMANDE, REPONSE_OFFRE, ABANDON_MISE_EN_RELATION

}
