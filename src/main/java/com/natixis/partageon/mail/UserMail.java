package com.natixis.partageon.mail;

import com.natixis.partageon.model.system.PreferencesUtilisateur;

/**
 * @author chaigneauam
 *
 */
public class UserMail {

	private PreferencesUtilisateur u;
	private String mail;

	/**
	 * @return the u
	 */
	public PreferencesUtilisateur getU() {
		return u;
	}

	/**
	 * @param u the u to set
	 */
	public void setU(PreferencesUtilisateur u) {
		this.u = u;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

}
