package com.natixis.partageon.mail;

import com.natixis.partageon.model.client.WMail;

/**
 * @author chaigneauam
 *
 */
public interface MailComposer {

	/**
	 * @param mailEvent
	 * @param mailinfo
	 * @throws Exception 
	 */
	void compose(MailObject mailEvent, MailInfo mailinfo);
	
	WMail preVisualisation(MailObject mailEvent, MailInfo mailinfo);

}
