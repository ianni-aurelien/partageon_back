package com.natixis.partageon.mail;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author chaigneauam
 *
 */
@Getter
public class EventMailConfiguration {

	private String type;
	
	private final Map<String,MailConfiguration> map;
	
	/**
	 * 
	 */
	public EventMailConfiguration() {
		map= new HashMap<>();
	}
	
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	public void put(String key, MailConfiguration conf){
		map.put(key, conf);
	}
	
	public MailConfiguration getConfiguration(String key){
		return map.get(key);
	}
}
