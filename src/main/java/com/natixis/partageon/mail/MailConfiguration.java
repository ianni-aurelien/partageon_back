package com.natixis.partageon.mail;

import java.util.Arrays;
import java.util.List;

/**
 * @author chaigneauam
 *
 */
public class MailConfiguration {
	
	private List<String> destinataires;
	
	private List<String> copies;
	
	private String emetteur;

	private String subject;
	
	private String body;

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * @return the destinataires
	 */
	public String getDestinataires() {
		if(destinataires!=null){
			 return destinataires.toString();
		} else{
			return null;
		}
	}

	public List<String> getDestinatairesAsList() {
		return destinataires;
	}
	
	/**
	 * @param destinataires the destinataires to set
	 */
	public void setDestinataires(String destinataires) {
		this.destinataires = Arrays.asList(destinataires.split(",|;"));
	}
	
	/**
	 * @return the destinataires
	 */
	public String getCopies() {
		if(copies!=null){
			 return copies.toString();
		} else{
			return null;
		}
	}

	public List<String> getCopiesAsList() {
		return copies;
	}
	
	/**
	 * @param copies the destinataires to set
	 */
	public void setCopies(String copies) {
		this.copies = Arrays.asList(copies.split(",|;"));
	}

	/**
	 * @return the emetteur
	 */
	public String getEmetteur() {
		return emetteur;
	}

	/**
	 * @param emetteur the emetteur to set
	 */
	public void setEmetteur(String emetteur) {
		this.emetteur = emetteur;
	}
	
}
