package com.natixis.partageon.mail;

import lombok.Getter;
import lombok.Setter;

/**
 * @author chaigneauam
 *
 */
@Getter
@Setter
public class BodyInfo {
	
//	private Long annonceId;

	private String annonce;
	
	private String commentaire;
	
	private String title;

}
