package com.natixis.partageon.config;

import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.List;

@Configuration
public class MapperConfig {
	
	@Bean(name = "mapper")
	public DozerBeanMapper dozerBean() {
		List<String> mappingFiles = Collections.singletonList(
            "dozer/partageon-mapper.xml"
        );
		DozerBeanMapper dozerBean = new DozerBeanMapper();
		dozerBean.setMappingFiles(mappingFiles);
		return dozerBean;
	}
}
