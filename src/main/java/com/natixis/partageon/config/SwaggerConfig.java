package com.natixis.partageon.config;

import com.fasterxml.classmate.TypeResolver;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;

import static springfox.documentation.schema.AlternateTypeRules.newRule;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

	@Value("${info.build.version}")
	private String buidVersion;

	private final TypeResolver typeResolver;
    
    @Autowired
    public SwaggerConfig(TypeResolver typeResolver) {
        this.typeResolver = typeResolver;
    }
    
    @Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.any())
				.paths(exclude())
				.build()
				.pathMapping("/")
				.directModelSubstitute(LocalDate.class, String.class)
				.genericModelSubstitutes(ResponseEntity.class)
				.alternateTypeRules(newRule(
						typeResolver.resolve(DeferredResult.class,
								typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
						typeResolver.resolve(WildcardType.class)))
				.useDefaultResponseMessages(false)
				.apiInfo(apiInfo());
	}

	/**
	 * Path to exclude.
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Predicate<String> exclude() {
		return Predicates.and(
				Predicates.not(PathSelectors.regex("/error")),
				Predicates.not(PathSelectors.regex("/health")),
				Predicates.not(PathSelectors.regex("/info.json")));
	}

	/**
	 * API Info.
	 * 
	 * @return
	 */
	private ApiInfo apiInfo() {
		// Contact contact = new Contact("Pierre LEGER", "http://oversimple.fr", "pierre@oversimple.fr");
		Contact contact = new Contact("", "", "");
        return new ApiInfo("PartageOn", "PartageOn REST API", buidVersion, null, contact, null, null);
	}
}
