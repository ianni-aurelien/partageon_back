package com.natixis.partageon.config;

import com.natixis.partageon.cache.EhCacheBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CachingConfig {
	
	@Bean
	public EhCacheBeanFactory ehCacheCacheManager() {
		EhCacheBeanFactory cmfb = new EhCacheBeanFactory();
		cmfb.setCacheFilename("ehcache.xml");
		return cmfb;
	}
}
