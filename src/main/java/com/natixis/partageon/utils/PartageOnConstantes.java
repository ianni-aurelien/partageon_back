package com.natixis.partageon.utils;

/**
 * 
 * 
 * Ensemble des constantes.
 * 
 * @author chaigneauam
 *
 */
public interface PartageOnConstantes {

	String AVATAR_DIR = "AVATAR_DIRECTORY";
	String PROPERTIES = "partageon.properties";

	String INITIATEUR = "initiateur";
	String SUIVEUR = "suiveur";
}
