package com.natixis.partageon.utils;

/**
 * @author chaigneauam
 *
 */
public interface PropertiesService {

	String loadProperty(String key, String file);
}
