package com.natixis.partageon.utils;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * 
 * Classe utilitaire d'accès au système de fichier.
 * 
 * @author chaigneauam
 *
 */
public class FileUtils {

	private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);
	
	/**
	 * @param path
	 * @param classpath
	 * @return content
	 * @throws UnsupportedEncodingException 
	 */
	public static String getFileContent(String path, boolean classpath, String charset) throws UnsupportedEncodingException {
		InputStream in=null;
		if(classpath){
			in=FileUtils.class.getClassLoader().getResourceAsStream(path);
		} else{
			try {
				in=new FileInputStream(path);
			} catch (FileNotFoundException e) {
				logger.error(e.getMessage(), e);
			}
		}
		if(in==null){
			return null;
		}
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		try {
			IOUtils.copy(in, baos);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		if(charset!=null){
			return baos.toString(charset);
		} else{
			return baos.toString();
		}
	}

	/**
	 * @param path
	 * @return content
	 */
	public static String getFileContent(String path) {
		try {
			return getFileContent(path, true, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}
}
