package com.natixis.partageon.utils.impl;

import com.natixis.partageon.utils.PropertiesService;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 
 * Outil d'accès aux properties.
 * 
 * 
 * @author chaigneauam
 *
 */
@Service
public class PropertiesServiceImpl implements PropertiesService {
	
	private final Map<String,Map<String,String>> maps= new HashMap<>();

	/* (non-Javadoc)
	 * @see com.natixis.partageon.utils.PropertiesService#loadProperty(java.lang.String, java.lang.String)
	 */
	@Override
	public String loadProperty(String key, String file) {
		if(!maps.containsKey(file)){
			InputStream in = PropertiesServiceImpl.class.getClassLoader().getResourceAsStream(file);
			Properties props=new Properties();
			try {
				props.load(in);
				Map<String,String> map= new HashMap<>();
				for(Object kobj:props.keySet()){
					String k=(String)kobj;
					map.put(k, props.getProperty(k));
				}
				maps.put(file, map);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return maps.get(file).get(key);
	}

}
