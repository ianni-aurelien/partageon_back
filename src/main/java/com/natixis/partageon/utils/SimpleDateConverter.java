package com.natixis.partageon.utils;

import org.dozer.CustomConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * Classe de cnversion des dates entre front (format jj/mm/yyyy) et la date du
 * modèle de données (java.util.Date)
 * 
 * 
 * @author chaigneauam
 *
 */
public class SimpleDateConverter implements CustomConverter {

	private static final Logger logger = LoggerFactory.getLogger(SimpleDateConverter.class);
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.dozer.CustomConverter#convert(java.lang.Object,
	 * java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@Override
	public Object convert(Object existingDestinationFieldValue,
			Object sourceFieldValue, Class<?> destinationClass,
			Class<?> sourceClass) {
		DateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		if (destinationClass.equals(String.class)) {
			if (sourceFieldValue == null) {
				return null;
			}
            return fmt.format((Date) sourceFieldValue);
		} else {
			if (sourceFieldValue == null) {
				return null;
			}
			try {
                return fmt.parse((String) sourceFieldValue);
			} catch (ParseException e) {
				logger.error(e.getMessage(), e);
				return null;
			}
		}
	}

}
