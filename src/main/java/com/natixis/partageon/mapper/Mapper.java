package com.natixis.partageon.mapper;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyDescriptor;
import java.util.Map;

/**
 * @author chaigneauam
 *
 */
public class Mapper {
	
	private static final Logger logger = LoggerFactory.getLogger(Mapper.class);

	/**
	 * @param map
	 * @param clazz
	 * @return
	 */
	public <T> T map(Map<String, Object> map, Class<T> clazz) {
		T obj = null;
		ConvertUtilsBean c=new ConvertUtilsBean();
		try {
			obj = clazz.newInstance();
			for (String k : map.keySet()) {
				try {
					PropertyDescriptor propDesc = PropertyUtils.getPropertyDescriptor(obj, k.toLowerCase());
					Class<?> type = propDesc.getPropertyType();
					PropertyUtils.setProperty(obj, k.toLowerCase(), c.convert(map.get(k), type));
				} catch (Exception exc) {
					logger.error("error setting property "+k+" on "+clazz+". exc: "+exc.toString());
//					exc.printStackTrace();
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return obj;
	}

	/**
	 * @param object
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> T convert(Object object, Class<T> clazz) {
		Object obj=ConvertUtils.convert(object, clazz);
		return (T) obj;
	}

}
