package com.natixis.partageon.exception;

/**
 * @author chaigneauam
 *
 */
public class UnknownUserException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4675532263566328683L;

	
	/**
	 * @param msg
	 */
	public UnknownUserException(String msg) {
		super(msg);
	}
}
