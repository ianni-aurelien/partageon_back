package com.natixis.partageon.exception;

public class EndPointException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5897852327742132458L;
	
	/**
	 * @param exc
	 */
	public EndPointException(Exception exc) {
		super(exc);
	}
	
	public EndPointException(String msg) {
		super(msg);
	}

}
