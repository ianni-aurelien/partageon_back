package com.natixis.partageon.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author chaigneauam
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserException {

	private String code;

	private String message;
}
