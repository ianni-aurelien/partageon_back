package com.natixis.partageon.exception;

/**
 * @author chaigneauam
 *
 */
public class AuthenticationRequiredException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7293943475226595023L;

	/**
	 * @param userId
	 */
	public AuthenticationRequiredException(String userId) {
		super(userId);
	}

}
