package com.natixis.partageon.exception;


/**
 * @author chaigneauam
 *
 */
public class TechnicalException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 966774828080437951L;

	/**
	 * @param exc
	 */
	public TechnicalException(Exception exc) {
		super(exc);
	}

}
