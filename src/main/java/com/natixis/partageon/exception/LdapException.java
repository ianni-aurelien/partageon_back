package com.natixis.partageon.exception;

public class LdapException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4202224265744262603L;

	/**
	 * @param exc
	 */
	public LdapException(Exception exc) {
		super(exc);
	}
}
