package com.natixis.partageon.model.client;

import com.natixis.partageon.model.system.Media;
import com.natixis.partageon.model.system.Site;

import java.util.List;

/**
 * @author chaigneauam
 *
 */
public class MediaAndSites {

	
	private List<Media> media;
	
	private List<Site> sites;

	/**
	 * @return the media
	 */
	public List<Media> getMedia() {
		return media;
	}

	/**
	 * @param media the media to set
	 */
	public void setMedia(List<Media> media) {
		this.media = media;
	}

	/**
	 * @return the sites
	 */
	public List<Site> getSites() {
		return sites;
	}

	/**
	 * @param sites the sites to set
	 */
	public void setSites(List<Site> sites) {
		this.sites = sites;
	}
	
}
