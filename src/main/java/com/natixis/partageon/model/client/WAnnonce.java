package com.natixis.partageon.model.client;

import com.natixis.partageon.model.system.AnnonceType;
import com.natixis.partageon.model.system.StatutAnnonce;
import com.natixis.partageon.model.system.Theme;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author chaigneauam
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class WAnnonce {
	private Long annonceId;
	private String title;
	private User initiateur;
	private AnnonceType nature;
	private String commentaire;
	private String postDate;
	private WUrgence urgence;
	private List<Theme> themes;
	private StatutAnnonce statut;
	private Integer nombrePartages;
	private Double moyenneEvaluations;
}
