package com.natixis.partageon.model.client;

import com.natixis.partageon.model.system.Media;
import com.natixis.partageon.model.system.Site;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserProfile {

	private String fedUserId;

	private String pseudo;

	private String telephone;

	private String mobile;

	private String site;

	private Dispo indispo;

	private String commentaire;

	private String avatar;
    
	private Boolean anonyme;

	private List<Site> sites;

	private List<Media> media;
}
