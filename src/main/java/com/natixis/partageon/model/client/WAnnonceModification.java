package com.natixis.partageon.model.client;

import com.natixis.partageon.model.system.Theme;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author chaigneauam
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class WAnnonceModification {
	private String fedUserId;
	private Long annonceId;
	private String commentaire;
	private WUrgence urgence;
	private List<Theme> themes;
}
