package com.natixis.partageon.model.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author chaigneauam
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class WMailInfo {
	private Long annonceId;
	private String fedUserid;
	private String annonce;
	private String commentaireReponse;
}
