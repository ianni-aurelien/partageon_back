package com.natixis.partageon.model.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author chaigneauam
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class WMiseEnRelation extends WAnnonce {
	private Long miseEnRelationId;
	private User suiveur;
	private String matchdate;
	private Integer evaluation;
	private String commentaireEvaluation;
	private String miseEnRelationStatut;
	private Integer pendingDeeps;
	private Integer wonDeeps;
}
