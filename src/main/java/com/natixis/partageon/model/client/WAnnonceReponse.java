package com.natixis.partageon.model.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author chaigneauam
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class WAnnonceReponse {
	private Long annonceid;
	private String fedUserid;
	private String commentaire;
}
