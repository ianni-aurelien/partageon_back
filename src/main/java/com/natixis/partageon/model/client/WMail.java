package com.natixis.partageon.model.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author chaigneauam
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class WMail {
	private String emetteur;
	private List<String> destinataires;
	private List<String> copies;
	private String subject;
	private String content;
}
