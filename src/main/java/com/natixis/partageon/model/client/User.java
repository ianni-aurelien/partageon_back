package com.natixis.partageon.model.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.natixis.partageon.model.system.Deeps;
import com.natixis.partageon.model.system.Media;
import com.natixis.partageon.model.system.Site;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Utilisateur vu par le front.
 * 
 * 
 * @author chaigneauam
 *
 */
@Getter
@Setter
@ToString
public class User {

	private String fedUserId;

	@JsonIgnore
	private String password;

	private String pseudo;

	private String nom;

	private String prenom;

	private String telephone;

	private String mobile;

	private String site;

	private Dispo indispo;

	private Deeps deeps;

	private String commentaire;

	private String avatar;
	
	private WAvatar avatar_img;

	private Boolean anonyme;

	private List<Site> sites;

	private List<Media> media;

	private String email;

	private Boolean needAuthentication;

	private Boolean unknownFromDB;

	/**
	 * @return the sites
	 */
	public List<Site> getSites() {
		if (sites == null) {
			sites = new ArrayList<>();
		}
		return sites;
	}

	/**
	 * @return the media
	 */
	public List<Media> getMedia() {
		if (media == null) {
			media = new ArrayList<>();
		}
		return media;
	}

}
