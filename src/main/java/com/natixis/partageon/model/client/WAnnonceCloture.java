package com.natixis.partageon.model.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author chaigneauam
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class WAnnonceCloture {
	private String fedUserId;
	private Long annonceId;
}
