package com.natixis.partageon.model.system;

import lombok.Getter;
import lombok.Setter;

/**
 * @author chaigneauam
 *
 */
@Getter
@Setter
public class Media {
    
    private Long id;
    private String media;
}
