package com.natixis.partageon.model.system;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;

@SuppressWarnings("serial")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction implements Serializable {
    
    // Transaction
    private String signatureHash;
    private String unsignedTransactionBytes;
    private TransactionJson transactionJSON;
    private Boolean broadcasted;
    private Number requestProcessingTime;
    private String transactionBytes;
    private String fullHash;
    private String transaction;
}
