package com.natixis.partageon.model.system;

import lombok.*;

/**
 * Created by ianniau on 13/04/2017.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserSmartContractDetail {
    
    Long smartID;
    String privKey;
    String name;
    String abi;
    String address;
    
}
