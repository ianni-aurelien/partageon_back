package com.natixis.partageon.model.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author chaigneauam
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class Theme {
    private Long id;
    private String name;
    private boolean isSystem;
    
    public boolean getIsSystem() {
        return isSystem;
    }
    
    public void setIsSystem(boolean _isSystem) {
        isSystem = _isSystem;
    }
}
