package com.natixis.partageon.model.system;

/**
 * @author chaigneauam
 *
 */
public enum StatutAnnonce {
    DISPONIBLE(0), PARTAGE(2), CLOTURE(1), EVALUE(3), ABANDONNE(4), ARCHIVE(5), DESACTIVEE(6);
    
    private final Integer v;
    
    /**
     *
     */
    StatutAnnonce(Integer i) {
        this.v=i;
    }
    
    
    
    /**
     * @return the v
     */
    public Integer getValue() {
        return v;
    }
    
    
    
    /**
     *
     */
    static public StatutAnnonce valueOf(Integer i) {
        if(i==null){
            return null;
        }
        switch(i){
            case 0: return StatutAnnonce.DISPONIBLE;
            case 2: return StatutAnnonce.PARTAGE;
            case 1: return StatutAnnonce.CLOTURE;
            case 3: return StatutAnnonce.EVALUE;
            case 4: return StatutAnnonce.ABANDONNE;
            case 5: return StatutAnnonce.ARCHIVE;
            case 6: return StatutAnnonce.DESACTIVEE;
            default: return StatutAnnonce.DISPONIBLE;
        }
    }
}
