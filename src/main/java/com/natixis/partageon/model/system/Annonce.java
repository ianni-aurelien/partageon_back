package com.natixis.partageon.model.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author chaigneauam
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class Annonce {
    private Long annonceid;
    private String title;
    private AnnonceType type;
    private PreferencesUtilisateur initiateur;
    private String annonce;
    private Date postDt;
    private StatutAnnonce statut;
    private Urgence urgence;
    private List<Theme> themes;
    private Integer nombrePartages;
    private Double moyenneEvaluations;
    /**
     * @return the themes
     */
    public List<Theme> getThemes() {
        if(themes==null){
            themes= new ArrayList<>();
        }
        return themes;
    }
}
