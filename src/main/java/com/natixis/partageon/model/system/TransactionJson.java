package com.natixis.partageon.model.system;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

/**
 * Created by aurelien on 25/11/16.
 */
// Transaction JSON
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
class TransactionJson {
    
    private String transactionJSON;
    private String senderPublicKey;
    private String signature;
    private String feeNQT;
    private String type;
    private String fullHash;
    private String version;
    private String ecBlockId;
    private String signatureHash;
    private String senderRS;
    private String subtype;
    private String amountNQT;
    private String sender;
    private String recipientRS;
    private String recipient;
    private String ecBlockHeight;
    private String deadline;
    private String transaction;
    private String timestamp;
    private String height;
}
