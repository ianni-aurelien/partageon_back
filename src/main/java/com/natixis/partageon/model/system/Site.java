package com.natixis.partageon.model.system;

import lombok.Getter;
import lombok.Setter;

/**
 * @author chaigneauam
 *
 */
@Getter
@Setter
public class Site {
    
    private Long id;
    private String site;
    private String region;
}
