package com.natixis.partageon.model.system;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name="gamification")
@XmlAccessorType(XmlAccessType.FIELD)
public class GamificationRules {
    
    @XmlElement(name="event")
    private List<Event> summaryElementList;
    
    public List<Event> getSummaryElementList() {
        return summaryElementList;
    }
}
