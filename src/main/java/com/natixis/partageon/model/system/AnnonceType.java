package com.natixis.partageon.model.system;

/**
 * @author chaigneauam
 *
 */
public enum AnnonceType {
    
    OFFRE, DEMANDE
}
