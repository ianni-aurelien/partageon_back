package com.natixis.partageon.model.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * @author chaigneauam
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class AnnonceReponse {
    private Long annonceid;
    private Long userid;
    private Date matchdate;
    private String commentaire;
}
