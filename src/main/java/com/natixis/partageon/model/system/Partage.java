package com.natixis.partageon.model.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * @author chaigneauam
 *
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Partage extends Annonce {
	private Long partageid;
	private PreferencesUtilisateur suiveur;
	private Date matchDt;
	private Integer evaluation;
	private String commentaireEvaluation;
	private String commentaireAbandon;
	private Long lacheurId;
	private String commentaireReponse;
	private StatutAnnonce statutmer;
}
