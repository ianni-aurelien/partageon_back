package com.natixis.partageon.model.system;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;

/**
 * Created by aurelien on 04/07/17.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserIdentity {
    
    private BigInteger id;
    private String feduserid;
    private String pubkey;
    private String privkey;
}
