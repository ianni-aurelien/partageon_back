package com.natixis.partageon.model.system;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author chaigneauam
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class KeyPair {
    
    private String privKey;
    private String pubKey;
}
