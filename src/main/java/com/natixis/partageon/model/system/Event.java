package com.natixis.partageon.model.system;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Event {
    
    @XmlAttribute
    public String id;
    
    @XmlElement
    public InitiateurDeep initiateur;
    
    @XmlElement
    public SuiveurDeep suiveur;
}
