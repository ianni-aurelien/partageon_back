package com.natixis.partageon.model.system;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * Informations de l'utilisateur récupéré du service de fédération des identités.
 * @author chaigneauam
 *
 */
@Getter
@Setter
public class UtilisateurFederationIdentite {
    
    private String nom;
    private String prenom;
    private String site;
    private String email;
}
