package com.natixis.partageon.model.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * @author chaigneauam
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class Urgence {
    private Boolean urgence;
    private Date urgencedate;
}
