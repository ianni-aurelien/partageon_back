package com.natixis.partageon.model.system;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author chaigneauam
 * @implNote Les informations du profil utilisateur modifiables
 */
@ToString
@Getter
@Setter
public class PreferencesUtilisateur {

	/**
	 * identifiant de l'utilisateur en base.
	 */
	private Long userid;
	
	private String telephone;
	
	private String mobile;
	
	/**
	 * identifiant de l'utilisateur dans la fédération des identités.
	 */
	private String feduserid;
	
	private String pseudo;
	
	private Date indispoDebut;
	
	private Date indispoFin;
	
	private String commentaire;
	
	private List<Site> sitesAccessibles;
	
	private List<Media> mediaPrivilegies;
	
	private Boolean anonyme;
	
	private String uriAvatar;
	
	private Avatar avatar;

	/**
	 * @return the userid
	 */
	public Long getUserid() {
		return userid;
	}

	/**
	 * @param userid the userid to set
	 */
	public void setUserid(Long userid) {
		this.userid = userid;
	}

	/**
	 * @return the pseudo
	 */
	public String getPseudo() {
		return pseudo;
	}

	/**
	 * @param pseudo the pseudo to set
	 */
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	/**
	 * @return the indispoDebut
	 */
	public Date getIndispoDebut() {
		return indispoDebut;
	}

	/**
	 * @param indispoDebut the indispoDebut to set
	 */
	public void setIndispoDebut(Date indispoDebut) {
		this.indispoDebut = indispoDebut;
	}

	/**
	 * @return the indispoFin
	 */
	public Date getIndispoFin() {
		return indispoFin;
	}

	/**
	 * @param indispoFin the indispoFin to set
	 */
	public void setIndispoFin(Date indispoFin) {
		this.indispoFin = indispoFin;
	}

	/**
	 * @return the commentaire
	 */
	public String getCommentaire() {
		return commentaire;
	}

	/**
	 * @param commentaire the commentaire to set
	 */
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	/**
	 * @return the sitesAccessibles
	 */
	public List<Site> getSitesAccessibles() {
		if(sitesAccessibles==null){
			sitesAccessibles= new ArrayList<>();
		}
		return sitesAccessibles;
	}

	/**
	 * @param sitesAccessibles the sitesAccessibles to set
	 */
	public void setSitesAccessibles(List<Site> sitesAccessibles) {
		this.sitesAccessibles = sitesAccessibles;
	}

	/**
	 * @return the mediaPrivilegies
	 */
	public List<Media> getMediaPrivilegies() {
		if(mediaPrivilegies==null){
			mediaPrivilegies= new ArrayList<>();
		}
		return mediaPrivilegies;
	}

	/**
	 * @param mediaPrivilegies the mediaPrivilegies to set
	 */
	public void setMediaPrivilegies(List<Media> mediaPrivilegies) {
		this.mediaPrivilegies = mediaPrivilegies;
	}

	/**
	 * @return the anonyme
	 */
	public Boolean getAnonyme() {
		return anonyme;
	}

	/**
	 * @param anonyme the anonyme to set
	 */
	public void setAnonyme(Boolean anonyme) {
		this.anonyme = anonyme;
	}

	/**
	 * @return the urlAvatar
	 */
	public String getUriAvatar() {
		return uriAvatar;
	}

	/**
	 * @param uriAvatar the urlAvatar to set
	 */
	public void setUriAvatar(String uriAvatar) {
		this.uriAvatar = uriAvatar;
	}

	/**
	 * @return the feduserid
	 */
	public String getFeduserid() {
		return feduserid;
	}

	/**
	 * @param feduserid the feduserid to set
	 */
	public void setFeduserid(String feduserid) {
		this.feduserid = feduserid;
	}

	/**
	 * @return the telephone
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * @param telephone the telephone to set
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
}
