package com.natixis.partageon.model.system;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Account {
    
    private String account;
    private String accountRS;
    private String publicKey;
    private String name;
    private String description;
    
    private String unconfirmedBalanceNQT;
    private String guaranteedBalanceNQT;
    private int effectiveBalanceNXT;
    private String forgedBalanceNQT;
    private String balanceNQT;
    private String lastBlock;
    private int requestProcessingTime;
}
