package com.natixis.partageon.model.system;

import lombok.*;

/**
 * Created by aurelien on 01/07/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Avatar {
    
    private Long img_id;
    private String img_title;
    private byte[] img_data;
}
