package com.natixis.partageon.model.system;

import lombok.*;

/**
 * @author chaigneauam
 *
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Deeps {
    
    private Long total;
    
    private Long monthly;
}
