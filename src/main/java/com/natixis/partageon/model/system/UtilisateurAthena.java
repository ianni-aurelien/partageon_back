package com.natixis.partageon.model.system;

import lombok.Getter;
import lombok.Setter;

/**
 * Contient les informations utilisateur contenues dans le ldap Athena.
 * Ces données sont soumises au changement dès lors que les informations
 * contenues dans Athena seront connues, ce qui n'est pas le cas en date du
 * 17/11/2016.
 * @author chaigneauam
 */
@Getter
@Setter
public class UtilisateurAthena {
    
    /**
     * identifiant de l'utilisateur chez ATHENA.
     */
    private String id;
    private String prenom;
    private String nom;
    private String email;
    private String telephone;
    private String site;
}
