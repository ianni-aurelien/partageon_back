package com.natixis.partageon.model.smartcontract;

import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.Future;

/**
 * <p>Auto generated code.<br>
 * <strong>Do not modify!</strong><br>
 * Please use {@link org.web3j.codegen.SolidityFunctionWrapperGenerator} to update.
 *
 * <p>Generated with web3j version 2.1.0.
 */
public final class Partageon extends Contract {
    private static final String BINARY = "6060604052341561000c57fe5b5b60008054600160a060020a03191633600160a060020a03161790555b5b61016d806100396000396000f3006060604052361561003b5763ffffffff60e060020a60003504166318976e818114610051578063f8f786451461007f578063f96b8774146100a0575b341561004357fe5b61004f5b60006000fd5b565b005b341561005957fe5b61006d600160a060020a03600435166100ce565b60408051918252519081900360200190f35b341561008757fe5b61004f600160a060020a03600435166024356100e0565b005b34156100a857fe5b61006d600160a060020a0360043516610122565b60408051918252519081900360200190f35b60016020526000908152604090205481565b600054600160a060020a0390811690331681146100fd5760006000fd5b600160a060020a03831660009081526001602052604090208054830190555b5b505050565b600160a060020a0381166000908152600160205260409020545b9190505600a165627a7a7230582090df9c935991c214a305976b946aed88efb7c89c56ded015fe3ffade71ee1a260029";
    
    private Partageon(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }
    
    private Partageon(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }
    
    public Future<Uint256> deeps(Address param0) {
        Function function = new Function("deeps",
            Arrays.<Type>asList(param0),
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeCallSingleValueReturnAsync(function);
    }
    
    public Future<TransactionReceipt> addDeep(Address to, Uint256 amount) {
        Function function = new Function("addDeep", Arrays.<Type>asList(to, amount), Collections.<TypeReference<?>>emptyList());
        return executeTransactionAsync(function);
    }
    
    public Future<Uint256> getDeep(Address belongTo) {
        Function function = new Function("getDeep",
            Arrays.<Type>asList(belongTo),
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeCallSingleValueReturnAsync(function);
    }
    
    public static Future<Partageon> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit, BigInteger initialValue) {
        return deployAsync(Partageon.class, web3j, credentials, gasPrice, gasLimit, BINARY, "", initialValue);
    }
    
    public static Future<Partageon> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit, BigInteger initialValue) {
        return deployAsync(Partageon.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "", initialValue);
    }
    
    public static Partageon load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new Partageon(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }
    
    public static Partageon load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new Partageon(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }
}
