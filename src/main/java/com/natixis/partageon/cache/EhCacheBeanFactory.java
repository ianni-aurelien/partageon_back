package com.natixis.partageon.cache;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.ConfigurationFactory;
import org.springframework.beans.factory.FactoryBean;

import java.io.InputStream;

/**
 * @author chaigneauam
 *
 */
public class EhCacheBeanFactory implements FactoryBean<CacheManager> {

	private String cacheFilename;

	/**
	 * @param cacheFilename
	 *            the cacheFilename to set
	 */
	public void setCacheFilename(String cacheFilename) {
		this.cacheFilename = cacheFilename;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.beans.factory.FactoryBean#getObject()
	 */
	@Override
	public CacheManager getObject() throws Exception {
		InputStream in = EhCacheBeanFactory.class.getClassLoader().getResourceAsStream(
				cacheFilename);
		Configuration conf = ConfigurationFactory.parseConfiguration(in);
		conf.setName("ehcache-configuration-"+System.currentTimeMillis());
		CacheManager cacheMgr = new CacheManager(conf);
		return cacheMgr;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.beans.factory.FactoryBean#getObjectType()
	 */
	@Override
	public Class<?> getObjectType() {
		return CacheManager.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.beans.factory.FactoryBean#isSingleton()
	 */
	@Override
	public boolean isSingleton() {
		return true;
	}

}
