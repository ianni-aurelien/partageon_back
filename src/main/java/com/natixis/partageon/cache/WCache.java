package com.natixis.partageon.cache;

import net.sf.ehcache.Element;

/**
 * @author chaigneauam
 *
 */
public interface WCache {

	/**
	 * @param hash
	 * @return
	 */
	Element get(String hash);

	/**
	 * @param element
	 */
	void put(Element element);

}
