package com.natixis.partageon.cache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import org.springframework.stereotype.Repository;

/**
 * @author chaigneauam
 *
 */
@Repository
public class WCacheImpl implements WCache {
	
	private Cache cache;
	
	/**
	 * @param cache the cache to set
	 */
	public void setCache(Cache cache) {
		this.cache = cache;
	}

	/* (non-Javadoc)
	 * @see com.natixis.partageon.cache.WCache#get(java.lang.String)
	 */
	@Override
	public Element get(String hash) {
		return cache.get(hash);
	}

	/* (non-Javadoc)
	 * @see com.natixis.partageon.cache.WCache#put(net.sf.ehcache.Element)
	 */
	@Override
	public void put(Element element) {
		cache.put(element);
	}

}
