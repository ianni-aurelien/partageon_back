package com.natixis.partageon.cache;

import lombok.Setter;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author chaigneauam
 *
 */
@Repository
@Setter
public class WCacheManagerImpl implements WCacheManager {

    @Autowired
	private CacheManager cacheMgr;
 
	/* (non-Javadoc)
	 * @see com.natixis.partageon.cache.WCacheManager#getCache(java.lang.String)
	 */
	@Override
	public WCache getCache(String cacheName) {
		Cache cache = cacheMgr.getCache(cacheName);
		WCacheImpl wcache=new WCacheImpl();
		wcache.setCache(cache);
		return wcache;
	}
}
