package com.natixis.partageon.cache;


/**
 * @author chaigneauam
 *
 */
public interface WCacheManager {

	/**
	 * @param cacheName
	 * @return
	 */
	WCache getCache(String cacheName);
	
}
