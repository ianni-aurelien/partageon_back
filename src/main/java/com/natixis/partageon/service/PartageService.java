package com.natixis.partageon.service;

import com.natixis.partageon.exception.UnknownUserException;
import com.natixis.partageon.model.client.*;
import com.natixis.partageon.model.system.Theme;

import java.util.List;

/**
 * 
 * Interface d'accès aux services liés aux partages (annonces et mise en relation).
 * 
 * @author chaigneauam
 *
 */
public interface PartageService {

	/**
	 * Liste annonces et mises en relation relatives à l'utilisateur.
	 * 
	 * @param federationId
	 * @return
	 */
	List<WAnnonce> listAnnonces(String federationId) throws UnknownUserException;

	/**
	 * Liste les annonces (hors mises en relation auxquelles l'utlisateur appartient) auxquelles il appartient.
	 * 
	 * @return
	 */
	List<WAnnonce> listAnnonces();

	/**
	 * Créé ou modifie (duplique) une annonce.
	 * 
	 * @param annonce
	 * @return
	 */
	WAnnonce createAnnonce(WAnnonceCreation annonce) throws UnknownUserException;

	/**
	 * S'attache à une annonce.
	 * 
	 * @param reponse
	 * @return
	 * @throws UnknownUserException 
	 */
	WMiseEnRelation lieAnnonce(WAnnonceReponse reponse) throws UnknownUserException;

	/**
	 * Prévisualisation Mail d'attache à une annonce.
	 * 
	 * @param reponse
	 * @return
	 * @throws UnknownUserException 
	 */
	WMail visuMailLieAnnonce(WMailInfo reponse) throws UnknownUserException;
	
	/**
	 * 
	 * @param annonce
	 * @return
	 * @throws UnknownUserException
	 */
	WAnnonce modifyAnnonce(WAnnonceModification annonce) throws UnknownUserException;

	/**
	 * 
	 * Clôture une annonce.
	 * 
	 * @param annonce
	 */
	void clotureAnnonce(WAnnonceCloture annonce);
	
	/**
	 * Abandon d'une mise en relation.
	 * 
	 * @param mer
	 */
	void abandonneMiseEnRelation(WMiseEnRelationAbandon mer);

	/**
	 * Archivage d'une mise en relation.
	 * 
	 * @param mer
	 */
	void archiveMiseEnRelation(WMiseEnRelationArchivage mer);

	/**
	 * Evaluation d'une mise en relation.
	 * 
	 * @param evaluation
	 */
	void evalueMiseEnRelation(WMiseEnRelationEvaluation evaluation);

	/**
	 * Retourne la liste des thèmes.
	 * 
	 * @return
	 */
	List<Theme> listThemes();
}
