package com.natixis.partageon.service;

import com.natixis.partageon.exception.AuthenticationRequiredException;
import com.natixis.partageon.exception.LdapException;
import com.natixis.partageon.exception.TechnicalException;
import com.natixis.partageon.exception.UnknownUserException;
import com.natixis.partageon.model.client.WAvatar;
import com.natixis.partageon.model.client.MediaAndSites;
import com.natixis.partageon.model.client.User;
import com.natixis.partageon.model.system.Avatar;

import java.util.List;

/**
 * interface de service exposée au frontal.
 *
 * @author chaigneauam
 *
 */
public interface ProfileService {
    
    /**
     * @param fedUserId le token issu de la fédération des identités.
     * @return les informations liées à l'utlisateur
     */
    User getUserProfil(String fedUserId);
    
    /**
     * @param user
     * @throws UnknownUserException si inconnu de la FED
     * @throws AuthenticationRequiredException
     */
    void createOrModifyUserProfile(User user) throws UnknownUserException, AuthenticationRequiredException;
    
    List<WAvatar> listAvatars();
    
    /**
     *
     * @param avatarId
     * @return
     */
    WAvatar getAvatar(String avatarId);
    
    /**
     *
     * @param WAvatar
     * @return
     */
    Long setAvatar(WAvatar WAvatar);
    
    /**
     *
     * @param WAvatar
     */
    void setAvatarToUserByFedId(String fedId, WAvatar WAvatar) throws AuthenticationRequiredException, UnknownUserException;
    
    MediaAndSites listMediaAndSites();
    
    Avatar getAvatarByUserFedId(String fedId) throws AuthenticationRequiredException, UnknownUserException;
    
    /**
     * @param user
     * @param password
     * @return User
     * @throws TechnicalException
     * @throws LdapException
     */
    User authenticate(String user, String password) throws TechnicalException, LdapException;
    
}
