package com.natixis.partageon.service.impl;

import com.natixis.partageon.service.DateSource;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @author chaigneauam
 *
 */
@Repository
public class DateSourceImpl implements DateSource {

	/* (non-Javadoc)
	 * @see com.natixis.partageon.service.DateSource#getDate()
	 */
	@Override
	public Date getDate() {
		return new Date();
	}

}
