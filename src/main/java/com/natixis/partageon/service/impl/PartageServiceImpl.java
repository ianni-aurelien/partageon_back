package com.natixis.partageon.service.impl;

import com.natixis.partageon.blockchain.BlockChainBusiness;
import com.natixis.partageon.blockchain.GamificationEvent;
import com.natixis.partageon.dao.PartageDAO;
import com.natixis.partageon.dao.ProfileDAO;
import com.natixis.partageon.exception.UnknownUserException;
import com.natixis.partageon.mail.*;
import com.natixis.partageon.model.client.*;
import com.natixis.partageon.model.system.*;
import com.natixis.partageon.service.DateSource;
import com.natixis.partageon.service.PartageService;
import lombok.Setter;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author chaigneauam
 *
 */
@Setter
@Component
public class PartageServiceImpl implements PartageService {

	private DozerBeanMapper mapper;

	private PartageDAO partageDao;

	private ProfileDAO profileDao;

	private BlockChainBusiness blockchain;

	private MailComposer mailComposer;

	private DateSource dateSource;
    
    @Autowired
    public PartageServiceImpl(ProfileDAO profileDao,
                              DozerBeanMapper mapper,
                              DateSource dateSource,
                              MailComposer mailComposer,
                              PartageDAO partageDao,
                              BlockChainBusiness blockchain) {
        this.profileDao = profileDao;
        this.mapper = mapper;
        this.dateSource = dateSource;
        this.mailComposer = mailComposer;
        this.partageDao = partageDao;
        this.blockchain = blockchain;
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.service.PartageService#
	 * createOrModifyAnnonce(WAnnonce)
	 */
	@Override
	public WAnnonce createAnnonce(WAnnonceCreation wannonceC)
			throws UnknownUserException {
		Long userId = profileDao.getUserIdByFederationId(wannonceC
				.getFedUserId());
		if (userId == null) {
			throw new UnknownUserException("Initiateur non trouvé");
		}
		// Gamification
		KeyPair key = profileDao.getKeyPairByFederationId(wannonceC
				.getFedUserId());
		if (wannonceC.getNature().equals(AnnonceType.DEMANDE)) {
			blockchain.crediteEvenement(GamificationEvent.CREATION_DEMANDE,
					key.getPubKey(), null, null, true);
		} else {
			blockchain.crediteEvenement(GamificationEvent.CREATION_OFFRE,
					key.getPubKey(), null, null, true);
		}
		Annonce insertAnnonce = new Annonce();
		mapper.map(wannonceC, insertAnnonce);
		insertAnnonce.setPostDt(getDate());
		insertAnnonce.getInitiateur().setUserid(userId);
		partageDao.createOrModifyAnnonce(insertAnnonce);
        return mapper.map(insertAnnonce, WAnnonce.class);
	}

	/**
	 * Modify Annonce is without Gamification Transaction
	 */
	@Override
	public WAnnonce modifyAnnonce(WAnnonceModification wannonceM)
			throws UnknownUserException {
		Long userId = profileDao.getUserIdByFederationId(wannonceM
				.getFedUserId());
		if (userId == null) {
			throw new UnknownUserException("Initiateur non trouvé");
		}
		Annonce updateAnnonce = mapper.map(wannonceM, Annonce.class);
		updateAnnonce.getInitiateur().setUserid(userId);
		updateAnnonce.setPostDt(getDate());
		updateAnnonce = partageDao.createOrModifyAnnonce(updateAnnonce);
        return mapper.map(updateAnnonce, WAnnonce.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.service.PartageService#listAnnonces
	 * (java.lang.String)
	 */
    @Override
    public List<WAnnonce> listAnnonces(String federationId) {
        List<WAnnonce> result = new ArrayList<>();
        Long userId = profileDao.getUserIdByFederationId(federationId);
        List<Annonce> annonces = partageDao.listPartagesEtAnnonces(userId);
        for (Annonce annonce : annonces) {
            if (annonce instanceof Partage) {
                Partage p = (Partage) annonce;
                WMiseEnRelation wmei = mapper.map(p, WMiseEnRelation.class);
                if ((p.getInitiateur().getUserid().equals(userId) && p
                    .getType().equals(AnnonceType.OFFRE))
                    || (p.getSuiveur().getUserid().equals(userId))
                    && p.getType().equals(AnnonceType.DEMANDE)) {
                    KeyPair kp = profileDao.getKeyPairByFederationId(federationId);
                    Map<String, Integer> returnPayment;
                    if (p.getInitiateur().getUserid().equals(userId)
                        && p.getType().equals(AnnonceType.OFFRE)) {
                        if (p.getEvaluation() == null) {
                            // je suis l'initiateur et c'est une offre:
                            // l'évaluation de mon offre me fera gagner des
                            // deeps si l'autre l'évalue. Combien?
                            returnPayment = blockchain.crediteEvenement(
                                GamificationEvent.EVALUATION_PARTAGE,
                                kp.getPubKey(), null, p, false);
                            Integer pending = returnPayment.get("INITIATEUR");
                            wmei.setPendingDeeps(pending);
                        } else {
                            // je suis l'initiateur et c'est une offre évaluée.
                            // J'ai gagné combien?
                            returnPayment = blockchain.crediteEvenement(
                                GamificationEvent.EVALUATION_PARTAGE,
                                kp.getPubKey(), null, p, false);
                            Integer won = returnPayment.get("INITIATEUR");
                            wmei.setWonDeeps(won);
                        }
                    } else {
                        if (p.getEvaluation() == null) {
                            // je suis le suiveur et c'est une demande:
                            // l'évaluation de ma réponse me fera gagner des
                            // deeps si l'autre l'évalue
                            returnPayment = blockchain.crediteEvenement(
                                GamificationEvent.EVALUATION_PARTAGE,
                                kp.getPubKey(), null, p, false);
                            Integer pending = returnPayment.get("SUIVEUR");
                            wmei.setPendingDeeps(pending);
                        } else {
                            // je suis le suiveur et c'est une demande. J'ai
                            // gagné combien?
                            returnPayment = blockchain.crediteEvenement(
                                GamificationEvent.EVALUATION_PARTAGE,
                                kp.getPubKey(), null, p, false);
                            Integer won = returnPayment.get("SUIVEUR");
                            wmei.setWonDeeps(won);
                        }
                    }
                }
                result.add(wmei);
            } else {
                WAnnonce wann = mapper.map(annonce, WAnnonce.class);
                result.add(wann);
            }
        }
        return result;
    }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.service.PartageService#listAnnonces
	 * ()
	 */
	@Override
	public List<WAnnonce> listAnnonces() {
		List<WAnnonce> result = new ArrayList<>();
		List<Annonce> annonces = partageDao.listAnnonces();
		for (Annonce annonce : annonces) {
			result.add(mapper.map(annonce, WAnnonce.class));
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.service.PartageService#lieAnnonce
	 * (WAnnonceReponse)
	 */
	@Override
	public WMiseEnRelation lieAnnonce(WAnnonceReponse wreponse)
			throws UnknownUserException {
		AnnonceReponse reponse = mapper.map(wreponse, AnnonceReponse.class);
		reponse.setMatchdate(getDate());
		String suiveurFedId = wreponse.getFedUserid();
		Long userId = profileDao.getUserIdByFederationId(suiveurFedId);
        
		if (userId == null) throw new UnknownUserException("Vous devez être loggé");
        
		reponse.setUserid(userId);
		Partage partage = partageDao.lieAnnonce(reponse);
		List<Media> initiateurMedia = profileDao.listMedia(partage
				.getInitiateur().getUserid());
		partage.getInitiateur().setMediaPrivilegies(initiateurMedia);
		List<Media> suiveurMedia = profileDao.listMedia(partage.getSuiveur()
				.getUserid());
		partage.getSuiveur().setMediaPrivilegies(suiveurMedia);
		KeyPair kp = profileDao.getKeyPairByFederationId(wreponse
				.getFedUserid());
		Long u1 = userId;
		Long u2 = partage.getInitiateur().getUserid();
		BodyInfo body=new BodyInfo();
		body.setAnnonce(partage.getAnnonce());
		body.setTitle(partage.getTitle());
		body.setCommentaire(wreponse.getCommentaire());
		if (partage.getType().equals(AnnonceType.DEMANDE)) {
			blockchain.crediteEvenement(GamificationEvent.REPONSE_DEMANDE,
					kp.getPubKey(), null, partage, true);
			MailInfo mailinfo = composeMail(body, u1, u2);
			mailComposer.compose(MailObject.REPONSE_DEMANDE, mailinfo);
		} else {
			blockchain.crediteEvenement(GamificationEvent.REPONSE_OFFRE,
					kp.getPubKey(), null, partage, true);
			MailInfo mailinfo = composeMail(body, u1, u2);
			mailComposer.compose(MailObject.REPONSE_OFFRE, mailinfo);
		}
        return mapper.map(partage, WMiseEnRelation.class);
	}
	
	/* (non-Javadoc)
	 * @see com.natixis.partageon.service.PartageService#visuMailLieAnnonce(WAnnonceReponse)
	 */
	@Override
	public WMail visuMailLieAnnonce(WMailInfo wreponse)
			throws UnknownUserException {
		Annonce annonce = partageDao.getAnnonceById(wreponse.getAnnonceId());
		WMail mail;
		Long u2=annonce.getInitiateur().getUserid();
		Long userId = profileDao.getUserIdByFederationId(wreponse.getFedUserid());
		if (userId == null) {
			throw new UnknownUserException("Vous devez être loggé");
		}
		BodyInfo bodyInfo=new BodyInfo();
		bodyInfo.setAnnonce(annonce.getAnnonce());
		bodyInfo.setTitle(annonce.getTitle());
		bodyInfo.setCommentaire(wreponse.getCommentaireReponse());
		if (annonce.getType().equals(AnnonceType.DEMANDE)) {
			MailInfo mailinfo = composeMail(bodyInfo, userId, u2);
			mail=mailComposer.preVisualisation(MailObject.REPONSE_DEMANDE, mailinfo);
		} else {
			MailInfo mailinfo = composeMail(bodyInfo, userId, u2);
			mail=mailComposer.preVisualisation(MailObject.REPONSE_OFFRE, mailinfo);
		}
		return mail;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.service.PartageService#
	 * abandonneMiseEnRelation
	 * (WMiseEnRelationAbandon)
	 */
	@Override
	public void abandonneMiseEnRelation(WMiseEnRelationAbandon mer) {
		Long lacheurId = profileDao.getUserIdByFederationId(mer.getFedUserId());
		Partage partage = mapper.map(mer, Partage.class);
		partage.setLacheurId(lacheurId);
		partageDao.abandonneMiseEnRelation(partage);
		partage = partageDao.getMiseEnRelationById(mer.getMiseEnRelationId());
		List<Media> initiateurMedia = profileDao.listMedia(partage
				.getInitiateur().getUserid());
		partage.getInitiateur().setMediaPrivilegies(initiateurMedia);
		List<Media> suiveurMedia = profileDao.listMedia(partage.getSuiveur()
				.getUserid());
		partage.getSuiveur().setMediaPrivilegies(suiveurMedia);
		Long initiateurid = partage.getInitiateur().getUserid();
		Long suiveurid = partage.getSuiveur().getUserid();
		MailInfo mailinfo;
		BodyInfo bodyInfo=new BodyInfo();
		bodyInfo.setAnnonce(partage.getAnnonce());
		bodyInfo.setTitle(partage.getTitle());
		bodyInfo.setCommentaire(partage.getCommentaireAbandon());
		if (Objects.equals(lacheurId, initiateurid)) {
			mailinfo = composeMail(bodyInfo, initiateurid, suiveurid);
		} else {
			mailinfo = composeMail(bodyInfo, suiveurid, initiateurid);
		}
		mailComposer.compose(MailObject.ABANDON_MISE_EN_RELATION, mailinfo);
	}

	/**
	 * @param partage e
	 * @param u1 e
	 * @param u2 e
	 */
	private MailInfo composeMail(BodyInfo partage, Long u1, Long u2) {
		UserMail um1 = new UserMail();
		UserMail um2 = new UserMail();
		String mail1 = profileDao.getMail(u1);
		String mail2 = profileDao.getMail(u2);
		PreferencesUtilisateur pu1 = profileDao.getProfil(u1);
		PreferencesUtilisateur pu2 = profileDao.getProfil(u2);
		MailInfo mailinfo = new MailInfo();
		mailinfo.setBodyInfo(partage);
		mailinfo.setU1(um1);
		mailinfo.setU2(um2);
		um1.setU(pu1);
		um1.setMail(mail1);
		um2.setU(pu2);
		um2.setMail(mail2);
		return mailinfo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.service.PartageService#
	 * archiveMiseEnRelation
	 * (WMiseEnRelationArchivage)
	 */
	@Override
	public void archiveMiseEnRelation(WMiseEnRelationArchivage mer) {
		Partage partage = mapper.map(mer, Partage.class);
		partageDao.archiveMiseEnRelation(partage);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.service.PartageService#
	 * evalueMiseEnRelation
	 * (WMiseEnRelationEvaluation)
	 */
	@Override
	public void evalueMiseEnRelation(WMiseEnRelationEvaluation evaluation) {
		Partage partage = mapper.map(evaluation, Partage.class);
		Long userId = profileDao.getUserIdByFederationId(evaluation
				.getFedUserId());
		partage.getSuiveur().setUserid(userId);
		partage = partageDao.evalueMiseEnRelation(partage);
		KeyPair kp = profileDao.getKeyPairByFederationId(evaluation
				.getFedUserId());
		if (partage.getType().equals(AnnonceType.DEMANDE)) {
			blockchain.crediteEvenement(GamificationEvent.EVALUATION_PARTAGE,
					kp.getPubKey(), null, partage, true);
		} else {
			blockchain.crediteEvenement(GamificationEvent.EVALUATION_PARTAGE,
					kp.getPubKey(), null, partage, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.service.PartageService#listThemes()
	 */
	@Override
	public List<Theme> listThemes() {
		return partageDao.listThemes();
	}

	/**
	 * @return Date
	 */
    private Date getDate() {
		return dateSource.getDate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.service.PartageService#clotureAnnonce
	 * (WAnnonceCloture)
	 */
	@Override
	public void clotureAnnonce(WAnnonceCloture wClotureannonce) {
		Annonce annonce = mapper.map(wClotureannonce, Annonce.class);
		partageDao.clotureAnnonce(annonce);
	}

}
