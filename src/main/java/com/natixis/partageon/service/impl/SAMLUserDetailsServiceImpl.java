package com.natixis.partageon.service.impl;

import com.natixis.partageon.model.client.User;
import org.opensaml.saml2.core.Attribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.security.saml.userdetails.SAMLUserDetailsService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ianniau on 09/03/2017.
 *
 */

@Service
public class SAMLUserDetailsServiceImpl implements SAMLUserDetailsService {

	// Logger
	private static final Logger LOG = LoggerFactory.getLogger(SAMLUserDetailsServiceImpl.class);

	public Object loadUserBySAML(SAMLCredential credential)
			throws UsernameNotFoundException {

		String userID = credential.getNameID().getValue();
		Map<String,String> attributes = new HashMap<>();

		for (Attribute attribute : credential.getAttributes()) {
			LOG.debug("attribute name : "+attribute.getName());
			int slash = attribute.getName().lastIndexOf("/");
			String attName = attribute.getName().substring(slash+1);
			LOG.debug("substring attname: "+attName);
			LOG.debug("attribute value : "+credential.getAttributeAsString(attribute.getName()));
			attributes.put(attName,credential.getAttributeAsString(attribute.getName()));
		}

		User user = new User();
		// attributes.get("CommonName") correspond au CIB\ianniau chez NXS MAIS pas obligatoire dans le groupe
		user.setFedUserId(userID);
		user.setNom(attributes.get("surname"));
		user.setPrenom(attributes.get("givenname"));
		user.setEmail(attributes.get("emailaddress"));
		LOG.debug("USER retrieve"+ user.toString());
		return user;
	}
}
