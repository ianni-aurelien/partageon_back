package com.natixis.partageon.service.impl;

import com.natixis.partageon.blockchain.BlockChainBusiness;
import com.natixis.partageon.blockchain.CryptoService;
import com.natixis.partageon.blockchain.GamificationEvent;
import com.natixis.partageon.dao.ProfileDAO;
import com.natixis.partageon.exception.AuthenticationRequiredException;
import com.natixis.partageon.exception.LdapException;
import com.natixis.partageon.exception.TechnicalException;
import com.natixis.partageon.exception.UnknownUserException;
import com.natixis.partageon.model.client.MediaAndSites;
import com.natixis.partageon.model.client.User;
import com.natixis.partageon.model.client.WAvatar;
import com.natixis.partageon.model.system.*;
import com.natixis.partageon.service.ProfileService;
import com.natixis.partageon.useridentity.FederationIdentiteService;
import com.natixis.partageon.utils.PropertiesService;
import lombok.Setter;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Implémentation du service exposé en frontal.
 *
 * @author chaigneauam
 *
 */
@Service
@Setter
public class ProfileServiceImpl implements ProfileService {
    
    private static final Logger logger = LoggerFactory.getLogger(ProfileServiceImpl.class);
    
    private FederationIdentiteService federationIdentiteService;
    
    private final ProfileDAO dao;
    
    private final CryptoService crypto;
    
    private final BlockChainBusiness blockchain;
    
    private final DozerBeanMapper mapper;
    
    private final PropertiesService properties;
    
    @Autowired
    public ProfileServiceImpl(
        BlockChainBusiness blockchain,
        CryptoService crypto, PropertiesService properties,
        FederationIdentiteService federationIdentiteService,
        DozerBeanMapper mapper,
        ProfileDAO dao) {
        this.blockchain = blockchain;
        this.crypto = crypto;
        this.properties = properties;
        this.federationIdentiteService = federationIdentiteService;
        this.mapper = mapper;
        this.dao = dao;
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.service.ProfileService#getUserProfil (java.lang.String)
	 */
    @Override
    public User getUserProfil(String fedUserId) {
        UtilisateurFederationIdentite fedInfo = federationIdentiteService.getElementFromCache(fedUserId);
        // if (fedInfo == null) {
        // throw new UnknownUserException("L'utilisateur " + fedUserId
        // + " est inconnu de la FEDID.");
        // }
        User u = new User();
        if (fedInfo == null) {
            u.setNeedAuthentication(true);
            u.setFedUserId(fedUserId);
            return u;
        }
        u.setNeedAuthentication(false);
        Long userid = dao.getUserIdByFederationId(fedUserId);
        mapper.map(fedInfo, u);
        if (userid == null) {
            u.setFedUserId(fedUserId);
            u.setUnknownFromDB(true);
            return u;
        }
        PreferencesUtilisateur profil = dao.getProfil(userid);
        mapper.map(profil, u);
        KeyPair kp = dao.getKeyPairByFederationId(fedUserId);
        mapper.map(kp, u);
        
        try {
            Deeps deeps = blockchain.getDeeps(kp.getPubKey());
            u.setDeeps(deeps);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
        return u;
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.service.ProfileService#createUserProfile (User)
	 */
    @Override
    public void createOrModifyUserProfile(User user)
        throws UnknownUserException, AuthenticationRequiredException {
        UtilisateurFederationIdentite fedInfo = federationIdentiteService.getElementFromCache(user.getFedUserId());
        User u = new User();
        if (fedInfo == null) {
            u.setNeedAuthentication(true);
            u.setFedUserId(user.getFedUserId());
            throw new AuthenticationRequiredException("Authentication Expected");
        }
        // if (fedInfo == null) {
        // throw new UnknownUserException("L'utilisateur "
        // + user.getFedUserId() + " est inconnu de la FEDID.");
        // }
        Long userid = dao.getUserIdByFederationId(user.getFedUserId());
        PreferencesUtilisateur profil = mapper.map(user, PreferencesUtilisateur.class);
        if (userid == null) {
            // l'utlisateur est inconnu de la base: on le créé
            KeyPair keypair = crypto.genereKeyPair();
            userid = dao.createUserIdentite(user.getFedUserId(), keypair.getPubKey(), keypair.getPrivKey());
            profil.setUserid(userid);
            dao.createUserProfile(profil);
            // on affecte les 10 premiers Deeps
            try {
                blockchain.crediteEvenement(GamificationEvent.CREATION_PROFIL, keypair.getPubKey(), null, null, true);
            } catch (Exception e) {
                logger.error(e.getMessage(),e);
            }
        } else {
            // il s'agit d'une modification du profil
            profil.setUserid(userid);
            dao.modifyPreferences(profil);
        }
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.service.ProfileService#listMediaAndSites ()
	 */
    @Override
    public MediaAndSites listMediaAndSites() {
        MediaAndSites mas = new MediaAndSites();
        List<Media> media = dao.listMedia();
        mas.setMedia(media);
        List<Site> sites = dao.listSites();
        mas.setSites(sites);
        return mas;
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.service.ProfileService#listAvatars()
	 */
    @Override
    public List<WAvatar> listAvatars() {
        List<Avatar> res = dao.findAllAvatar();
        List<WAvatar> result = new ArrayList<>();
        for (Avatar avatar : res) {
            result.add(mapper.map(avatar, WAvatar.class));
        }
        return result;
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.service.ProfileService#getAvatar(java .lang.String)
	 */
    @Override
    public WAvatar getAvatar(String avatarTitle) {
        Avatar avatar = dao.findAvatarByTitle(avatarTitle);
        return mapper.map(avatar, WAvatar.class);
    }
    
    @Override
    public Long setAvatar(WAvatar WAvatar) {
        Avatar dto = new Avatar();
        mapper.map(WAvatar, dto);
        return dao.addAvatar(dto);
    }
    
    @Override
    public void setAvatarToUserByFedId(String fedId, WAvatar WAvatar) throws AuthenticationRequiredException, UnknownUserException {
        // we insert avatar image and catch his id
        User user = getUserProfil(fedId);
        if(user.getNeedAuthentication()) {
            throw new AuthenticationRequiredException("Authentication Expected");
        }
        Long userid = dao.getUserIdByFederationId(user.getFedUserId());
        if(userid == null) {
            createOrModifyUserProfile(user);
            userid = dao.getUserIdByFederationId(user.getFedUserId());
        }
        PreferencesUtilisateur profil = mapper.map(user, PreferencesUtilisateur.class);
        if(WAvatar.getContent() == null) {
            profil.setUriAvatar(WAvatar.getFilename());
            dao.setUserAvatarUri(profil);
        } else {
            Avatar avatar = mapper.map(WAvatar, Avatar.class);
            profil.setAvatar(avatar);
            profil.setUserid(userid);
            dao.setUserAvatarImg(profil);
        }
    }
    
    @Override
    public Avatar getAvatarByUserFedId(String fedId) throws AuthenticationRequiredException, UnknownUserException {
        User user = getUserProfil(fedId);
        Long userid = dao.getUserIdByFederationId(user.getFedUserId());
        if(userid == null) {
            throw new UnknownUserException("User unknown, not register in application");
        }
        return dao.findAvatarByFedId(fedId);
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.service.ProfileService#authenticate (java.lang.String, java.lang.String, java.lang.String)
	 */
    @Override
    public User authenticate(String username, String password)
        throws TechnicalException, LdapException {
        UtilisateurFederationIdentite userFed;
        try {
            userFed = federationIdentiteService.retrieveUserInfo(
                username, password);
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new LdapException(e);
        }
        String fedUserId = federationIdentiteService.generateHashId(userFed, username);
        federationIdentiteService.putElementInCache(fedUserId, userFed);
        Long userId = dao.getUserIdByFederationId(fedUserId);
        dao.updateUserMail(userId, userFed.getEmail());
        User user = getUserProfil(fedUserId);
        user.setFedUserId(fedUserId);
        return user;
    }
    
}
