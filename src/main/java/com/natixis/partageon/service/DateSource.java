package com.natixis.partageon.service;

import java.util.Date;

/**
 * @author chaigneauam
 *
 */
public interface DateSource {

	Date getDate();
}
