package com.natixis.partageon.blockchain;

import org.springframework.http.ResponseEntity;


public interface BlockChainService {

	
	/**
	 * 
	 * @return
	 */
    <T> ResponseEntity<?> getBlockChainTime();

	/**
	 * 
	 * @return
	 */
    <T> ResponseEntity<?> getBlockChainStatus();

	/**
	 * 
	 * @param publicKey
	 * @return
	 */
    ResponseEntity<?> getAccountId(String publicKey);

	/**
	 * 
	 * @param accountId
	 * @return
	 */
    ResponseEntity<?> getAccountPublicKey(String accountId);

	/**
	 * 
	 * @param accountId
	 * @return
	 */
    ResponseEntity<?> getAccountInfo(String accountId);

	/**
	 * 
	 * @param accountAddress
	 * @return
	 */
    ResponseEntity<?> getAccountProperties(String accountAddress);

	/**
	 * 
	 * @param accountAddress
	 * @return
	 */
    ResponseEntity<?> getAccountBalance(String accountAddress);

	/**
	 * 
	 * @param senderPublicKey
	 * @param recipientPublicKey
	 * @param amount
	 * @return
	 */
    ResponseEntity<?> getUnsignedTransaction(String senderPublicKey, String recipientPublicKey, int amount);

	/**
	 * 
	 * @param senderPublicKey
	 * @param recipientPublicKey
	 * @param amount
	 * @return
	 */
    ResponseEntity<?> sendTransaction(String senderPublicKey, String recipientPublicKey, int amount);
	
	/**
	 * 
	 * @param resp
	 */
    boolean checkResponseStatus(ResponseEntity<?> resp);
}
