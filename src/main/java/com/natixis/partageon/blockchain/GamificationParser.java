package com.natixis.partageon.blockchain;

import com.natixis.partageon.model.system.GamificationRules;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;

@Component
public class GamificationParser {

	private static final Logger logger = LoggerFactory.getLogger(GamificationParser.class);

	@Value("${app.gamification.file}")
	private String configuration;

	public static GamificationRules parseConfiguration(String filename) throws JAXBException, IOException {
		JAXBContext jc = JAXBContext.newInstance(GamificationRules.class);
		Unmarshaller unmarshaller = jc.createUnmarshaller();
		Resource resource = new ClassPathResource(filename);
		File xml = resource.getFile();
        return (GamificationRules) unmarshaller.unmarshal(xml);
	}

	public GamificationRules getObject() {
		GamificationRules conf = null;
		try {
			conf = parseConfiguration(configuration);
		} catch (JAXBException | IOException e) {
			logger.error(e.getMessage(), e);
		}
		return conf;
	}

}

