package com.natixis.partageon.blockchain;

import com.natixis.partageon.model.system.Event;
import com.natixis.partageon.model.system.GamificationRules;
import com.natixis.partageon.model.system.Partage;
import com.natixis.partageon.utils.PartageOnConstantes;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by ianniau on 31/03/2017.
 */
@Configuration
public class BlockchainUtils {

	private static GamificationRules gamificationRules;

	@Autowired
	private GamificationParser gamificationParser;

	@PostConstruct
	public void initSmartBlockChainBusinessImpl() {
		gamificationRules = gamificationParser.getObject();
	}

	public static Map<String, Integer> setRewardAmount(GamificationEvent event, Object value) {
		List<GamificationEvent> eventResult = define_event_list(event, value);
		List<Event> eventRule = get_rules_by_id(eventResult);
		Map<String,Integer> deepResult = new HashMap<>();

		int deepAmountInitiateur = eventRule.stream()
			.filter(item -> item.initiateur != null)
			.mapToInt(item -> item.initiateur.deeps)
			.sum();
		int deepAmountSuiveur = eventRule.stream()
			.filter(item -> item.suiveur != null)
			.mapToInt(item -> item.suiveur.deeps)
			.sum();

		deepResult.put(PartageOnConstantes.INITIATEUR, deepAmountInitiateur);
		deepResult.put(PartageOnConstantes.SUIVEUR, deepAmountSuiveur);

		return deepResult;
	}

	private static int diff_between_date(Date aDate, Date aDate2) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(aDate);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(aDate2);

		LocalDate d1 = new LocalDate(cal.getTimeInMillis());
		LocalDate d2 = new LocalDate(cal2.getTimeInMillis());

		return Days.daysBetween(d2, d1).getDays();
	}

	public static List<Event> get_rules_by_id(List<GamificationEvent> aEventResult) {
		List<Event> result = new ArrayList<>();
		for (GamificationEvent lEvent : aEventResult) {
			result.addAll(gamificationRules.getSummaryElementList().stream()
				.filter(rule -> lEvent.toString().equals(rule.id))
				.collect(Collectors.toList()));
		}
		return result;
	}

	public static List<GamificationEvent> define_event_list(GamificationEvent event, Object value) {
		List<GamificationEvent> resultList = new ArrayList<>();
		resultList.add(event);
		if(null != value) {
			Partage aPartage = (Partage)value;
			if(GamificationEvent.REPONSE_DEMANDE.equals(event)) {
				if(diff_between_date(aPartage.getMatchDt(),aPartage.getPostDt()) < 7) {
					resultList.add(GamificationEvent.REPONSE_DEMANDE_NEW);
				}
				if(aPartage.getUrgence().getUrgence()
					&& diff_between_date(aPartage.getMatchDt(),aPartage.getPostDt()) < 2) {
					resultList.add(GamificationEvent.REPONSE_DEMANDE_URGENTE);
				}
			}
			if(GamificationEvent.EVALUATION_PARTAGE.equals(event)) {
				if(aPartage.getEvaluation() != null && aPartage.getEvaluation() > 2) {
					resultList.add(GamificationEvent.EVALUATION_PARTAGE_SUP_2_STAR);
				}
				if(diff_between_date(new Date(), aPartage.getMatchDt()) < 15) {
					resultList.add(GamificationEvent.EVALUATION_PARTAGE_INF_15J);
				}
			}
		}
		return resultList;
	}
}
