package com.natixis.partageon.blockchain.impl.nxt;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;

/**
 * Created by ianniau on 26/01/2017.
 */
@Getter
@Setter
@Component
@Profile("nxt")
public class NxtProperties {

    @NotNull
    String BLOCKCHAIN_BASE_URL;

    @NotNull
    String BANK_ACCOUNT_ID;

    @NotNull
    String BANK_ACCOUNT_RS;

    @NotNull
    String BANK_PUB_KEY;

    @NotNull
    String BANK_PASSPHRASE;

    BigInteger GAS_LIMIT = BigInteger.valueOf(100000L);

    BigInteger GAS_PRICE = BigInteger.valueOf(1L);
}
