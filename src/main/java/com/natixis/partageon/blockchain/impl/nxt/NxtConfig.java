package com.natixis.partageon.blockchain.impl.nxt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

/**
 * Created by ianniau on 15/02/2017.
 */

@Configuration
@PropertySource(value = "classpath:${config.folder}/nxt.properties")
@Profile("nxt")
public class NxtConfig {

    @Autowired
    private Environment env;

    @Bean
    public NxtProperties nxtProperties() {
        NxtProperties nxtProperties = new NxtProperties();
        nxtProperties.setBLOCKCHAIN_BASE_URL(env.getProperty("nxt.BLOCKCHAIN_BASE_URL"));
        nxtProperties.setBANK_PUB_KEY(env.getProperty("nxt.BANK_PUB_KEY"));
        nxtProperties.setBANK_ACCOUNT_ID(env.getProperty("nxt.BANK_ACCOUNT_ID"));
        nxtProperties.setBANK_ACCOUNT_RS(env.getProperty("nxt.BANK_ACCOUNT_RS"));
        nxtProperties.setBANK_PASSPHRASE(env.getProperty("nxt.BANK_PASSPHRASE"));
        return nxtProperties;
    }
}
