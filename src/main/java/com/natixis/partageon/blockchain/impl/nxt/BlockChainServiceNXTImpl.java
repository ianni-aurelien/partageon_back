package com.natixis.partageon.blockchain.impl.nxt;

import com.natixis.partageon.blockchain.BlockChainService;
import com.natixis.partageon.model.system.Account;
import com.natixis.partageon.model.system.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Profile("nxt")
public class BlockChainServiceNXTImpl implements BlockChainService {

	private final BlockChainNXTRequest blockChainNXTRequest;

	private final String BLOCKCHAIN_BASE_URL;

	@Autowired
	public BlockChainServiceNXTImpl(BlockChainNXTRequest blockChainNXTRequest, NxtProperties nxtProperties) {
		this.blockChainNXTRequest = blockChainNXTRequest;
		this.BLOCKCHAIN_BASE_URL = nxtProperties.getBLOCKCHAIN_BASE_URL();
	}

	/**
	 * 
	 * @return
	 */
	public <T> ResponseEntity<?> getBlockChainTime() {
		String requestType = "?requestType=getTime";
		String finalRequest = BLOCKCHAIN_BASE_URL + requestType;
		return blockChainNXTRequest.getEntity(finalRequest, String.class);
	}

	/**
	 * 
	 * @return
	 */
	public <T> ResponseEntity<?> getBlockChainStatus() {
		String requestType = "?requestType=getBlockchainStatus";
		String finalRequest = BLOCKCHAIN_BASE_URL + requestType;
		return blockChainNXTRequest.getEntity(finalRequest, String.class);
	}

	/**
	 * 
	 * @param publicKey
	 * @return
	 */
	public ResponseEntity<?> getAccountId(String publicKey) {
		String requestType = "?requestType=getAccountId";
		String param = "&publicKey=" + publicKey;
		String finalRequest = BLOCKCHAIN_BASE_URL + requestType + param;
		Map<String, String> map = new HashMap<>();
		map.put("publicKey", publicKey);
		map.put("requestType ", "getAccountId");
		return blockChainNXTRequest.postEntity(finalRequest, new Account(), Account.class, map);
	}

	/**
	 * 
	 * @param accountId
	 * @return
	 */
	public ResponseEntity<?> getAccountPublicKey(String accountId) {
		String requestType = "?requestType=getAccountPublicKey";
		String param = "&account=" + accountId;
		String finalRequest = BLOCKCHAIN_BASE_URL + requestType + param;
		return blockChainNXTRequest.getEntity(finalRequest, Account.class);
	}

	/**
	 * 
	 * @param accountId
	 * @return
	 */
	public ResponseEntity<?> getAccountInfo(String accountId) {
		String requestType = "?requestType=getAccount";
		String param = "&account=" + accountId;
		String finalRequest = BLOCKCHAIN_BASE_URL + requestType + param;
		return blockChainNXTRequest.getEntity(finalRequest, Account.class);
	}

	/**
	 * 
	 * @param accountAddress
	 * @return
	 */
	public ResponseEntity<?> getAccountProperties(String accountAddress) {
		String requestType = "?requestType=getAccountProperties";
		String param = "&account=" + accountAddress;
		String finalRequest = BLOCKCHAIN_BASE_URL + requestType + param;
		return blockChainNXTRequest.getEntity(finalRequest, Account.class);
	}

	/**
	 * 
	 * @param accountAddress
	 * @return
	 */
	public ResponseEntity<?> getAccountBalance(String accountAddress) {
		String requestType = "?requestType=getBalance";
		String param = "&account=" + accountAddress;
		String finalRequest = BLOCKCHAIN_BASE_URL + requestType + param;
		return blockChainNXTRequest.getEntity(finalRequest, Account.class);
	}

	/**
	 * 
	 * @param senderKey
	 * @param rcpKey
	 * @param amount
	 * @return
	 */
	@SuppressWarnings("unused")
	public ResponseEntity<?> getUnsignedTransaction(String senderKey, String rcpKey, int amount) {
		String requestType = "?requestType=sendMoney";
		String senderPublicKey = "&publicKey=" + senderKey;
		String recipientPublicKey = "&recipient=" + rcpKey;
		String amountNQT = "&amountNQT=" + amount;
		String feeNQT = "&feeNQT=" + 5;
		String deadline = "&deadline=" + 60;
		String referencedTransactionFullHash = "";
		String broadcast = "&broadcast=" + false;
		String finalRequest = BLOCKCHAIN_BASE_URL + requestType + senderPublicKey + recipientPublicKey + amountNQT + feeNQT + deadline + broadcast;
		Map<String, String> map = new HashMap<>();
		return blockChainNXTRequest.postEntity(finalRequest, new Transaction(), Transaction.class, map);
	}

	/**
	 * 
	 * @param BANK_PASS
	 * @param aAccountRS
	 * @param amount
	 * @return
	 */
	public ResponseEntity<?> sendTransaction(String BANK_PASS, String aAccountRS, int amount) {
		String requestType = "?requestType=sendMoney";
		String senderPublicKey = "&secretPhrase=" + BANK_PASS;
		String recipientPublicKey = "&recipient=" + aAccountRS;
		String amountNQT = "&amountNQT=" + amount;
		String feeNQT = "&feeNQT=" + 100000000;
		String deadline = "&deadline=" + 60;
		String finalRequest = BLOCKCHAIN_BASE_URL + requestType + senderPublicKey + recipientPublicKey + amountNQT + feeNQT + deadline;
		Map<String, String> map = new HashMap<>();
		return blockChainNXTRequest.postEntity(finalRequest, new Transaction(), Transaction.class, map);
	}

	@Override
	public boolean checkResponseStatus(ResponseEntity<?> aResp) {
		return aResp.getStatusCodeValue() < 400;
	}
}
