package com.natixis.partageon.blockchain.impl.ethereum;

import com.natixis.partageon.blockchain.BlockChainService;
import com.natixis.partageon.exception.EndPointException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.Keys;
import org.web3j.crypto.TransactionEncoder;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.RawTransaction;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.core.methods.response.NetListening;
import org.web3j.protocol.exceptions.MessageDecodingException;
import org.web3j.protocol.http.HttpService;
import org.web3j.utils.Numeric;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.math.BigInteger;
import java.util.concurrent.ExecutionException;

@Service
@Profile("ethereum")
public class BlockChainServiceEthereumImpl implements BlockChainService {

	private static final Logger LOG = LoggerFactory.getLogger(BlockChainServiceEthereumImpl.class);

	private final EthereumProperties ethereumProperties;

	private BigInteger GAS_PRICE;
	private BigInteger GAS_LIMIT;

	private Web3j web3j;
    
    @Autowired
    public BlockChainServiceEthereumImpl(EthereumProperties ethereumProperties) {
        this.ethereumProperties = ethereumProperties;
    }
    
    @PostConstruct
	public void initSmartBlockChainEthereumServiceImpl() {
		GAS_LIMIT = ethereumProperties.getGAS_LIMIT();
		GAS_PRICE = ethereumProperties.getGAS_PRICE();
		web3j = Web3j.build(new HttpService(ethereumProperties.getBLOCKCHAIN_BASE_URL()));
		try {
			EthGetTransactionCount nonce = web3j
					.ethGetTransactionCount(ethereumProperties.getBANK_PUB_KEY()
							, DefaultBlockParameterName.PENDING)
					.sendAsync()
					.get();
		} catch (ExecutionException | InterruptedException e) {
			LOG.error(e.getMessage(), e);
		}
	}

	@Override
	public  ResponseEntity<?> getBlockChainTime() {return null;}

	@Override
	public ResponseEntity<?> getBlockChainStatus() {
		NetListening status;
		try {
			status = web3j.netListening().send();
			return new ResponseEntity<>(status.isListening()
					, HttpStatus.ACCEPTED);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<?> getAccountId(String publicKey) {
		return new ResponseEntity<>(Keys.getAddress(publicKey)
				, HttpStatus.ACCEPTED);
	}

	@Override
	public ResponseEntity<?> getAccountPublicKey(String accountAddress) {
		return new ResponseEntity<>(new EndPointException("Not Implemented")
				, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	public ResponseEntity<?> getAccountInfo(String accountAddress) {
		return new ResponseEntity<>(new EndPointException("Not Implemented")
				, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	public ResponseEntity<?> getAccountProperties(String accountAddress) {
		return new ResponseEntity<>(new EndPointException("Not Implemented")
				, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	public ResponseEntity<?> getAccountBalance(String publicKey) {
		EthGetBalance balance;
		String address = Keys.getAddress(publicKey);
		LOG.debug("Recipient @: "+"0x"+address);
		try {
			balance = web3j.ethGetBalance("0x"+address, DefaultBlockParameterName.LATEST)
                .sendAsync()
                .get();
			LOG.debug("Balance: "+ balance.getBalance());
			return new ResponseEntity<>(balance.getBalance()
					, HttpStatus.ACCEPTED);
		} catch (InterruptedException | MessageDecodingException | ExecutionException e) {
			LOG.error(e.getMessage(), e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<?> getUnsignedTransaction(String senderKey, String rcpKey, int amount) {
		RawTransaction rawTransaction;
		EthGetTransactionCount ethGetTransactionCount;
		try {
			ethGetTransactionCount = web3j.ethGetTransactionCount(
					senderKey, DefaultBlockParameterName.PENDING).sendAsync().get();
			BigInteger nonce = ethGetTransactionCount.getTransactionCount();
			LOG.debug("Nonce: "+nonce);
			rawTransaction = RawTransaction.createEtherTransaction(
					nonce,GAS_PRICE,GAS_LIMIT,
					Keys.getAddress(rcpKey), BigInteger.valueOf(amount));

			LOG.debug("Raw Transaction "+rawTransaction.getTo());
			return new ResponseEntity<>(rawTransaction, HttpStatus.ACCEPTED);
		} catch ( InterruptedException | ExecutionException e) {
			LOG.error(e.getMessage(), e);
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<?> sendTransaction(String senderPublicKey, String recipientPublicKey, int amount) {
		ResponseEntity<?> unsignedTransaction = getUnsignedTransaction(senderPublicKey
												, recipientPublicKey
												, amount);
		EthSendTransaction ethSendTransaction;

		if(unsignedTransaction.getStatusCode() != HttpStatus.ACCEPTED) {
			return new ResponseEntity<>(unsignedTransaction.getBody()
					,HttpStatus.INTERNAL_SERVER_ERROR);
		}

		try {
				RawTransaction rawTransaction = (RawTransaction)unsignedTransaction.getBody();
				Credentials test = Credentials.create(ethereumProperties.getBANK_PRIV_KEY());
				byte[] signedMessage = TransactionEncoder.signMessage(rawTransaction, test);
				String hexValue = Numeric.toHexString(signedMessage);
				ethSendTransaction = web3j.ethSendRawTransaction(hexValue).send();
		} catch (IOException e) {
				LOG.error(e.getMessage(), e);
				return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if(ethSendTransaction.hasError() || ethSendTransaction.getTransactionHash() == null) {
			LOG.error("Transaction reject "+ethSendTransaction.getError().getMessage());
			return new ResponseEntity<>(new EndPointException(ethSendTransaction.getResult())
					, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		LOG.debug("Transaction Hash: "+ethSendTransaction.getTransactionHash());
		return new ResponseEntity<>(ethSendTransaction, HttpStatus.ACCEPTED);
	}

	@Override
	public boolean checkResponseStatus(ResponseEntity<?> resp) {
		return false;
	}
}
