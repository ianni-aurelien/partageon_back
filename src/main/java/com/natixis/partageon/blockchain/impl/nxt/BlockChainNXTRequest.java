package com.natixis.partageon.blockchain.impl.nxt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Map;

@Component
@Profile("nxt")
public class BlockChainNXTRequest {

	private static final Logger logger = LoggerFactory.getLogger(BlockChainNXTRequest.class);

	private static final int HTTP_REQUEST_TIMEOUT = 500;

	public <T> ResponseEntity<?> getEntity(String url, Class<T> entity) {
		if(hostHealthCheck(url)) {
			RestTemplate restTemplate = new RestTemplate();
			MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
			mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM, MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON_UTF8));
			restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
			ResponseEntity<?> resultEntity = restTemplate.getForEntity(url, entity);
			logger.debug("request entity result"+resultEntity.toString());
			return resultEntity;
		} else {
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("BlockChain Unreacheable");
		}
	}

	public <T> ResponseEntity<?> postEntity(String url, T instance, Class<T> entity, Map<String, String> param) {
		if(hostHealthCheck(url)) {
			RestTemplate restTemplate = new RestTemplate();
			MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
			mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM, MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON_UTF8));
			restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
			ResponseEntity<?> dataEntity = restTemplate.postForEntity(url, instance, entity, param);
			logger.debug("request entity result "+dataEntity.toString());
			return dataEntity;
		} else {
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("BlockChain Unreacheable");
		}
	}

	public static boolean hostHealthCheck(String url) {
		HttpURLConnection connection;
		try {
			connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestMethod("HEAD");
			connection.setConnectTimeout(HTTP_REQUEST_TIMEOUT);
			int responseCode = connection.getResponseCode();
			return responseCode == 200;
		} catch (IOException e) {
//			logger.error(e.getMessage(),e);
			return false;
		}
	}
}
