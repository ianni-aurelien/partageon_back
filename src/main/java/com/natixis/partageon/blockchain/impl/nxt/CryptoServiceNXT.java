package com.natixis.partageon.blockchain.impl.nxt;

import com.natixis.partageon.blockchain.CryptoService;
import com.natixis.partageon.model.system.KeyPair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static org.apache.commons.lang3.RandomStringUtils.random;

/**
 * @author chaigneauam
 *
 */
@Service
@Profile("nxt")
public class CryptoServiceNXT implements CryptoService {

	private static final Logger logger = LoggerFactory.getLogger(CryptoServiceNXT.class);

	private static final char[] hexChars = { '0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f' };
	/*
	 * (non-Javadoc)
	 *
	 * @see com.natixis.partageon.blockchain.CryptoService#genereKeyPair()
	 */
	@Override
	public KeyPair genereKeyPair() {
		String secretPhrase = random(50,true,true);
		try {
			byte[] privateKey = getPrivateKey(secretPhrase);
			byte[] publicKey = getPublicKey(secretPhrase);
			String str_privateKey = toHexString(privateKey);
			String str_publicKey = toHexString(publicKey);

			// TODO Remove only for Debug
			logger.info("passphrase key :" + secretPhrase);
			logger.info("private key :" + privateKey);
			logger.info("public key :" + publicKey);
			logger.info("str_private key :" + str_privateKey);
			logger.info("str_public key :" + str_publicKey);
			return new KeyPair(str_privateKey, str_publicKey);
		} catch (NoSuchAlgorithmException exception) {
			throw new RuntimeException(exception.getMessage());
		}
	}

	public static byte[] parseHexString(String hex) {
		if (hex == null) {
			return null;
		}
		byte[] bytes = new byte[hex.length() / 2];
		for (int i = 0; i < bytes.length; i++) {
			int char1 = hex.charAt(i * 2);
			char1 = char1 > 0x60 ? char1 - 0x57 : char1 - 0x30;
			int char2 = hex.charAt(i * 2 + 1);
			char2 = char2 > 0x60 ? char2 - 0x57 : char2 - 0x30;
			if (char1 < 0 || char2 < 0 || char1 > 15 || char2 > 15) {
				throw new NumberFormatException("Invalid hex number: " + hex);
			}
			bytes[i] = (byte) ((char1 << 4) + char2);
		}
		return bytes;
	}

	public static String toHexString(byte[] bytes) {
		if (bytes == null) {
			return null;
		}
		char[] chars = new char[bytes.length * 2];
		for (int i = 0; i < bytes.length; i++) {
			chars[i * 2] = hexChars[((bytes[i] >> 4) & 0xF)];
			chars[i * 2 + 1] = hexChars[(bytes[i] & 0xF)];
		}
		return String.valueOf(chars);
	}

	public static byte[] getPrivateKey(String secretPhrase) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		byte[] s = md.digest(secretPhrase.getBytes(StandardCharsets.UTF_8));
		Curve25519.clamp(s);
		return s;
	}

	public static byte[] getPublicKey(String secretPhrase) throws NoSuchAlgorithmException {
		byte[] publicKey = new byte[32];
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(secretPhrase.getBytes(StandardCharsets.UTF_8));
		Curve25519.keygen(publicKey, null, secretPhrase.getBytes(StandardCharsets.UTF_8));
		return publicKey;
	}
}
