package com.natixis.partageon.blockchain.impl.mock;

import com.natixis.partageon.blockchain.BlockChainBusiness;
import com.natixis.partageon.blockchain.GamificationEvent;
import com.natixis.partageon.model.system.Deeps;
import com.natixis.partageon.utils.PartageOnConstantes;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by aurelien on 01/04/17.
 */
@Service
@Profile("mockblockchain")
public class MockBlockchainBusiness implements BlockChainBusiness {
    
    @Override
    public Deeps getDeeps(String pK) {
        return new Deeps(0L,0L);
    }
    
    @Override
    public List<String> getClassement() {
        return Collections.emptyList();
    }
    
    @Override
    public Map<String, Integer> crediteEvenement(GamificationEvent aEvent, String aPK1, String aPK2, Object aValue, boolean aPaiement) {
        HashMap<String, Integer> result = new HashMap<>();
        result.put(PartageOnConstantes.INITIATEUR, 10);
        result.put(PartageOnConstantes.SUIVEUR, 10);
        return result;
        
    }
}
