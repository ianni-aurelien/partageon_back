package com.natixis.partageon.blockchain.impl.ethereum;

import com.natixis.partageon.blockchain.CryptoService;
import com.natixis.partageon.model.system.KeyPair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.web3j.crypto.ECKeyPair;
import org.web3j.crypto.Keys;
import org.web3j.utils.Numeric;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

/**
 * @author chaigneauam
 *
 */
@Service
@Profile("ethereum")
public class CryptoServiceEthereum implements CryptoService {

	private static final Logger logger = LoggerFactory.getLogger(CryptoServiceEthereum.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see com.natixis.partageon.blockchain.CryptoService#genereKeyPair()
	 */
	@Override
	public KeyPair genereKeyPair() {
		ECKeyPair ecKeyPair;
		try {
			ecKeyPair = Keys.createEcKeyPair();
			String pubKey = Numeric.toHexStringWithPrefix(ecKeyPair.getPublicKey());
			String privKey = Numeric.toHexStringWithPrefix(ecKeyPair.getPrivateKey());

			logger.info("Generate PrivKey "+privKey);
			logger.info("Generate PubKey "+pubKey);
			return new KeyPair(privKey, pubKey);
		} catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException | NoSuchProviderException e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(e.getMessage());
		}
	}
}
