package com.natixis.partageon.blockchain.impl.ethereum;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;

/**
 * Created by ianniau on 26/01/2017.
 */
@Getter
@Setter
@Component
@Profile("ethereum")
public class EthereumProperties {

    @NotNull
    String BLOCKCHAIN_BASE_URL;

    @NotNull
    String BANK_PRIV_KEY;

    @NotNull
    String BANK_PUB_KEY;

    @NotNull
    String BANK_PASSPHRASE;

    BigInteger GAS_LIMIT = BigInteger.valueOf(1000000L);

    BigInteger GAS_PRICE = BigInteger.valueOf(1L);
}
