package com.natixis.partageon.blockchain.impl.ethereum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

/**
 * Created by ianniau on 15/02/2017.
 */

@Configuration
@PropertySource(value = "classpath:${config.folder}/ethereum.properties")
@Profile("ethereum")
public class EthereumConfig {

    @Autowired
    private Environment env;

    @Bean
    public EthereumProperties ethereumProperties() {
        EthereumProperties ethereumProperties = new EthereumProperties();
        ethereumProperties.setBLOCKCHAIN_BASE_URL(env.getProperty("ethereum.BLOCKCHAIN_BASE_URL"));
        ethereumProperties.setBANK_PRIV_KEY(env.getProperty("ethereum.BANK_PRIV_KEY"));
        ethereumProperties.setBANK_PUB_KEY(env.getProperty("ethereum.BANK_PUB_KEY"));
        ethereumProperties.setBANK_PASSPHRASE(env.getProperty("ethereum.BANK_PASSPHRASE"));
        return ethereumProperties;
    }
}
