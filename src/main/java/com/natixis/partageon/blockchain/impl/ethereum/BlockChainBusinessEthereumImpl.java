package com.natixis.partageon.blockchain.impl.ethereum;

import com.natixis.partageon.blockchain.BlockChainBusiness;
import com.natixis.partageon.blockchain.BlockChainService;
import com.natixis.partageon.blockchain.BlockchainUtils;
import com.natixis.partageon.blockchain.GamificationEvent;
import com.natixis.partageon.model.system.Deeps;
import com.natixis.partageon.utils.PartageOnConstantes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by ianniau on 26/01/2017.
 */

@Service
@Profile("ethereum")
public class BlockChainBusinessEthereumImpl implements BlockChainBusiness {
    
    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger(BlockChainBusinessEthereumImpl.class);
    
    private final BlockChainService blockChainService;
    
    private final EthereumProperties ethereumProperties;
    
    @Autowired
    public BlockChainBusinessEthereumImpl(@Qualifier(value = "blockChainServiceEthereumImpl") BlockChainService blockChainService
        , EthereumProperties ethereumProperties) {
        this.blockChainService = blockChainService;
        this.ethereumProperties = ethereumProperties;
    }
    
    @Override
    public Deeps getDeeps(String publicKey) {
        Deeps deeps = new Deeps();
        ResponseEntity<?> result = blockChainService.getAccountBalance(publicKey);
        if(result.getStatusCode().is2xxSuccessful()) {
            String str_balance = result.getBody().toString();
            BigInteger balance = new BigInteger(str_balance);
            deeps.setTotal(balance.longValue());
        } else {
            deeps.setTotal(0L);
        }
        deeps.setMonthly(5469L);
        return deeps;
    }
    
    @Override
    public List<String> getClassement() {
        return Collections.emptyList();
    }
    
    @Override
    public Map<String, Integer> crediteEvenement(GamificationEvent aEvent, String pK1, String pK2, Object aValue, boolean paiement) {
        Map<String,Integer> deepResult = BlockchainUtils.setRewardAmount(aEvent, aValue);
        
        int deepAmountInitiateur = deepResult.get(PartageOnConstantes.INITIATEUR);
        int deepAmountSuiveur = deepResult.get(PartageOnConstantes.SUIVEUR);
        if(paiement) {
            blockChainService.sendTransaction(ethereumProperties.getBANK_PUB_KEY(), pK1, deepAmountInitiateur);
            if(deepAmountSuiveur != 0 && null != pK2) {
                blockChainService.sendTransaction(ethereumProperties.getBANK_PUB_KEY(), pK2, deepAmountSuiveur);
            }
        }
        return deepResult;
    }
}
