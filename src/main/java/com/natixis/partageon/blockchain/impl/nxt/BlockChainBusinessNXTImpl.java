package com.natixis.partageon.blockchain.impl.nxt;

import com.natixis.partageon.blockchain.BlockChainBusiness;
import com.natixis.partageon.blockchain.BlockChainService;
import com.natixis.partageon.blockchain.BlockchainUtils;
import com.natixis.partageon.blockchain.GamificationEvent;
import com.natixis.partageon.model.system.Account;
import com.natixis.partageon.model.system.Deeps;
import com.natixis.partageon.utils.PartageOnConstantes;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 * Accès à la Smart bockchain NXT.

 * @author chaigneauam
 *
 */
@Getter
@Service
@Profile("nxt")
public class BlockChainBusinessNXTImpl implements BlockChainBusiness {
    
    private static final Logger logger = LoggerFactory.getLogger(BlockChainBusinessNXTImpl.class);
    
    @Autowired
    @Qualifier(value = "blockChainServiceNXTImpl")
    private BlockChainService blockChainService;
    
    final NxtProperties nxtProperties;
    
    private String BANK_ACCOUNT_ID;
    
    private String BANK_PUB_KEY;
    
    private String BANK_ACCOUNT_RS;
    
    private String BANK_PASSPHRASE;
    
    @Autowired
    public BlockChainBusinessNXTImpl(NxtProperties nxtProperties) {
        this.nxtProperties = nxtProperties;
        BANK_ACCOUNT_ID = nxtProperties.getBANK_ACCOUNT_ID();
        BANK_PUB_KEY = nxtProperties.getBANK_PUB_KEY();
        BANK_ACCOUNT_RS = nxtProperties.getBANK_ACCOUNT_RS();
        BANK_PASSPHRASE = nxtProperties.getBANK_PASSPHRASE();
        
    }
    
    /*
	 * (non-Javadoc)
	 *
	 * @see com.natixis.partageon.blockchain.BlockChainBusiness#getDeeps(com.natixis.partageon.model.system.KeyPair)
	 */
    @Override
    public Deeps getDeeps(String pubKey) {
        Deeps deeps = new Deeps();
        ResponseEntity<?> result = blockChainService.getAccountId(pubKey);
        if(blockChainService.checkResponseStatus(result)) {
            String accountRs = ((Account) result.getBody()).getAccountRS();
            ResponseEntity<?> account = blockChainService.getAccountBalance(accountRs);
            String balance = ((Account) account.getBody()).getBalanceNQT();
            logger.info(accountRs + " balance " + balance);
            if (null == balance)
                deeps.setTotal(0L);
            else
                deeps.setTotal(Long.parseLong(balance, 10));
        } else {
            deeps.setTotal(0L);
        }
        // TODO processing calculation for month
        deeps.setMonthly(5469L);
        return deeps;
    }
    
    /*
	 * (non-Javadoc)
	 *
	 * @see com.natixis.partageon.blockchain.BlockChainBusiness#getClassement()
	 */
    @Override
    public List<String> getClassement() {
        List<String> lst = new ArrayList<>();
        lst.add("PUBKEY1");
        lst.add("PUBKEY2");
        lst.add("PUBKEY3");
        lst.add("PUBKEY4");
        return lst;
    }
    
    /* (non-Javadoc)
	 * @see com.natixis.partageon.blockchain.SmartBlockChainBusiness#crediteEvenement(com.natixis.partageon.blockchain.GamificationEvent, java.lang.String, java.lang.String)
	 */
    @Override
    public Map<String,Integer> crediteEvenement(GamificationEvent event, String pK1, String pK2, Object value, boolean paiement) {
        Map<String,Integer> deepResult = BlockchainUtils.setRewardAmount(event, value);
        
        if(paiement) {
            reward_to_recipient(deepResult.get(PartageOnConstantes.INITIATEUR), pK1);
            if(deepResult.get(PartageOnConstantes.SUIVEUR) != 0 && null != pK2) {
                reward_to_recipient(deepResult.get(PartageOnConstantes.SUIVEUR), pK2);
            }
        }
        return deepResult;
    }
    
    /**
     *
     * @param deepAmount
     * @param pk
     */
    private void reward_to_recipient(int deepAmount, String pk) {
        ResponseEntity<?> blockChainAccount = blockChainService.getAccountId(pk);
        if(blockChainService.checkResponseStatus(blockChainAccount)) {
            Account account = ((Account) blockChainAccount.getBody());
            blockChainService.getAccountInfo(account.getAccount());
            blockChainService.sendTransaction(BANK_PASSPHRASE, account.getAccountRS(), deepAmount);
        }
    }
    
}
