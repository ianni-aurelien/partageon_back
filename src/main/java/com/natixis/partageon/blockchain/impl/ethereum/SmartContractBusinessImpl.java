package com.natixis.partageon.blockchain.impl.ethereum;

import com.natixis.partageon.blockchain.BlockChainBusiness;
import com.natixis.partageon.blockchain.BlockchainUtils;
import com.natixis.partageon.blockchain.GamificationEvent;
import com.natixis.partageon.exception.EndPointException;
import com.natixis.partageon.model.system.Deeps;
import com.natixis.partageon.utils.PartageOnConstantes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author ianniau
 *
 */
@Service
@Profile("not")
public class SmartContractBusinessImpl implements BlockChainBusiness {
    
    private final SmartContractServiceImpl smartContractServiceImpl;
    
    private static final Logger LOG = LoggerFactory.getLogger(SmartContractBusinessImpl.class);
    
    @Autowired
    public SmartContractBusinessImpl(SmartContractServiceImpl smartContractServiceImpl) {
        this.smartContractServiceImpl = smartContractServiceImpl;
    }
    
    /* (non-Javadoc)
	 * @see com.natixis.partageon.blockchain.SmartBlockChainBusiness#getDeeps(java.lang.String)
	 */
    @Override
    public Deeps getDeeps(String pK) {
        Deeps deeps = new Deeps();
        try {
            long amount = smartContractServiceImpl.getDeeps(pK);
            deeps.setTotal(amount);
        } catch (EndPointException e) {
            LOG.error(e.getMessage(), e);
            deeps.setTotal(0L);
        }
        deeps.setMonthly(5469L);
        return deeps;
    }
    
    /* (non-Javadoc)
	 * @see com.natixis.partageon.blockchain.SmartBlockChainBusiness#getClassement()
	 */
    @Override
    public List<String> getClassement() {
        // TODO Auto-generated method stub
        return null;
    }
    
    /* (non-Javadoc)
	 * @see com.natixis.partageon.blockchain.SmartBlockChainBusiness#crediteEvenement(com.natixis.partageon.model.system.GamificationEvent, java.lang.String, java.lang.String, java.lang.Object, boolean)
	 */
    @Override
    public Map<String, Integer> crediteEvenement(GamificationEvent aEvent,
                                                 String aPK1, String aPK2, Object aValue, boolean aPaiement) {
        
        Map<String,Integer> deepResult = BlockchainUtils.setRewardAmount(aEvent, aValue);
        
        int deepAmountInitiateur = deepResult.get(PartageOnConstantes.INITIATEUR);
        int deepAmountSuiveur = deepResult.get(PartageOnConstantes.SUIVEUR);
        if(aPaiement) {
            smartContractServiceImpl.credite(aPK1, deepAmountInitiateur);
            if(deepAmountSuiveur != 0 && null != aPK2) {
                smartContractServiceImpl.credite(aPK2, deepAmountSuiveur);
            }
        }
        return deepResult;
    }
    
}
