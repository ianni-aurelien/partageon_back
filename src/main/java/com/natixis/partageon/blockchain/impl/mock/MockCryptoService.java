package com.natixis.partageon.blockchain.impl.mock;

import com.natixis.partageon.blockchain.CryptoService;
import com.natixis.partageon.model.system.KeyPair;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Created by aurelien on 01/04/17.
 */
@Service
@Profile("mockblockchain")
public class MockCryptoService implements CryptoService {
    
    @Override
    public KeyPair genereKeyPair() {
        return new KeyPair("mockPrivateKey","mockPublicKey");
    }
}
