package com.natixis.partageon.blockchain.impl.ethereum;

import com.natixis.partageon.dao.impl.SmartContractDAO;
import com.natixis.partageon.exception.EndPointException;
import com.natixis.partageon.model.smartcontract.Partageon;
import com.natixis.partageon.model.system.UserSmartContractDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.Keys;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;

import javax.annotation.PostConstruct;
import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by aurelien on 29/04/17.
 */

@Service
@Profile("not")
public class SmartContractServiceImpl {
    
    private static final Logger LOG = LoggerFactory.getLogger(SmartContractServiceImpl.class);
    
    private final EthereumProperties ethereumProperties;
    
    private final SmartContractDAO smartContractDAO;
    
    private Partageon partageonContract;
    
    private BigInteger GAS_PRICE;
    private BigInteger GAS_LIMIT;
    
    private Web3j web3j;
    
    @Autowired
    public SmartContractServiceImpl(EthereumProperties ethereumProperties,
                                    SmartContractDAO smartContractDAO) {
        this.ethereumProperties = ethereumProperties;
        this.smartContractDAO = smartContractDAO;
    }
    
    @PostConstruct
    public void initSmartContractService() throws ExecutionException, InterruptedException {
        GAS_LIMIT = ethereumProperties.getGAS_LIMIT();
        GAS_PRICE = ethereumProperties.getGAS_PRICE();
        web3j = Web3j.build(new HttpService(ethereumProperties.getBLOCKCHAIN_BASE_URL()));
        initPartageonContract();
        LOG.debug(Partageon.class.getSimpleName());
    }
    
    private void initPartageonContract() throws ExecutionException, InterruptedException {
        
        List<UserSmartContractDetail> contracts = smartContractDAO.getOneContractAddressByOwner(
            Partageon.class.getSimpleName(),
            ethereumProperties.getBANK_PRIV_KEY()
        );
        Credentials credentials = Credentials.create(ethereumProperties.getBANK_PRIV_KEY());
        
        if(contracts.isEmpty()) {
            partageonContract = Partageon.deploy(web3j,
                credentials,
                GAS_PRICE,
                GAS_LIMIT,
                BigInteger.ZERO)
                .get();
            
            smartContractDAO.insertSmartContract(
                ethereumProperties.getBANK_PRIV_KEY(),
                partageonContract.getClass().getSimpleName(),
                null,
                partageonContract.getContractAddress()
            );
        } else {
            UserSmartContractDetail userSmartContractDetail = contracts.get(0);
            String address = userSmartContractDetail.getAddress();
            
            partageonContract = Partageon.load(address, web3j, credentials, GAS_PRICE, GAS_LIMIT);
        }
    }
    
    void credite(String aPK1, int amount) {
        partageonContract.addDeep(new Address(Keys.getAddress(aPK1)),
            new Uint256(BigInteger.valueOf(amount))
        );
    }
    
    long getDeeps(String aPK1) throws EndPointException {
        Uint256 amount;
        try {
            amount = partageonContract.getDeep(new Address(Keys.getAddress(aPK1))).get();
            return amount.getValue().longValue();
        } catch (InterruptedException | ExecutionException e) {
            throw new EndPointException(e);
        }
    }
}
