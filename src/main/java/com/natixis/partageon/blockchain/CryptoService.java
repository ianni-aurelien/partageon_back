package com.natixis.partageon.blockchain;

import com.natixis.partageon.model.system.KeyPair;

/**
 * @author chaigneauam
 *
 */
public interface CryptoService {

	/**
	 * @return une paire de clé privée/publique
	 */
	KeyPair genereKeyPair();

}
