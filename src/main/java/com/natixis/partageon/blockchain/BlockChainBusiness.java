package com.natixis.partageon.blockchain;

import com.natixis.partageon.model.system.Deeps;

import java.util.List;
import java.util.Map;

/**
 * 
 * Interface de service d'accès à la blockchain. En vue des smart contracts.
 * 
 * @author chaigneauam
 *
 */
public interface BlockChainBusiness {

	/**
	 * @param pK
	 * @return Deeps
	 */
	Deeps getDeeps(String pK);

	/**
	 * 
	 * @return List
	 */
	List<String> getClassement();

	/**
	 * @return Map
	 */
	Map<String, Integer> crediteEvenement(GamificationEvent aEvent, String aPK1, String aPK2, Object aValue, boolean aPaiement);

}
