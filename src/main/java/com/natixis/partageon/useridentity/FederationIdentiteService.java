package com.natixis.partageon.useridentity;

import com.natixis.partageon.model.system.UtilisateurFederationIdentite;

import javax.naming.NamingException;

/**
 *
 * Service d'accès au service de fédération des identité groupe.
 *
 *
 * @author chaigneauam
 *
 */
public interface FederationIdentiteService extends FederationIdentiteBusiness {

	/*
	 * (non-Javadoc)
	 *
	 * @see com.natixis.partageon.useridentity.FederationIdentiteBusiness#
	 * retrieveUserInfo(java.lang.String, java.lang.String)
	 */
	UtilisateurFederationIdentite retrieveUserInfo(String username,
                                                   String password) throws NamingException;

	UtilisateurFederationIdentite getElementFromCache(String fedUserId);

	void putElementInCache(String fedUserId, UtilisateurFederationIdentite userFed);

	String generateHashId(UtilisateurFederationIdentite userFed, String username);
}
