package com.natixis.partageon.useridentity;

import com.natixis.partageon.cache.WCacheManager;
import com.natixis.partageon.model.system.UtilisateurFederationIdentite;
import lombok.Setter;
import net.sf.ehcache.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;

/**
 * @author chaigneauam
 *
 */
@Service
@Setter
public class FederationIdentiteServiceImpl implements FederationIdentiteService {

	private static final Logger LOG = LoggerFactory.getLogger(FederationIdentiteServiceImpl.class);

	private FederationIdentiteBusiness fedBusiness;

	private WCacheManager cacheMgr;
    
    @Autowired
    public FederationIdentiteServiceImpl(FederationIdentiteBusiness fedBusiness,
                                         WCacheManager cacheMgr) {
        this.fedBusiness = fedBusiness;
        this.cacheMgr = cacheMgr;
    }
    
    /*
	 * (non-Javadoc)
	 *
	 * @see com.natixis.partageon.useridentity.FederationIdentiteBusiness#
	 * retrieveUserInfo(java.lang.String, java.lang.String)
	 */
	@Override
	public UtilisateurFederationIdentite retrieveUserInfo(String username,
														  String password) throws NamingException {
		UtilisateurFederationIdentite userFed;
		userFed = fedBusiness.retrieveUserInfo(username, password);
		String fedUserId = generateHashId(userFed, username);
		putElementInCache(fedUserId, userFed);
		return userFed;
	}

	@Override
	public String getServiceName() {
		return "composite";
	}

	public UtilisateurFederationIdentite getElementFromCache(String hash) {
		Element elem = cacheMgr.getCache(fedBusiness.getServiceName()).get(hash);
		if (elem == null) {
			return null;
		}
		return (UtilisateurFederationIdentite)elem.getObjectValue();
	}

	/**
	 * @param userFed
	 * @param username
	 * @return
	 */
	public String generateHashId(UtilisateurFederationIdentite userFed,
								 String username) {
		return username;
	}

	/* (non-Javadoc)
	 * @see com.natixis.partageon.useridentity.FederationIdentiteService#putElementInCache
	 * (java.lang.String, com.natixis.partageon.model.system.UtilisateurFederationIdentite)
	 */
	@Override
	public void putElementInCache(String fedUserId,
								  UtilisateurFederationIdentite userFed) {
		cacheMgr.getCache(fedBusiness.getServiceName())
            .put(new Element(fedUserId, userFed));
	}

}
