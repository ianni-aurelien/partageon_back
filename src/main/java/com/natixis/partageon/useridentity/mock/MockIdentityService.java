package com.natixis.partageon.useridentity.mock;

import com.natixis.partageon.model.system.UtilisateurFederationIdentite;
import com.natixis.partageon.useridentity.FederationIdentiteBusiness;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;

/**
 * Created by aurelien on 26/03/17.
 */
@Service
@Profile("mockldap")
public class MockIdentityService implements FederationIdentiteBusiness {
    
    @Override
    public UtilisateurFederationIdentite retrieveUserInfo(String user, String password) throws NamingException {
        UtilisateurFederationIdentite utilisateurFederationIdentite= new UtilisateurFederationIdentite();
        utilisateurFederationIdentite.setNom(user);
        utilisateurFederationIdentite.setPrenom(user);
        utilisateurFederationIdentite.setEmail(user+"@"+user+".com");
        return utilisateurFederationIdentite;
    }
    
    @Override
    public String getServiceName() {
        return "mockIdentityService";
    }
}
