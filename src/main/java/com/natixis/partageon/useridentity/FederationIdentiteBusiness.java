package com.natixis.partageon.useridentity;

import com.natixis.partageon.model.system.UtilisateurFederationIdentite;

import javax.naming.NamingException;

/**
 *
 * Service d'accès au service de fédération des identité groupe.
 *
 *
 * @author chaigneauam
 *
 */
public interface FederationIdentiteBusiness {

	UtilisateurFederationIdentite retrieveUserInfo(String user, String password) throws NamingException;

	String getServiceName();
}
