package com.natixis.partageon.useridentity.athena;

import com.natixis.partageon.useridentity.IdentityConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.List;
import java.util.Map;

/**
 * @author chaigneauam
 *
 */
@Configuration
@Profile("athena")
public class AthenaConfiguration {

	private Map<String,String> ldapConfiguration;

	private IdentityConfiguration idconf;

	private List<String> values;

	/**
	 * @return the ldapConfiguration
	 */
	public Map<String, String> getLdapConfiguration() {
		return ldapConfiguration;
	}

	/**
	 * @param ldapConfiguration the ldapConfiguration to set
	 */
	public void setLdapConfiguration(Map<String, String> ldapConfiguration) {
		this.ldapConfiguration = ldapConfiguration;
	}

	/**
	 * @return the idconf
	 */
	public IdentityConfiguration getIdconf() {
		return idconf;
	}

	/**
	 * @param idconf the idconf to set
	 */
	public void setIdconf(IdentityConfiguration idconf) {
		this.idconf = idconf;
	}

	/**
	 * @return the values
	 */
	public List<String> getValues() {
		return values;
	}

	/**
	 * @param values the values to set
	 */
	public void setValues(List<String> values) {
		this.values = values;
	}

}
