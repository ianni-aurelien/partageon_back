package com.natixis.partageon.useridentity.athena;

import com.natixis.partageon.useridentity.IdentityConfiguration;
import org.apache.commons.digester3.Digester;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author chaigneauam
 *
 */
@Component
@Profile("athena")
public class AthenaConfigurationParser implements FactoryBean<AthenaConfiguration> {

	private String configuration;

	/**
	 * @param configuration the configuration to set
	 */
	@Value("${ldap.configuration}")
	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	public static AthenaConfiguration parseConfiguration(String filename) throws IOException, SAXException{
		InputStream in=AthenaConfigurationParser.class.getClassLoader().getResourceAsStream(filename);

		Digester d=new Digester();
		d.addObjectCreate("athena-configuration", AthenaConfiguration.class);
		d.addObjectCreate("athena-configuration/ldap", HashMap.class);
		d.addCallMethod("athena-configuration/ldap/env", "put", 2);
		d.addCallParam("athena-configuration/ldap/env", 0, "key");
		d.addCallParam("athena-configuration/ldap/env", 1, "value");
		d.addSetNext("athena-configuration/ldap/env", "setLdapConfiguration");

		d.addObjectCreate("athena-configuration/identity", IdentityConfiguration.class);
		d.addBeanPropertySetter("athena-configuration/identity/principalDNPrefix", "principalDNPrefix");
		d.addBeanPropertySetter("athena-configuration/identity/principalDNSuffix", "principalDNSuffix");
		d.addSetNext("athena-configuration/identity", "setIdconf");

		d.addObjectCreate("athena-configuration/value-of-interest", ArrayList.class);
		d.addCallMethod("athena-configuration/value-of-interest/value", "add", 1);
		d.addCallParam("athena-configuration/value-of-interest/value", 0);
		d.addSetNext("athena-configuration/value-of-interest", "setValues");

		AthenaConfiguration conf= d.parse(in);
		return conf;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.FactoryBean#getObject()
	 */
	@Override
	public AthenaConfiguration getObject() throws Exception {
		AthenaConfiguration conf=parseConfiguration(configuration);
		return conf;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.FactoryBean#getObjectType()
	 */
	@Override
	public Class<?> getObjectType() {
		return AthenaConfiguration.class;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.FactoryBean#isSingleton()
	 */
	@Override
	public boolean isSingleton() {
		return true;
	}
}
