package com.natixis.partageon.useridentity.athena;

import com.natixis.partageon.model.system.UtilisateurFederationIdentite;
import com.natixis.partageon.useridentity.FederationIdentiteBusiness;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * @author chaigneauam
 *
 */
@Setter
@Service
@Profile("athena")
public class AthenaService implements FederationIdentiteBusiness {

	private static final Logger LOG = LoggerFactory.getLogger(AthenaService.class);

	private final AthenaConfiguration configuration;
    
    public AthenaService(AthenaConfiguration configuration) {
        this.configuration = configuration;
    }
    
	/*
	 * (non-Javadoc)
	 *
	 * @see com.natixis.partageon.useridentity.FederationIdentiteBusiness#
	 * retrieveUserInfo(java.lang.String, java.lang.String)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public UtilisateurFederationIdentite retrieveUserInfo(String userid, String password) throws NamingException {
		Hashtable env = new Hashtable();
		env.putAll(configuration.getLdapConfiguration());

		String cn = configuration.getIdconf().getPrincipalDNPrefix() + userid;
		String dn = cn + configuration.getIdconf().getPrincipalDNSuffix();
		env.put(Context.SECURITY_PRINCIPAL, dn);
		env.put(Context.SECURITY_CREDENTIALS, password);

		DirContext ctx = new InitialDirContext(env);

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);

		List<String> attrIDs = configuration.getValues();

		String[] attrIDsArray = new String[attrIDs.size()];
		int i = 0;
		for (String s : attrIDs) {
			attrIDsArray[i] = s;
			i++;
		}

		constraints.setReturningAttributes(attrIDsArray);

		NamingEnumeration answer = ctx.search(dn, cn, constraints);
		List<String> valuesOfInterest = configuration.getValues();
		Map<String, String> values = new HashMap<>();
		if (answer.hasMore()) {
			Attributes attrs = ((SearchResult) answer.next()).getAttributes();
			for (String key : valuesOfInterest) {
				Attribute attr = attrs.get(key);
				if (attr != null) {
					String value = (String) attr.get();
					values.put(key, value);
				}
			}
		}
		ctx.close();
		UtilisateurFederationIdentite user=new UtilisateurFederationIdentite();
		user.setEmail(values.get("mail"));
		user.setNom(values.get("sn"));
		user.setPrenom(values.get("givenname"));
		user.setSite(values.get("ah-populationType"));
		return user;

	}

	@Override
	public String getServiceName() {
		return "athena";
	}
}
