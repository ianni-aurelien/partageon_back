package com.natixis.partageon.useridentity;

/**
 * @author chaigneauam
 *
 */
public class IdentityConfiguration {

	private String principalDNPrefix;

	private String principalDNSuffix;

	/**
	 * @return the principalDNPrefix
	 */
	public String getPrincipalDNPrefix() {
		return principalDNPrefix;
	}

	/**
	 * @param principalDNPrefix the principalDNPrefix to set
	 */
	public void setPrincipalDNPrefix(String principalDNPrefix) {
		this.principalDNPrefix = principalDNPrefix;
	}

	/**
	 * @return the principalDNSuffix
	 */
	public String getPrincipalDNSuffix() {
		return principalDNSuffix;
	}

	/**
	 * @param principalDNSuffix the principalDNSuffix to set
	 */
	public void setPrincipalDNSuffix(String principalDNSuffix) {
		this.principalDNSuffix = principalDNSuffix;
	}

}
