package com.natixis.partageon.useridentity.fednxs;

import com.natixis.partageon.model.client.User;
import com.natixis.partageon.model.system.UtilisateurFederationIdentite;
import com.natixis.partageon.useridentity.FederationIdentiteBusiness;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;

/**
 * Created by ianniau on 22/03/2017.
 */
@Service
@Profile("fednxs")
public class SAMLService implements FederationIdentiteBusiness {

	private static final Logger LOG = LoggerFactory.getLogger(SAMLService.class);
 
	@Override
	public UtilisateurFederationIdentite retrieveUserInfo(String user, String password) throws NamingException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User principal = (User)auth.getPrincipal();
		if(user.equals(principal.getFedUserId())) {
			LOG.debug("auth getname: " + principal.getFedUserId());
			UtilisateurFederationIdentite userFed = new UtilisateurFederationIdentite();
			userFed.setNom(principal.getNom());
			userFed.setPrenom(principal.getPrenom());
			userFed.setEmail(principal.getEmail());
			return userFed;
		} else {
			throw new NamingException();
		}
	}

	@Override
	public String getServiceName() {
		return "fednxs";
	}
}
