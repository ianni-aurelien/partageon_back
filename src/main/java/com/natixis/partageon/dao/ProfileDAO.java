package com.natixis.partageon.dao;

import com.natixis.partageon.model.system.*;

import java.util.List;

/**
 * 
 * Interface de service d'accès à la base de données (DAO).
 * 
 * @author chaigneauam
 *
 */
public interface ProfileDAO {
    
    /**
     * retourne l'ensemble des media en base.
     *
     * @return liste des media
     */
    List<Media> listMedia();
    
    /**
     * retourne l'ensemble des media auxquels est lié l'utlisateur en base
     *
     * @return liste des media
     */
    List<Media> listMedia(Long userid);
    
    /**
     * retourne l'ensemble des sites en base.
     *
     * @return liste des sites
     */
    List<Site> listSites();
    
    /**
     * retourne l'ensemble des sites auxquels est lié l'utlisateur en base
     *
     * @return liste des sites
     */
    List<Site> listSites(Long userid);
    
    /**
     * retourne les préférences utilisateur.
     *
     * @return liste des media
     */
    PreferencesUtilisateur getProfil(Long userId);
    
    /**
     * @param fedUserId
     */
    Long getUserIdByFederationId(String fedUserId);
    
    /**
     * @param fedUserId
     */
    KeyPair getKeyPairByFederationId(String fedUserId);
    
    /**
     * @param fedUserId
     * @param pubKey
     * @param privKey
     * @return Long
     */
    Long createUserIdentite(String fedUserId, String pubKey, String privKey);
    
    /**
     * @param user
     */
    void createUserProfile(PreferencesUtilisateur user);
    
    void updateUserSites(PreferencesUtilisateur u);
    
    void updateUserMedia(PreferencesUtilisateur u);
    
    Avatar findAvatarById(Long img_id);
    
    Avatar findAvatarByTitle(String title);
    
    Avatar findAvatarByFedId(String fedId);
    
    List<Avatar> findAllAvatar();
    
    /**
     * @param profil
     */
    void modifyPreferences(PreferencesUtilisateur profil);
    
    UserIdentity findUserIdentityByFedId(String feduserid);
    
    /**
     * @param userid
     * @param mail
     */
    void updateUserMail(Long userid, String mail);
    
    /**
     * @param initiateurid
     * @return
     */
    String getMail(Long initiateurid);
    
    int setUserAvatarImg(PreferencesUtilisateur u);
    
    int setUserAvatarUri(PreferencesUtilisateur u);
    
    /**
     *
     * @param avatar
     * @return
     */
    Long addAvatar(Avatar avatar);
}
