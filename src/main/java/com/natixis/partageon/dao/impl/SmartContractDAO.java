package com.natixis.partageon.dao.impl;

import com.natixis.partageon.model.system.UserSmartContractDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

/**
 * Created by aurelien on 28/04/17.
 */
@Repository
@Profile("not")
public class SmartContractDAO {
    
    private static final Logger LOG = LoggerFactory.getLogger(SmartContractDAO.class);
    
    private final JdbcTemplate jdbcTemplate;
    
    private final NamedParameterJdbcOperations namedParameterJdbcOperations;
    
    @Value("${spring.datasource.platform}")
    private String dataSourceType;
    
    @Autowired
    public SmartContractDAO(JdbcTemplate jdbcTemplate,
                            NamedParameterJdbcOperations namedParameterJdbcOperations) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcOperations = namedParameterJdbcOperations;
    }
    
    public Long insertSmartContract(String ownerPrivKey,
                                    String name,
                                    String abi,
                                    String address) {
        Long id;
        if(dataSourceType.equals("hsql")) {
            id = jdbcTemplate.queryForObject(
                "SELECT NEXT VALUE FOR SMARTCONTRACT_seq from dual",
                Long.class);
        } else {
            id = jdbcTemplate.queryForObject(
                " SELECT SMARTCONTRACT_seq.nextval from dual",
                Long.class);
        }
        
        
        KeyHolder keyHolder = new GeneratedKeyHolder();
        if(dataSourceType.equals("hsql")) {
            final String INSERT_SMART_CONTRACT = "INSERT INTO SMARTCONTRACT " +
                "(id, name, abi, address) " +
                "VALUES (?,?,?,?)";
            jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps =
                        connection.prepareStatement(INSERT_SMART_CONTRACT,
                            Statement.RETURN_GENERATED_KEYS);
                    ps.setLong(1, id);
                    ps.setString(2, name);
                    ps.setString(3, abi);
                    ps.setString(4, address);
                    return ps;
                },
                keyHolder);
        } else {
            final String INSERT_SMART_CONTRACT = "INSERT INTO SMARTCONTRACT " +
                "(id, name, abi, address) " +
                "VALUES (:param1,:param2,:param3,:param4)";
            MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("param1", id)
                .addValue("param2", name)
                .addValue("param3", abi)
                .addValue("param4", address);
            namedParameterJdbcOperations.update(
                INSERT_SMART_CONTRACT,
                parameters,
                keyHolder,
                new String[]{"ID"}
            );
        }
        
        jdbcTemplate.update("INSERT INTO USERCONTRACT" +
            "(privKey, smartID) " +
            "VALUES (?,?)", ownerPrivKey, keyHolder.getKey());
        
        LOG.debug(keyHolder.getKey().longValue()+"");
        return keyHolder.getKey().longValue();
    }
    
    public List<UserSmartContractDetail> getOneContractAddressByOwner(String className, String ownerPrivKey) {
        LOG.debug("SmartContract Name: "+className);
        LOG.debug("SmartContract Owner: "+ownerPrivKey);
        List<UserSmartContractDetail> result = jdbcTemplate.query("SELECT " +
                "USERCONTRACT.smartid, " + "USERCONTRACT.privkey, " +
                "SMARTCONTRACT.name, " + "SMARTCONTRACT.abi, " +
                "SMARTCONTRACT.address " +
                "FROM USERCONTRACT " +
                "LEFT JOIN SMARTCONTRACT " +
                "ON USERCONTRACT.smartid = SMARTCONTRACT.id " +
                "WHERE SMARTCONTRACT.name = ? " +
                "AND USERCONTRACT.privkey = ? ",
            new String[]{className, ownerPrivKey},
            (rs,rowNum) -> new UserSmartContractDetail(
                rs.getLong("smartid"),
                rs.getString("privkey"),
                rs.getString("name"),
                rs.getString("abi"),
                rs.getString("address")
            )
        );
        LOG.debug(String.valueOf(result));
        
        return result;
    }
    
    public List<UserSmartContractDetail> getAllContract() {
        List<UserSmartContractDetail> result = jdbcTemplate.query("SELECT USERCONTRACT.smartID, " +
                "USERCONTRACT.privKey, SMARTCONTRACT.name, SMARTCONTRACT.abi, SMARTCONTRACT.address " +
                "FROM USERCONTRACT " +
                "LEFT JOIN SMARTCONTRACT " +
                "ON USERCONTRACT.smartID = SMARTCONTRACT.id",
            (rs,rowNum) -> new UserSmartContractDetail(
                rs.getLong("USERCONTRACT.smartID"),
                rs.getString("USERCONTRACT.privKey"),
                rs.getString("SMARTCONTRACT.name"),
                rs.getString("SMARTCONTRACT.abi"),
                rs.getString("SMARTCONTRACT.address")
            )
        );
        LOG.debug(String.valueOf(result));
        
        return result;
    }
    
    public List<UserSmartContractDetail> getOneContract(String className) {
        List<UserSmartContractDetail> result = jdbcTemplate.query("SELECT USERCONTRACT.smartID, " +
                "USERCONTRACT.privKey, SMARTCONTRACT.name, SMARTCONTRACT.abi, SMARTCONTRACT.address " +
                "FROM USERCONTRACT " +
                "LEFT JOIN SMARTCONTRACT " +
                "ON USERCONTRACT.smartID = SMARTCONTRACT.id " +
                "WHERE SMARTCONTRACT.name = (:className)", new String[]{className},
            (rs,rowNum) -> new UserSmartContractDetail(
                rs.getLong("USERCONTRACT.smartID"),
                rs.getString("USERCONTRACT.privKey"),
                rs.getString("SMARTCONTRACT.name"),
                rs.getString("SMARTCONTRACT.abi"),
                rs.getString("SMARTCONTRACT.address")
            )
        );
        LOG.debug(String.valueOf(result));
        
        return result;
    }
}
