package com.natixis.partageon.dao.impl;

import com.natixis.partageon.dao.PartageDAO;
import com.natixis.partageon.model.system.*;
import com.natixis.partageon.utils.FileUtils;
import lombok.Setter;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlQuery;
import org.springframework.jdbc.object.SqlUpdate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Types;
import java.util.*;

@Repository
@Setter
public class PartageDAOImpl implements PartageDAO, InitializingBean {

	private SqlQuery<Map<String, Object>> queryUserPartages;
    private SqlQuery<Map<String, Object>> queryAnnonces;
	private SqlQuery<Map<String, Object>> queryAnnoncesThemes;
	private SqlQuery<Map<String, Object>> queryAnnoncesThemesByAnnonce;
	private SqlQuery<Map<String, Object>> queryThemes;
	private SqlQuery<Long> queryAnnonceId;
	private SqlQuery<Long> queryMerId;
	private SqlQuery<Long> queryThemeId;
	private SqlQuery<Long> queryGetThemeIdByName;
	private SqlQuery<Map<String, Object>> queryAnnonceById;
	private SqlQuery<Map<String, Object>> queryMerById;
	private SqlUpdate lieAnnonce;
	private SqlUpdate abandonneMer;
	private SqlUpdate archiveMer;
	private SqlUpdate evalueMer;
	private SqlUpdate createAnnonce;
	private SqlUpdate clotureAnnonce;
	private SqlUpdate insertAnnonceTheme;
	private SqlUpdate insertTheme;
	private SqlUpdate deleteThemesAnnonce;
	private SqlUpdate updateAnnonceStatut;
	
	@Value("${spring.datasource.platform}")
	private String dataSourceType;

	private DataSource datasource;
    
    @Autowired
    public PartageDAOImpl(DataSource datasource) {
        this.datasource = datasource;
    }
    
    /**
	 * @param dataSourceType the dataSourceType to set
	 */
	public void setDataSourceType(String dataSourceType) {
		this.dataSourceType = dataSourceType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.dao.PartageDAO#listAnnonces(java.lang
	 * .Long)
	 */
	@Override
	public List<Annonce> listPartagesEtAnnonces(Long userid) {
		List<Map<String, Object>> lstPartages = queryUserPartages
				.execute(userid);
		List<Annonce> mpartages = new ArrayList<>();
		Map<Long, Annonce> mpartagesThemes = new HashMap<>();
		for (Map<String, Object> m : lstPartages) {
			Annonce p;
			if (m.get("MERID") == null) {
				p = new Annonce();
			} else {
				p = mapToPartage(m);
			}
			mapToAnnonce(m, p);
			mpartagesThemes.put(p.getAnnonceid(), p);
			mpartages.add(p);
		}
		ajouteThemes(mpartagesThemes);
		return mpartages;
	}

	/**
	 * @param m
	 * @return Annonce
	 */
	private Annonce mapToAnnonce(Map<String, Object> m, Annonce annonce) {
 		annonce.setAnnonceid(convert(m.get("ANNONCEID"), Long.class));
 		annonce.setTitle((String)m.get("TITLE"));
		Integer it=convert(m.get("OFFRE"), Integer.class);
		if (it == null || Integer.valueOf(0).equals(it)) {
			annonce.setType(AnnonceType.DEMANDE);
		} else {
			annonce.setType(AnnonceType.OFFRE);
		}
		annonce.setAnnonce((String) m.get("DESCRIPTION_ANNONCE"));
		annonce.setPostDt((Date) m.get("POSTDT"));
		annonce.setStatut(StatutAnnonce.valueOf(convert(m.get("STATUT_ANNONCE"), Integer.class)));
		Urgence urgence = new Urgence();
		urgence.setUrgencedate((Date) m.get("URGENCEDT"));
		Integer iurgence=convert(m.get("URGENCE"), Integer.class);
		urgence.setUrgence(iurgence == null
				|| iurgence.equals(0) ? Boolean.FALSE
				: Boolean.TRUE);
		annonce.setUrgence(urgence);
		PreferencesUtilisateur initiateur = new PreferencesUtilisateur();
		initiateur.setPseudo((String) m.get("INITIATEUR_PSEUDO"));
		initiateur.setFeduserid((String) m.get("INITIATEUR_FEDID"));
		initiateur.setUriAvatar((String) m.get("INITIATEUR_URIAVATAR"));
		initiateur.setUserid(convert(m.get("INITIATEUR_ID"), Long.class));
		initiateur.setCommentaire((String) m.get("INITIATEUR_COMMENTAIRE"));
		initiateur.setIndispoDebut((Date) m.get("INITIATEUR_INDISPODEBUT"));
		initiateur.setIndispoFin((Date) m.get("INITIATEUR_INDISPOFIN"));
		annonce.setInitiateur(initiateur);
		
		Integer nombrePartages=convert(m.get("NBPARTAGES"), Integer.class);
		annonce.setNombrePartages(nombrePartages);
		Double moyenneEvaluations=convert(m.get("MOYENNEEVALUATION"), Double.class);
		annonce.setMoyenneEvaluations(moyenneEvaluations);
		
		return annonce;
	}

	/**
	 * @param m
	 * @return Partage
	 */
	private Partage mapToPartage(Map<String, Object> m) {
		Partage p = new Partage();
		p.setPartageid(convert(m.get("MERID"), Long.class));
		p.setMatchDt((Date) m.get("MATCHDT"));
		p.setStatutmer(StatutAnnonce.valueOf(convert(m.get("STATUT_MER"), Integer.class)));
		PreferencesUtilisateur suiveur = new PreferencesUtilisateur();
		suiveur.setPseudo((String) m.get("SUIVEUR_PSEUDO"));
		suiveur.setUserid(convert(m.get("SUIVEUR_ID"), Long.class));
		suiveur.setFeduserid((String)m.get("SUIVEUR_FEDID"));
		suiveur.setUriAvatar((String) m.get("SUIVEUR_URIAVATAR"));
		suiveur.setCommentaire((String) m.get("SUIVEUR_COMMENTAIRE"));
		suiveur.setIndispoDebut((Date) m.get("SUIVEUR_INDISPODEBUT"));
		suiveur.setIndispoFin((Date) m.get("SUIVEUR_INDISPOFIN"));
		p.setSuiveur(suiveur);
		p.setEvaluation(convert(m.get("EVALUATION"), Integer.class));
		p.setCommentaireAbandon((String) m.get("COMMENTAIRE_ABANDON"));
		p.setCommentaireEvaluation((String) m.get("COMMENTAIRE_EVALUATION"));
		p.setCommentaireReponse((String) m.get("COMMENTAIRE_REPONSE"));
		return p;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.dao.PartageDAO#listAnnonces()
	 */
	@Override
	public List<Annonce> listAnnonces() {
		List<Map<String, Object>> lstPartages = queryAnnonces.execute();
		Map<Long, Annonce> mannonces = new HashMap<>();
		for (Map<String, Object> m : lstPartages) {
			Annonce annonce = new Annonce();
			mapToAnnonce(m, annonce);
			mannonces.put(annonce.getAnnonceid(), annonce);
		}
		ajouteThemes(mannonces);
		List<Annonce> result = new ArrayList<>();
		result.addAll(mannonces.values());
		return result;
	}

	/**
	 * @param mannonces
	 */
	private void ajouteThemes(Map<Long, ? extends Annonce> mannonces) {
		List<Map<String, Object>> lstThemes = queryAnnoncesThemes.execute();
		for (Map<String, Object> m : lstThemes) {
			Long id = convert(m.get("ANNONCEID"), Long.class);
			Annonce p = mannonces.get(id);
			if (p == null) {
				continue;
			}
			Theme theme = new Theme();
			theme.setId(convert(m.get("THEMEID"), Long.class));
			theme.setName((String) m.get("LIBELLE"));
			theme.setIsSystem(convert(m.get("IS_SYSTEM"), Boolean.class));
			p.getThemes().add(theme);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.dao.PartageDAO#listThemes()
	 */
	@Override
	public List<Theme> listThemes() {
		List<Theme> themes = new ArrayList<>();
		List<Map<String, Object>> lst = queryThemes.execute();
		for (Map<String, Object> m : lst) {
			Theme t = new Theme();
			t.setId(convert(m.get("ID"), Long.class));
			t.setName((String) m.get("LIBELLE"));
			t.setIsSystem(true);
			themes.add(t);
		}
		return themes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.dao.PartageDAO#createOrModifyAnnonce(
	 * Annonce)
	 */
	@Override
	public Annonce createOrModifyAnnonce(Annonce annonce) {
		Long annonceid = getAnnonceIdFromSequence();
		if (annonce.getAnnonceid() != null) {
			// modification
			Annonce aannonce = getAnnonceById(annonce.getAnnonceid());
			annonce.setType(aannonce.getType());
			annonce.setTitle(aannonce.getTitle());
			updateAnnonceStatut(annonce.getAnnonceid(),
					StatutAnnonce.DESACTIVEE);
			annonce.setStatut(StatutAnnonce.DISPONIBLE);
		}
		annonce.setAnnonceid(annonceid);
		Integer iTypeAnnonce = 1;
		if (annonce.getType() != null
				&& annonce.getType().equals(AnnonceType.DEMANDE)) {
			iTypeAnnonce = 0;
		}
		createAnnonce.update(annonce.getAnnonceid(), annonce.getTitle(), annonce.getInitiateur()
				.getUserid(), iTypeAnnonce, annonce.getUrgence() != null
				&& annonce.getUrgence().getUrgence() != null
				&& annonce.getUrgence().getUrgence() ? 1 : 0, annonce
				.getUrgence() != null ? annonce.getUrgence().getUrgencedate()
				: null, annonce.getPostDt(), annonce.getAnnonce(), 0);
		updateThemesAnnonce(annonce);
		return annonce;
	}

	/**
	 * @param annonceid
	 * @param statut
	 */
	private void updateAnnonceStatut(Long annonceid, StatutAnnonce statut) {
		updateAnnonceStatut.update(statut.getValue(), annonceid);
	}

	/**
	 * @param annonce
	 */
	public void updateThemesAnnonce(Annonce annonce) {
		deleteThemes(annonce.getAnnonceid());
		if (annonce.getThemes() != null && annonce.getThemes().size() != 0) {
			for (Theme t : annonce.getThemes()) {
                if (t.getIsSystem() == false) {
                    insertThemeIfNotExists(t);
                }
				insertTheme(annonce.getAnnonceid(), t);
			}
		}
	}

	/**
	 * @param t
	 */
	private void insertThemeIfNotExists(Theme t) {
        Long themeId;
        List<Long> themeIdList = queryGetThemeIdByName.execute(t.getName());
        if (themeIdList == null || themeIdList.isEmpty()) {
            themeId = queryMerId.findObject();
            insertTheme.update(themeId, t.getName(), t.getIsSystem());
        }
        else {
            themeId = themeIdList.get(0);
        }
        t.setId(themeId);
	}

	/**
	 * @param annonceid
	 * @param t
	 */
	private void insertTheme(Long annonceid, Theme t) {
		insertAnnonceTheme.update(t.getId(), annonceid);
	}

	/**
	 * @param annonceid
	 */
	private void deleteThemes(Long annonceid) {
		deleteThemesAnnonce.update(annonceid);
	}

	/**
	 * @param annonceid
	 * @return Annonce
	 */
	public Annonce getAnnonceById(Long annonceid) {
		Map<String, Object> m = queryAnnonceById.findObject(annonceid);
		Annonce annonce = new Annonce();
		mapToAnnonce(m, annonce);

		List<Theme> themes = getThemeByAnnonceId(annonceid);
		annonce.getThemes().addAll(themes);

		return annonce;
	}

	/**
	 * @return Long
	 */
	private Long getAnnonceIdFromSequence() {
        return queryAnnonceId.findObject();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.dao.PartageDAO#lieAnnonce(com.natixis
	 * .partageon.system.AnnonceReponse)
	 */
	@Override
	public Partage lieAnnonce(AnnonceReponse reponse) {
		Long merId = queryMerId.findObject();
		lieAnnonce.update(merId, reponse.getAnnonceid(), reponse.getUserid(),
				reponse.getMatchdate(), reponse.getCommentaire());
        return getMiseEnRelationById(merId);
	}

	/**
	 * @param merId
	 * @return
	 */
	@Override
	public Partage getMiseEnRelationById(Long merId) {
		Map<String, Object> m = queryMerById.findObject(merId);
		Partage p = mapToPartage(m);
		mapToAnnonce(m, p);

		List<Theme> themes = getThemeByAnnonceId(p.getAnnonceid());
		p.getThemes().addAll(themes);
		return p;
	}

	private List<Theme> getThemeByAnnonceId(Long annonceId) {
		List<Theme> themesList = new ArrayList<>();
		List<Map<String, Object>> mthemes = queryAnnoncesThemesByAnnonce
				.execute(annonceId);

		for (Map<String, Object> mtheme : mthemes) {
			Theme theme = new Theme();
			theme.setId(convert(mtheme.get("THEMEID"), Long.class));
			theme.setName((String) mtheme.get("LIBELLE"));
			themesList.add(theme);
		}
		return themesList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.dao.PartageDAO#abandonneMiseEnRelation
	 * (Partage)
	 */
	@Override
	public void abandonneMiseEnRelation(Partage partage) {
		abandonneMer.update(partage.getCommentaireAbandon(),
				partage.getLacheurId(), partage.getPartageid());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.dao.PartageDAO#archiveMiseEnRelation(
	 * Partage)
	 */
	@Override
	public void archiveMiseEnRelation(Partage partage) {
		archiveMer.update(partage.getPartageid());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.dao.PartageDAO#clotureAnnonce(com.natixis
	 * .partageon.system.Annonce)
	 */
	@Override
	public void clotureAnnonce(Annonce annonce) {
		clotureAnnonce.update(annonce.getAnnonceid());
	}

	/**
	 * 
	 * @param partage
	 * @return Partage
	 */
	@Override
	public Partage evalueMiseEnRelation(Partage partage) {
		evalueMer.update(partage.getEvaluation(), partage.getCommentaireEvaluation(), partage.getPartageid());
        return getMiseEnRelationById(partage.getPartageid());
	}
	
	@SuppressWarnings("unchecked")
	private <T> T convert(Object obj, Class<T> clazz){
		if(obj==null){
			return null;
		}
		ConvertUtilsBean c=new ConvertUtilsBean();
		return (T)c.convert(obj, clazz);
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		queryUserPartages = new SqlQuery<Map<String, Object>>(
				datasource,
				FileUtils
						.getFileContent("sql/"+dataSourceType+"/partages/list-user-partages.sql")) {
			@Override
			protected RowMapper<Map<String, Object>> newRowMapper(
					Object[] parameters, Map<?, ?> context) {
				return new ColumnMapRowMapper();
			}
		};
		queryUserPartages.setParameters(new SqlParameter[] { new SqlParameter(
				Types.BIGINT) });
        
        SqlQuery<Map<String, Object>> queryUserPartagesThemes = new SqlQuery<Map<String, Object>>(
            datasource,
            FileUtils
                .getFileContent("sql/" + dataSourceType + "/partages/list-user-partages-themes.sql")) {
            @Override
            protected RowMapper<Map<String, Object>> newRowMapper(
                Object[] parameters, Map<?, ?> context) {
                return new ColumnMapRowMapper();
            }
        };
		queryUserPartagesThemes
				.setParameters(new SqlParameter[] { new SqlParameter(
						Types.BIGINT) });

		queryAnnonces = new SqlQuery<Map<String, Object>>(datasource,
				FileUtils.getFileContent("sql/"+dataSourceType+"/partages/list-annonces.sql")) {
			@Override
			protected RowMapper<Map<String, Object>> newRowMapper(
					Object[] parameters, Map<?, ?> context) {
				return new ColumnMapRowMapper();
			}
		};

		queryAnnoncesThemes = new SqlQuery<Map<String, Object>>(
				datasource,
				FileUtils
						.getFileContent("sql/"+dataSourceType+"/partages/list-annonces-themes.sql")) {
			@Override
			protected RowMapper<Map<String, Object>> newRowMapper(
					Object[] parameters, Map<?, ?> context) {
				return new ColumnMapRowMapper();
			}
		};

		queryAnnoncesThemesByAnnonce = new SqlQuery<Map<String, Object>>(
				datasource,
				FileUtils
						.getFileContent("sql/"+dataSourceType+"/partages/list-annonce-themes.sql")) {
			@Override
			protected RowMapper<Map<String, Object>> newRowMapper(
					Object[] parameters, Map<?, ?> context) {
				return new ColumnMapRowMapper();
			}
		};

		queryAnnoncesThemesByAnnonce
				.setParameters(new SqlParameter[] { new SqlParameter(
						Types.BIGINT) });

		queryThemes = new SqlQuery<Map<String, Object>>(datasource,
				FileUtils.getFileContent("sql/"+dataSourceType+"/partages/list-themes.sql")) {
			@Override
			protected RowMapper<Map<String, Object>> newRowMapper(
					Object[] parameters, Map<?, ?> context) {
				return new ColumnMapRowMapper();
			}
		};

		queryAnnonceId = new SqlQuery<Long>(
				datasource,
				FileUtils
						.getFileContent("sql/"+dataSourceType+"/partages/create-annonce-id.sql")) {

			@Override
			protected RowMapper<Long> newRowMapper(Object[] parameters,
					Map<?, ?> context) {

				return (rs, rowNum) -> rs.getLong(1);
			}
		};

		queryGetThemeIdByName = new SqlQuery<Long>(datasource,
				FileUtils.getFileContent("sql/"+dataSourceType+"/partages/get-theme-id-by-libelle.sql")) {

			@Override
			protected RowMapper<Long> newRowMapper(Object[] parameters,
					Map<?, ?> context) {
				return (rs, rowNum) -> rs.getLong(1);
			}
		};
		queryGetThemeIdByName.setParameters(new SqlParameter[] { new SqlParameter(
				Types.VARCHAR) });

		queryMerId = new SqlQuery<Long>(datasource,
				FileUtils.getFileContent("sql/"+dataSourceType+"/partages/create-mer-id.sql")) {

			@Override
			protected RowMapper<Long> newRowMapper(Object[] parameters,
					Map<?, ?> context) {

				return (rs, rowNum) -> rs.getLong(1);
			}
		};

		queryThemeId = new SqlQuery<Long>(datasource,
				FileUtils.getFileContent("sql/"+dataSourceType+"/partages/create-theme-id.sql")) {

			@Override
			protected RowMapper<Long> newRowMapper(Object[] parameters,
					Map<?, ?> context) {
				return (rs, rowNum) -> rs.getLong(1);
			}
		};

		createAnnonce = new SqlUpdate(
				datasource,
				FileUtils
						.getFileContent("sql/"+dataSourceType+"/partages/create-annonce.sql"),
				new int[] { Types.BIGINT, Types.VARCHAR, Types.BIGINT, Types.INTEGER,
						Types.INTEGER, Types.TIMESTAMP, Types.TIMESTAMP,
						Types.VARCHAR, Types.INTEGER });

		queryAnnonceById = new SqlQuery<Map<String, Object>>(
				datasource,
				FileUtils
						.getFileContent("sql/"+dataSourceType+"/partages/get-annonce-by-id.sql")) {
			@Override
			protected RowMapper<Map<String, Object>> newRowMapper(
					Object[] parameters, Map<?, ?> context) {
				return new ColumnMapRowMapper();
			}
		};

		queryAnnonceById.setParameters(new SqlParameter[] { new SqlParameter(
				Types.BIGINT) });

		lieAnnonce = new SqlUpdate(datasource,
				FileUtils.getFileContent("sql/"+dataSourceType+"/partages/create-mer.sql"),
				new int[] { Types.BIGINT, Types.BIGINT, Types.BIGINT,
						Types.TIMESTAMP, Types.VARCHAR });

		queryMerById = new SqlQuery<Map<String, Object>>(datasource,
				FileUtils.getFileContent("sql/"+dataSourceType+"/partages/get-mer-by-id.sql")) {
			@Override
			protected RowMapper<Map<String, Object>> newRowMapper(
					Object[] parameters, Map<?, ?> context) {
				return new ColumnMapRowMapper();
			}
		};

		queryMerById.setParameters(new SqlParameter[] { new SqlParameter(
				Types.BIGINT) });

		abandonneMer = new SqlUpdate(
				datasource,
				FileUtils.getFileContent("sql/"+dataSourceType+"/partages/abandonne-mer.sql"),
				new int[] { Types.VARCHAR, Types.BIGINT, Types.BIGINT });

		archiveMer = new SqlUpdate(datasource,
				FileUtils.getFileContent("sql/"+dataSourceType+"/partages/archive-mer.sql"),
				new int[] { Types.BIGINT });

		evalueMer = new SqlUpdate(datasource,
				FileUtils.getFileContent("sql/"+dataSourceType+"/partages/evalue-mer.sql"),
				new int[] { Types.INTEGER, Types.VARCHAR, Types.BIGINT });

		clotureAnnonce = new SqlUpdate(
				datasource,
				FileUtils
						.getFileContent("sql/"+dataSourceType+"/partages/cloture-annonce.sql"),
				new int[] { Types.BIGINT });

		insertTheme = new SqlUpdate(
				datasource,
				FileUtils
						.getFileContent("sql/"+dataSourceType+"/partages/insert-theme.sql"),
				new int[] { Types.BIGINT, Types.VARCHAR, Types.INTEGER });

		insertAnnonceTheme = new SqlUpdate(
				datasource,
				FileUtils
						.getFileContent("sql/"+dataSourceType+"/partages/insert-themes-annonce.sql"),
				new int[] { Types.BIGINT, Types.BIGINT });

		deleteThemesAnnonce = new SqlUpdate(
				datasource,
				FileUtils
						.getFileContent("sql/"+dataSourceType+"/partages/delete-themes-annonce.sql"),
				new int[] { Types.BIGINT });

		updateAnnonceStatut = new SqlUpdate(
				datasource,
				FileUtils
						.getFileContent("sql/"+dataSourceType+"/partages/update-statut-annonce.sql"),
				new int[] { Types.INTEGER, Types.BIGINT });
	}

}
