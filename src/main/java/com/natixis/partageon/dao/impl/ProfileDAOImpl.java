package com.natixis.partageon.dao.impl;

import com.natixis.partageon.dao.ProfileDAO;
import com.natixis.partageon.mapper.Mapper;
import com.natixis.partageon.model.system.*;
import com.natixis.partageon.utils.FileUtils;
import lombok.Setter;
import org.hsqldb.jdbc.JDBCBlob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.object.SqlQuery;
import org.springframework.jdbc.object.SqlUpdate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * Implémentation de la couche d'accès à la base de données.
 *
 * @author chaigneauam
 *
 */
@Repository
@Setter
public class ProfileDAOImpl implements ProfileDAO, InitializingBean {
    
    private static final Logger logger = LoggerFactory.getLogger(ProfileDAOImpl.class);
    
    private SqlQuery<Map<String, Object>> querySites;
    private SqlQuery<Map<String, Object>> queryMedia;
    private SqlQuery<Map<String, Object>> queryProfil;
    private SqlQuery<Map<String, Object>> queryUserIdByFedId;
    private SqlQuery<Map<String, Object>> queryUserMedia;
    private SqlQuery<Map<String, Object>> queryUserSites;
    private SqlQuery<Long> queryUserId;
    private SqlQuery<Long> queryAvatarId;
    private SqlQuery<String> queryMail;
    private SqlUpdate insertUserIdentite;
    
    private SqlUpdate insertUserProfil;
    private SqlUpdate deleteSitesUpdate;
    
    private SqlUpdate deleteMediaUpdate;
    private SqlUpdate insertSiteUpdate;
    
    private SqlUpdate insertMediumUpdate;
    private SqlUpdate updateUser;
    
    private SqlUpdate updateUserMail;
    
    private JdbcTemplate jdbcTemplate;
    
    private final Mapper mapper = new Mapper();
    
    @Value("${spring.datasource.platform}")
    private String dataSourceType;
    private DataSource datasource;
    
    @Autowired
    public ProfileDAOImpl(@Qualifier("dataSource") DataSource dataSource) {
        this.datasource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.dao.PartageOnDAO#listMedia()
	 */
    @Override
    public List<Media> listMedia() {
        List<Map<String, Object>> ml = queryMedia.execute();
        List<Media> media = new ArrayList<>();
        for (Map<String, Object> m : ml) {
            Media medium = mapper.map(m, Media.class);
            media.add(medium);
        }
        return media;
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.dao.PartageOnDAO#listSites()
	 */
    @Override
    public List<Site> listSites() {
        List<Map<String, Object>> ml = querySites.execute();
        List<Site> sites = new ArrayList<>();
        for (Map<String, Object> m : ml) {
            Site site = mapper.map(m, Site.class);
            sites.add(site);
        }
        return sites;
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.dao.PartageOnDAO#getProfil(java.lang.Long)
	 */
    @Override
    public PreferencesUtilisateur getProfil(Long userId) {
        Map<String, Object> userm = queryProfil.findObject(userId);
        if (userm == null) {
            logger.info("getProfil: null");
            return null;
        }
        logger.info("getProfil: "+userm);
        PreferencesUtilisateur u = mapProfil(userm);
        logger.info("getProfil: "+u);
        return u;
    }
    
    /**
     * @param userm
     * @return PreferencesUtilisateur
     */
    private PreferencesUtilisateur mapProfil(Map<String, Object> userm) {
        PreferencesUtilisateur u = new PreferencesUtilisateur();
        u.setAnonyme(mapper.convert(userm.get("ANONYME"), Boolean.class));
        u.setFeduserid((String) userm.get("FEDUSERID"));
        u.setCommentaire((String) userm.get("COMMENTAIRE"));
        u.setIndispoDebut((Date) userm.get("INDISPODEBUT"));
        u.setIndispoFin((Date) userm.get("INDISPOFIN"));
        u.setPseudo((String) userm.get("PSEUDO"));
        u.setFeduserid((String) userm.get("FEDUSERID"));
        u.setUriAvatar((String) userm.get("URIAVATAR"));
        u.setTelephone((String) userm.get("TELEPHONE"));
        u.setMobile((String) userm.get("MOBILE"));
        Number tmp = (Number) userm.get("USERID");
        Long userId = tmp.longValue();
        u.setUserid(userId);
        List<Map<String, Object>> mlMedia = queryUserMedia.execute(userId);
        for (Map<String, Object> m : mlMedia) {
            Media media = mapper.map(m, Media.class);
            u.getMediaPrivilegies().add(media);
        }
        List<Map<String, Object>> mlSites = queryUserSites.execute(userId);
        for (Map<String, Object> m : mlSites) {
            Site site = mapper.map(m, Site.class);
            u.getSitesAccessibles().add(site);
        }
        Avatar avatar = findAvatarById((Long) userm.get("AVATARID"));
        u.setAvatar(avatar);
        
        return u;
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.dao.PartageOnDAO#listMedia(java.lang.Long)
	 */
    @Override
    public List<Media> listMedia(Long userid) {
        List<Map<String, Object>> ml = queryUserMedia.execute(userid);
        List<Media> media = new ArrayList<>();
        for (Map<String, Object> m : ml) {
            Media medium = mapper.map(m, Media.class);
            media.add(medium);
        }
        return media;
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.dao.PartageOnDAO#listSites(java.lang.Long)
	 */
    @Override
    public List<Site> listSites(Long userid) {
        List<Map<String, Object>> ml = queryUserSites.execute(userid);
        List<Site> sites = new ArrayList<>();
        for (Map<String, Object> m : ml) {
            Site site = mapper.map(m, Site.class);
            sites.add(site);
        }
        return sites;
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.dao.PartageOnDAO#getUserIdByFederationId(java.lang
	 * .String)
	 */
    @Override
    public Long getUserIdByFederationId(String fedUserId) {
        Map<String, Object> userm = queryUserIdByFedId.findObject(fedUserId);
        if (userm == null) {
            return null;
        }
        Number tmp = (Number) userm.get("ID");
        return tmp.longValue();
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.dao.PartageOnDAO#geKeyPairByFederationId(java.lang
	 * .String)
	 */
    @Override
    public KeyPair getKeyPairByFederationId(String fedUserId) {
        Map<String, Object> userm = queryUserIdByFedId.findObject(fedUserId);
        if (userm == null) {
            return null;
        }
        KeyPair kp = new KeyPair();
        kp.setPrivKey((String) userm.get("PRIVKEY"));
        kp.setPubKey((String) userm.get("PUBKEY"));
        return kp;
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.dao.PartageOnDAO#createUserIdentite(java.lang.String
	 * , java.lang.String, java.lang.String)
	 */
    @Override
    public Long createUserIdentite(String fedUserId, String pubKey,
                                   String privKey) {
        logger.info("createUserIdentiteDAO: "+fedUserId+ " "+ pubKey);
        Long userid = queryUserId.findObject();
        logger.info("createUserIdentiteDAO: "+userid);
        int n = insertUserIdentite.update(userid, fedUserId, pubKey, privKey);
        logger.info("createUserIdentiteDAO: update "+ n);
        return userid;
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.dao.PartageOnDAO#createUserProfile(java.lang.Long)
	 */
    @Override
    public void createUserProfile(PreferencesUtilisateur u) {
        insertUserProfil.update(u.getUserid(), u.getPseudo(), u.getTelephone(),
            u.getMobile(), u.getIndispoDebut(), u.getIndispoFin(),
            u.getCommentaire(), u.getAnonyme(), u.getUriAvatar());
        updateUserMedia(u);
        updateUserSites(u);
    }
    
    /**
     * @param u
     */
    public void updateUserSites(PreferencesUtilisateur u) {
        deleteSites(u.getUserid());
        if (u.getSitesAccessibles() != null
            && u.getSitesAccessibles().size() != 0) {
            for (Site s : u.getSitesAccessibles()) {
                insertSite(u.getUserid(), s);
            }
        }
    }
    
    /**
     * @param userid
     * @param s
     */
    private void insertSite(Long userid, Site s) {
        insertSiteUpdate.update(userid, s.getId());
    }
    
    /**
     * @param userid
     * @param m
     */
    private void insertMedia(Long userid, Media m) {
        insertMediumUpdate.update(userid, m.getId());
    }
    
    /**
     * @param u
     */
    public void updateUserMedia(PreferencesUtilisateur u) {
        deleteMedia(u.getUserid());
        if (u.getMediaPrivilegies() != null
            && u.getMediaPrivilegies().size() != 0) {
            for (Media m : u.getMediaPrivilegies()) {
                insertMedia(u.getUserid(), m);
            }
        }
    }
    
    @Override
    public int setUserAvatarImg(PreferencesUtilisateur u) {
        Long userId = getUserIdByFederationId(u.getFeduserid());
        Long avatarId = addAvatar(u.getAvatar());
        u.setAvatar(findAvatarById(avatarId));
        String sql = "UPDATE PREFERENCES_UTILISATEUR " +
            "SET avatarid = ?, uriavatar = null " +
            "WHERE userid = ?";
        int result = jdbcTemplate.update(sql,
            new Object[]{avatarId, userId},
            new int[]{Types.BIGINT, Types.BIGINT});
        return result;
    }
    
    @Override
    public int setUserAvatarUri(PreferencesUtilisateur u) {
        Long userId = getUserIdByFederationId(u.getFeduserid());
        String sql = "UPDATE PREFERENCES_UTILISATEUR " +
            "SET uriAvatar = ?, avatarid = null " +
            "WHERE userid = ?";
        int result = jdbcTemplate.update(sql,
            new Object[]{u.getUriAvatar(), userId},
            new int[]{Types.VARCHAR, Types.BIGINT});
        return result;
    }
    
    @Override
    public Long addAvatar(Avatar avatar) {
        JDBCBlob data = null;
        try {
            data = new JDBCBlob(avatar.getImg_data());
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }
        Long avatarId = queryAvatarId.findObject();
        int result = jdbcTemplate.update("INSERT INTO IMAGE_AVATAR (img_id, img_title, img_data) VALUES (?, ?, ?)",
            new Object[]{avatarId,
                avatar.getImg_title(),
                data
            },
            new int[]{Types.BIGINT, Types.VARCHAR, Types.BLOB});
        return avatarId;
    }
    
    @Override
    public Avatar findAvatarById(Long img_id) {
        Avatar avatar = new Avatar();
        if(img_id != null) {
            String sql = "SELECT * FROM IMAGE_AVATAR WHERE img_id = ?";
    
            avatar = (Avatar) jdbcTemplate.queryForObject(
                sql, new Object[]{img_id}, new BeanPropertyRowMapper(Avatar.class));
        }
        return avatar;
    }
    
    @Override
    public Avatar findAvatarByTitle(String img_title) {
        String sql = "SELECT * FROM IMAGE_AVATAR WHERE img_title = ? LIMIT 1";
    
        Avatar avatar = (Avatar)jdbcTemplate.queryForObject(
            sql, new Object[] { img_title }, new BeanPropertyRowMapper(Avatar.class));
        
        return avatar;
    }
    
    @Override
    public Avatar findAvatarByFedId(String fedId) {
        Avatar avatar = new Avatar();
        Long userId = getUserIdByFederationId(fedId);
        String sql = "SELECT avatarid FROM PREFERENCES_UTILISATEUR WHERE userid=?";
        String sql2 = "SELECT * FROM IMAGE_AVATAR WHERE img_id=?";
        Long avatarId = jdbcTemplate.queryForObject(
            sql, new Object[] { userId }, Long.class);
        if (avatarId != null) {
            avatar = (Avatar) jdbcTemplate.queryForObject(
                sql2, new Object[]{avatarId},
                new BeanPropertyRowMapper(Avatar.class));
        }
        return avatar;
    }
    
    @Override
    public List<Avatar> findAllAvatar() {
        String sql = "SELECT * FROM IMAGE_AVATAR";
        
        List<Avatar> avatar = jdbcTemplate.query(
            sql,
            new BeanPropertyRowMapper(Avatar.class));
        return avatar;
    }
    
    /**
     * @param userid
     */
    private void deleteSites(Long userid) {
        deleteSitesUpdate.update(userid);
    }
    
    /**
     * @param userid
     */
    private void deleteMedia(Long userid) {
        deleteMediaUpdate.update(userid);
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.dao.PartageOnDAO#modifyPreferences(com.natixis.
	 * partageon.system.PreferencesUtilisateur)
	 */
    @Override
    public void modifyPreferences(PreferencesUtilisateur profil) {
        if (profil.getUserid() != null) {
            updateUser.update(profil.getPseudo(), profil.getIndispoDebut(),
                profil.getIndispoFin(), profil.getCommentaire(),
                profil.getAnonyme(), profil.getUriAvatar(),
                profil.getTelephone(), profil.getMobile(),
                profil.getUserid());
            updateUserMedia(profil);
            updateUserSites(profil);
        }
    }
    
    @Override
    public UserIdentity findUserIdentityByFedId(String feduserid) {
        String sql = "SELECT * FROM IDENTITE_UTILISATEUR WHERE feduserid=?";
        
        UserIdentity userIdentity = (UserIdentity)jdbcTemplate.queryForObject(
            sql, new Object[] { feduserid },
            new BeanPropertyRowMapper(UserIdentity.class));
        return userIdentity;
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.natixis.partageon.dao.ProfileDAO#updateUserMail(java.lang
	 * .String, UtilisateurFederationIdentite)
	 */
    @Override
    public void updateUserMail(Long userid, String mail) {
        updateUserMail.update(mail, userid);
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see com.natixis.partageon.dao.ProfileDAO#getMail(java.lang.Long)
	 */
    @Override
    public String getMail(Long userid) {
        return queryMail.findObject(userid);
    }
    
    
    
    /* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
    @Override
    public void afterPropertiesSet() throws Exception {
        querySites = new SqlQuery<Map<String, Object>>(datasource,
            FileUtils.getFileContent("sql/"+dataSourceType+"/profile/list-sites.sql")) {
            @Override
            protected RowMapper<Map<String, Object>> newRowMapper(
                Object[] parameters, Map<?, ?> context) {
                return new ColumnMapRowMapper();
            }
        };
        
        queryMedia = new SqlQuery<Map<String, Object>>(datasource,
            FileUtils.getFileContent("sql/"+dataSourceType+"/profile/list-media.sql")) {
            @Override
            protected RowMapper<Map<String, Object>> newRowMapper(
                Object[] parameters, Map<?, ?> context) {
                return new ColumnMapRowMapper();
            }
        };
        
        queryProfil = new SqlQuery<Map<String, Object>>(datasource,
            FileUtils.getFileContent("sql/"+dataSourceType+"/profile/get-profil.sql")) {
            @Override
            protected RowMapper<Map<String, Object>> newRowMapper(
                Object[] parameters, Map<?, ?> context) {
                return new ColumnMapRowMapper();
            }
        };
        queryProfil.setParameters(new SqlParameter[] { new SqlParameter(
            Types.BIGINT) });
        
        queryUserIdByFedId = new SqlQuery<Map<String, Object>>(
            datasource,
            FileUtils
                .getFileContent("sql/"+dataSourceType+"/profile/get-userid-by-fedid.sql")) {
            @Override
            protected RowMapper<Map<String, Object>> newRowMapper(
                Object[] parameters, Map<?, ?> context) {
                return new ColumnMapRowMapper();
            }
        };
        queryUserIdByFedId.setParameters(new SqlParameter[] { new SqlParameter(
            Types.VARCHAR) });
        
        queryUserMedia = new SqlQuery<Map<String, Object>>(datasource,
            FileUtils.getFileContent("sql/"+dataSourceType+"/profile/get-user-media.sql")) {
            @Override
            protected RowMapper<Map<String, Object>> newRowMapper(
                Object[] parameters, Map<?, ?> context) {
                return new ColumnMapRowMapper();
            }
        };
        queryUserMedia.setParameters(new SqlParameter[] { new SqlParameter(
            Types.BIGINT) });
        
        queryUserSites = new SqlQuery<Map<String, Object>>(datasource,
            FileUtils.getFileContent("sql/"+dataSourceType+"/profile/get-user-sites.sql")) {
            @Override
            protected RowMapper<Map<String, Object>> newRowMapper(
                Object[] parameters, Map<?, ?> context) {
                return new ColumnMapRowMapper();
            }
        };
        queryUserSites.setParameters(new SqlParameter[] { new SqlParameter(
            Types.BIGINT) });
        
        queryUserId = new SqlQuery<Long>(datasource,
            FileUtils.getFileContent("sql/"+dataSourceType+"/profile/create-user-id.sql")) {
            
            @Override
            protected RowMapper<Long> newRowMapper(Object[] parameters,
                                                   Map<?, ?> context) {
                
                return (rs, rowNum) -> rs.getLong(1);
            }
        };
        
        queryMail = new SqlQuery<String>(datasource,
            FileUtils.getFileContent("sql/"+dataSourceType+"/profile/get-user-mail.sql")) {
            
            @Override
            protected RowMapper<String> newRowMapper(Object[] parameters,
                                                     Map<?, ?> context) {
                
                return (rs, rowNum) -> rs.getString(1);
            }
        };
        
        queryMail.setParameters(new SqlParameter[] { new SqlParameter(
            Types.VARCHAR) });
        
        queryAvatarId = new SqlQuery<Long>(datasource,
            FileUtils.getFileContent("sql/"+dataSourceType+"/profile/create-avatar-id.sql")) {
    
            @Override
            protected RowMapper<Long> newRowMapper(Object[] parameters,
                                                   Map<?, ?> context) {
        
                return (rs, rowNum) -> rs.getLong(1);
            }
        };
        
        insertUserIdentite = new SqlUpdate(
            datasource,
            FileUtils.getFileContent("sql/"+dataSourceType+"/profile/insert-user-id.sql"),
            new int[] { Types.BIGINT, Types.VARCHAR, Types.VARCHAR,
                Types.VARCHAR });
        insertUserProfil = new SqlUpdate(
            datasource,
            FileUtils
                .getFileContent("sql/"+dataSourceType+"/profile/insert-user-profil.sql"),
            new int[] { Types.BIGINT, Types.VARCHAR, Types.VARCHAR,
                Types.VARCHAR, Types.DATE, Types.DATE, Types.VARCHAR,
                Types.INTEGER, Types.VARCHAR});
        
        deleteMediaUpdate = new SqlUpdate(datasource,
            FileUtils.getFileContent("sql/"+dataSourceType+"/profile/delete-media.sql"),
            new int[] { Types.BIGINT });
        deleteSitesUpdate = new SqlUpdate(datasource,
            FileUtils.getFileContent("sql/"+dataSourceType+"/profile/delete-sites.sql"),
            new int[] { Types.BIGINT });
        
        insertSiteUpdate = new SqlUpdate(datasource,
            FileUtils.getFileContent("sql/"+dataSourceType+"/profile/insert-site.sql"),
            new int[] { Types.BIGINT, Types.BIGINT });
        insertMediumUpdate = new SqlUpdate(datasource,
            FileUtils.getFileContent("sql/"+dataSourceType+"/profile/insert-medium.sql"),
            new int[] { Types.BIGINT, Types.BIGINT });
        
        updateUser = new SqlUpdate(datasource,
            FileUtils.getFileContent("sql/"+dataSourceType+"/profile/update-user.sql"),
            new int[] { Types.VARCHAR, Types.DATE, Types.DATE,
                Types.VARCHAR, Types.INTEGER, Types.VARCHAR,
                Types.VARCHAR, Types.VARCHAR, Types.BIGINT});
        
        updateUserMail = new SqlUpdate(
            datasource,
            FileUtils
                .getFileContent("sql/"+dataSourceType+"/profile/update-user-mail.sql"),
            new int[] { Types.VARCHAR, Types.BIGINT });
    }
}
