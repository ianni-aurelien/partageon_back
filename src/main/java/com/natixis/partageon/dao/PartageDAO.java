package com.natixis.partageon.dao;

import com.natixis.partageon.model.system.Annonce;
import com.natixis.partageon.model.system.AnnonceReponse;
import com.natixis.partageon.model.system.Partage;
import com.natixis.partageon.model.system.Theme;

import java.util.List;

/**
 * 
 * Interface de service d'accès aux éléments de partages des utilisateurs.
 * 
 * @author chaigneauam
 *
 */
public interface PartageDAO {

	/**
	 * récupère les partages et annonces de l'utilisateur.
	 * 
	 * @param userid
	 * @return
	 */
	List<Annonce> listPartagesEtAnnonces(Long userid);

	/**
	 * récupère l'ensemble des annonces de tout le monde.
	 * 
	 * @return
	 */
	List<Annonce> listAnnonces();

	/**
	 * Création ou modification d'une annonce.
	 * 
	 * @param annonce
	 * @return
	 */
	Annonce createOrModifyAnnonce(Annonce annonce);

	/**
	 * Abandon d'une mise en relation.
	 * 
	 * @param partage
	 */
	void abandonneMiseEnRelation(Partage partage);

	/**
	 * Archivage d'une mise en relation.
	 * 
	 * @param partage
	 */
	void archiveMiseEnRelation(Partage partage);

	/**
	 * Evaluation d'une mise en relation.
	 * 
	 * @param partage
	 * @return 
	 */
	Partage evalueMiseEnRelation(Partage partage);

	/**
	 * Réponse à une annonce.
	 * 
	 * @param reponse
	 * @return
	 */
	Partage lieAnnonce(AnnonceReponse reponse);

	
	/**
	 * Récupération d'une mise en relation.
	 * 
	 * @param merId
	 * @return
	 */
	Partage getMiseEnRelationById(Long merId) ;

	/**
	 * Retourne la liste des thèmes.
	 * 
	 * @return
	 */
	List<Theme> listThemes();
	
	/**
	 * 
	 * met à jour les thèmes de l'annonce.
	 * 
	 * @param annonce
	 */
	 void updateThemesAnnonce(Annonce annonce);

	/**
	 * 
	 * Clôture une annonce.
	 * 
	 * @param annonce
	 */
	void clotureAnnonce(Annonce annonce);
	
	/**
	 * 
	 * 
	 * 
	 * @param annonceid
	 * @return
	 */
	Annonce getAnnonceById(Long annonceid);
}
