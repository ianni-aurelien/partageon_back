with args as(
	select ? as userid from dual
), mu as(
	select media_utilisateur.userid, mediaid from media_utilisateur inner join args on media_utilisateur.userid=args.userid
), mu1 as(
	select id, media from media inner join mu on media.id=mu.mediaid
)
select id, media from mu1