with args as(
	select ? as merid from dual
), mer as(
	select id as merid, annonceid, suiveurid as suiveur_id, matchdt, statut as statut_mer, evaluation, commentaire_reponse, commentaire_evaluation, commentaire_abandon
	from mise_en_relation inner join args on mise_en_relation.id=args.merid
), annonce_init as(
	select merid, annonce.id as annonceid, title, initiateurid, offre, description as description_annonce, creationdt as postdt, matchdt, statut as statut_annonce, urgence, urgencedt, suiveur_id,  statut_mer, evaluation, commentaire_reponse, commentaire_evaluation, commentaire_abandon
	from annonce inner join mer on annonce.id=mer.annonceid
), init_user as(
	select merid, annonceid, title, offre, description_annonce, postdt, matchdt, statut_annonce, urgence, urgencedt, pseudo as initiateur_pseudo, initiateurid as initiateur_id, uriavatar as initiateur_uriavatar, commentaire as initiateur_commentaire, indispodebut as initiateur_indispodebut, indispofin as initiateur_indispofin, suiveur_id,  statut_mer, evaluation, commentaire_reponse, commentaire_evaluation, commentaire_abandon
	from annonce_init inner join preferences_utilisateur on annonce_init.initiateurid=preferences_utilisateur.userid
), suiv_user as(
	select merid, annonceid, title, offre, description_annonce, postdt, matchdt, statut_annonce, urgence, urgencedt, initiateur_pseudo, initiateur_id, initiateur_uriavatar, initiateur_commentaire, initiateur_indispodebut, initiateur_indispofin, pseudo as suiveur_pseudo, suiveur_id, uriavatar as suiveur_uriavatar,  commentaire as suiveur_commentaire, indispodebut as suiveur_indispodebut, indispofin as suiveur_indispofin, statut_mer, evaluation, commentaire_reponse, commentaire_evaluation, commentaire_abandon
	from init_user inner join preferences_utilisateur on init_user.suiveur_id=preferences_utilisateur.userid
)
select * from suiv_user