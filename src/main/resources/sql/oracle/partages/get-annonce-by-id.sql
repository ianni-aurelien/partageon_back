with ann as(
select id as annonceid, title, initiateurid as initiateur_id, offre, urgence, urgencedt, creationdt as postdt, description as DESCRIPTION_ANNONCE, statut as STATUT_ANNONCE 
from annonce where id=?
), usr as (
	select pseudo as INITIATEUR_PSEUDO, uriavatar as INITIATEUR_URIAVATAR, annonceid, title, initiateur_id, offre, urgence, urgencedt, postdt, DESCRIPTION_ANNONCE, STATUT_ANNONCE from preferences_utilisateur inner join ann on preferences_utilisateur.userid=ann.initiateur_id
)
select * from usr