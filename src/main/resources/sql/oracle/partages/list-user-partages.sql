with args as(
	select ? as userid from dual
), mer_init as (
	select mise_en_relation.id as merid, annonce.id as annonceid, title, annonce.offre, annonce.description as DESCRIPTION_ANNONCE, mise_en_relation.matchdt, annonce.creationdt as postdt, annonce.statut as statut_annonce, mise_en_relation.statut as statut_mer, annonce.urgence, annonce.urgencedt, preferences_utilisateur.pseudo as INITIATEUR_PSEUDO, preferences_utilisateur.userid as INITIATEUR_ID, preferences_utilisateur.uriavatar as INITIATEUR_URIAVATAR, suiveurid, commentaire_reponse, commentaire_evaluation, commentaire_abandon, evaluation
	from mise_en_relation inner join annonce on mise_en_relation.annonceid=annonce.id 
		inner join args on annonce.initiateurid=args.userid 
		inner join preferences_utilisateur on args.userid=preferences_utilisateur.userid
), mer_init_user as (
	select merid, annonceid, title, offre, DESCRIPTION_ANNONCE, matchdt, postdt, statut_annonce, statut_mer, urgence, urgencedt, INITIATEUR_PSEUDO, INITIATEUR_ID, INITIATEUR_URIAVATAR, preferences_utilisateur.pseudo as SUIVEUR_PSEUDO, preferences_utilisateur.userid as SUIVEUR_ID, preferences_utilisateur.uriavatar as SUIVEUR_URIAVATAR, commentaire_reponse, commentaire_evaluation, commentaire_abandon, evaluation
	from mer_init inner join preferences_utilisateur on mer_init.suiveurid=preferences_utilisateur.userid
), mer_suiv as (
	select mise_en_relation.id as merid, annonce.id as annonceid, title, initiateurid, annonce.offre, annonce.description as DESCRIPTION_ANNONCE, mise_en_relation.matchdt, annonce.creationdt as postdt, annonce.statut as statut_annonce, mise_en_relation.statut as statut_mer, annonce.urgence, annonce.urgencedt, preferences_utilisateur.pseudo as SUIVEUR_PSEUDO, preferences_utilisateur.userid as SUIVEUR_ID, preferences_utilisateur.uriavatar as SUIVEUR_URIAVATAR, suiveurid, commentaire_reponse, commentaire_evaluation, commentaire_abandon, evaluation
	from mise_en_relation inner join annonce on mise_en_relation.annonceid=annonce.id 
		inner join args on mise_en_relation.suiveurid=args.userid 
		inner join preferences_utilisateur on args.userid=preferences_utilisateur.userid
), mer_suiv_user as (
	select merid, annonceid, title, offre, DESCRIPTION_ANNONCE, matchdt, postdt, statut_annonce, statut_mer, urgence, urgencedt, preferences_utilisateur.pseudo as INITIATEUR_PSEUDO, preferences_utilisateur.userid as INITIATEUR_ID, preferences_utilisateur.uriavatar as INITIATEUR_URIAVATAR, SUIVEUR_PSEUDO, SUIVEUR_ID, SUIVEUR_URIAVATAR, commentaire_reponse, commentaire_evaluation, commentaire_abandon, evaluation
	from mer_suiv inner join preferences_utilisateur on mer_suiv.initiateurid=preferences_utilisateur.userid
), ann as (
 	select cast(null as number) as merid, id as annonceid, title, offre, description as DESCRIPTION_ANNONCE, cast (null as timestamp ) as matchdt, creationdt as postdt, statut as statut_annonce, cast(null as integer) as statut_mer, urgence, urgencedt, pseudo as INITIATEUR_PSEUDO, preferences_utilisateur.userid as INITIATEUR_ID, uriavatar as INITIATEUR_URIAVATAR, null as SUIVEUR_PSEUDO, cast(null as number) as SUIVEUR_ID, null as SUIVEUR_URIAVATAR, null as  commentaire_reponse, null as commentaire_evaluation, null as commentaire_abandon, cast(null as integer) as evaluation
 	from annonce inner join args on args.userid=annonce.initiateurid
 		inner join preferences_utilisateur on annonce.initiateurid=preferences_utilisateur.userid
), all_union as(
   select * from mer_init_user
   union all
   select * from mer_suiv_user
   union all
   select * from ann
)
select * from all_union