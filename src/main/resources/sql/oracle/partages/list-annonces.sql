with req as(
select annonce.id as annonceid, title, offre, description as DESCRIPTION_ANNONCE, creationdt as postdt, statut as STATUT_ANNONCE, urgence, urgencedt, pseudo as INITIATEUR_PSEUDO, uriavatar as INITIATEUR_URIAVATAR
 	from annonce inner join preferences_utilisateur on annonce.initiateurid=preferences_utilisateur.userid 
), mei1 as(
	select req.annonceid, avg(cast(mise_en_relation.evaluation as number)) as moyenneevaluation, count(mise_en_relation.annonceid) as nbpartages
	from mise_en_relation right outer join req on mise_en_relation.annonceid=req.annonceid
	group by req.annonceid
), t1 as(
	select req.annonceid, title, offre, DESCRIPTION_ANNONCE, postdt, STATUT_ANNONCE, urgence, urgencedt, INITIATEUR_PSEUDO, INITIATEUR_URIAVATAR, mei1.moyenneevaluation, mei1.nbpartages
	from req left outer join mei1 on req.annonceid=mei1.annonceid
)
select * from t1 order by urgence desc, urgencedt asc,postdt desc