
delete from IDENTITE_UTILISATEUR;

insert into IDENTITE_UTILISATEUR(id,feduserid,pubkey,privkey)values(0,'ausername','PUBKEY','PRIVKEY');

insert into IDENTITE_UTILISATEUR(id,feduserid,pubkey,privkey)values(1,'ausernam1','PUBKE1','PRIVKE1');

insert into IDENTITE_UTILISATEUR(id,feduserid,pubkey,privkey)values(2,'ausernam2','PUBKE2','PRIVKE2');

insert into IDENTITE_UTILISATEUR(id,feduserid,pubkey,privkey)values(3,'ausernam3','PUBKE3','PRIVKE3');

delete from preferences_utilisateur;

insert into preferences_utilisateur(userid, pseudo, telephone, mobile, indispodebut, indispofin,commentaire, anonyme, uriavatar,mail)values(0,'Dark1', '01.46.08.60.00', '06.12.34.56.78',null, null,'Les Lundis & Mercredi entre 12h00 & 14h00.', 0, './images/avatar/av7.png','dark1@natixis.com');

insert into preferences_utilisateur(userid, pseudo, telephone, mobile, indispodebut, indispofin,commentaire, anonyme, uriavatar,mail)values(1,'Red1', '01.46.08.60.01', '06.12.34.56.71',null, null,'Les Lundis & Mercredi entre 12h00 & 14h00.', 0, './images/avatar/av6.png','red1@natixis.com');

insert into preferences_utilisateur(userid, pseudo, telephone, mobile, indispodebut, indispofin,commentaire, anonyme, uriavatar,mail)values(2,'Er1', '01.46.08.60.02', '06.12.34.56.72',null, null,'Les Lundis & Mercredi entre 12h00 & 14h00.', 0, './images/avatar/av3.png','er1@natixis.com');

insert into preferences_utilisateur(userid, pseudo, telephone, mobile, indispodebut, indispofin,commentaire, anonyme, uriavatar,mail)values(3,'Er2', '01.46.08.60.03', '06.12.34.56.73',null, null,'Les Lundis & Mercredi entre 12h00 & 14h00.', 0, './images/avatar/av4.png','er2@natixis.com');

delete from media_utilisateur;

insert into media_utilisateur(userid, mediaid)values(0,0);

insert into media_utilisateur(userid, mediaid)values(0,1);

insert into media_utilisateur(userid, mediaid)values(0,3);

insert into media_utilisateur(userid, mediaid)values(1,3);

insert into media_utilisateur(userid, mediaid)values(2,3);

insert into media_utilisateur(userid, mediaid)values(2,2);

insert into media_utilisateur(userid, mediaid)values(3,2);

delete from sites_utilisateur;

insert into sites_utilisateur(userid, siteid)values(0,0);

insert into sites_utilisateur(userid, siteid)values(0,2);

insert into sites_utilisateur(userid, siteid)values(0,8);

insert into sites_utilisateur(userid, siteid)values(0,1);

insert into sites_utilisateur(userid, siteid)values(1,8);

insert into sites_utilisateur(userid, siteid)values(1,1);

insert into sites_utilisateur(userid, siteid)values(2,1);

insert into sites_utilisateur(userid, siteid)values(3,8);

