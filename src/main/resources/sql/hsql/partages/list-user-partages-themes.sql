with args as(
	select ? as userid from dual
), mer_init as (
	select annonce.id
	from mise_en_relation inner join annonce on mise_en_relation.annonceid=annonce.id 
		inner join args on annonce.initiateurid=args.userid
), mer_suiv as (
	select annonce.id
	from mise_en_relation inner join annonce on mise_en_relation.annonceid=annonce.id 
		inner join args on mise_en_relation.suiveurid=args.userid
), ann as (
 	select id
 	from annonce inner join args on args.userid=annonce.initiateurid
), all_ann as(
   select * from mer_init
   union
   select * from mer_suiv
   union
   select * from ann
), themes as(
	select id as annonceid, themeid from all_ann inner join themes_annonce on all_ann.id=themes_annonce.annonceid WHERE is_system = 1
)
select annonceid, themeid from themes
