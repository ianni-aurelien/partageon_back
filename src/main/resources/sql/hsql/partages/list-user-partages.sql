with args AS(
    SELECT ? AS userid from dual
), mer_init AS (
    SELECT mise_en_relation.id AS merid,
        annonce.id AS annonceid,
        title,
        annonce.offre,
        annonce.descriptiON AS DESCRIPTION_ANNONCE,
        mise_en_relation.matchdt,
        annonce.creationdt AS postdt,
        annonce.statut AS statut_annonce,
        mise_en_relation.statut AS statut_mer,
        annonce.urgence, annonce.urgencedt,
        preferences_utilisateur.pseudo AS INITIATEUR_PSEUDO,
        preferences_utilisateur.userid AS INITIATEUR_ID,
        identite_utilisateur.feduserid AS INITIATEUR_FEDID,
        preferences_utilisateur.uriavatar AS INITIATEUR_URIAVATAR,
        suiveurid,
        commentaire_reponse,
        commentaire_evaluation,
        commentaire_abandon,
        evaluation
    FROM mise_en_relation
        INNER JOIN annonce ON mise_en_relation.annonceid = annonce.id
        INNER JOIN args ON annonce.initiateurid = args.userid
        INNER JOIN preferences_utilisateur ON args.userid = preferences_utilisateur.userid
        INNER JOIN identite_utilisateur ON args.userid = identite_utilisateur.id
), mer_init_user AS (
    SELECT merid,
        annonceid,
        title,
        offre,
        DESCRIPTION_ANNONCE,
        matchdt,
        postdt,
        statut_annonce,
        statut_mer,
        urgence,
        urgencedt,
        INITIATEUR_PSEUDO,
        INITIATEUR_ID,
        INITIATEUR_FEDID,
        INITIATEUR_URIAVATAR,
        preferences_utilisateur.pseudo AS SUIVEUR_PSEUDO,
        preferences_utilisateur.userid AS SUIVEUR_ID,
        identite_utilisateur.feduserid AS SUIVEUR_FEDID,
        preferences_utilisateur.uriavatar AS SUIVEUR_URIAVATAR,
        commentaire_reponse,
        commentaire_evaluation,
        commentaire_abandon,
        evaluation
    FROM mer_init 
    INNER JOIN preferences_utilisateur ON mer_init.suiveurid = preferences_utilisateur.userid
    INNER JOIN identite_utilisateur ON mer_init.suiveurid = identite_utilisateur.id
), mer_suiv AS (
    SELECT mise_en_relation.id AS merid,
        annonce.id AS annonceid,
        title,
        initiateurid,
        annonce.offre,
        annonce.description AS DESCRIPTION_ANNONCE,
        mise_en_relation.matchdt,
        annonce.creationdt AS postdt,
        annonce.statut AS statut_annonce,
        mise_en_relation.statut AS statut_mer,
        annonce.urgence, annonce.urgencedt,
        preferences_utilisateur.pseudo AS SUIVEUR_PSEUDO,
        preferences_utilisateur.userid AS SUIVEUR_ID,
        identite_utilisateur.feduserid AS SUIVEUR_FEDID,
        preferences_utilisateur.uriavatar AS SUIVEUR_URIAVATAR,
        suiveurid,
        commentaire_reponse,
        commentaire_evaluation,
        commentaire_abandon,
        evaluation
    FROM mise_en_relation
        INNER JOIN annonce ON mise_en_relation.annonceid = annonce.id
        INNER JOIN args ON mise_en_relation.suiveurid = args.userid
        INNER JOIN preferences_utilisateur ON args.userid = preferences_utilisateur.userid
        INNER JOIN identite_utilisateur ON args.userid = identite_utilisateur.id
), mer_suiv_user AS (
    SELECT merid,
        annonceid,
        title,
        offre,
        DESCRIPTION_ANNONCE,
        matchdt,
        postdt,
        statut_annonce,
        statut_mer,
        urgence,
        urgencedt,
        preferences_utilisateur.pseudo AS INITIATEUR_PSEUDO,
        preferences_utilisateur.userid AS INITIATEUR_ID,
        identite_utilisateur.feduserid AS INITIATEUR_FEDID,
        preferences_utilisateur.uriavatar AS INITIATEUR_URIAVATAR,
        SUIVEUR_PSEUDO,
        SUIVEUR_ID,
        SUIVEUR_FEDID,
        SUIVEUR_URIAVATAR,
        commentaire_reponse,
        commentaire_evaluation,
        commentaire_abandon,
        evaluation
    FROM mer_suiv
        INNER JOIN preferences_utilisateur ON mer_suiv.initiateurid = preferences_utilisateur.userid
        INNER JOIN identite_utilisateur ON mer_suiv.initiateurid = identite_utilisateur.id
), ann AS (
    SELECT cast(null AS bigint) AS merid,
        id AS annonceid,
        title,
        offre,
        description AS DESCRIPTION_ANNONCE,
        cast (null AS timestamp) AS matchdt,
        creatiONdt AS postdt,
        statut AS statut_annonce,
        cast(null AS integer) AS statut_mer,
        urgence,
        urgencedt,
        pseudo AS INITIATEUR_PSEUDO,
        preferences_utilisateur.userid AS INITIATEUR_ID,
        identite_utilisateur.feduserid AS INITIATEUR_FEDID,
        uriavatar AS INITIATEUR_URIAVATAR, null AS SUIVEUR_PSEUDO,
        cast(null AS BIGINT) AS SUIVEUR_ID,
        null AS SUIVEUR_FEDID,
        null AS SUIVEUR_URIAVATAR,
        null AS  commentaire_reponse,
        null AS commentaire_evaluation,
        null AS commentaire_abandon,
        cast(null AS integer) AS evaluation
    FROM annonce
        INNER JOIN args ON args.userid = annonce.initiateurid
        INNER JOIN preferences_utilisateur ON annonce.initiateurid = preferences_utilisateur.userid
        INNER JOIN identite_utilisateur ON annonce.initiateurid = identite_utilisateur.id
), all_union AS(
    SELECT * FROM mer_init_user
    UNION ALL
    SELECT * FROM mer_suiv_user
    UNION ALL
    SELECT * FROM ann
)
SELECT * FROM all_union
