WITH req AS(
    SELECT annonce.id AS annonceid,
        IDENTITE_UTILISATEUR.FEDUSERID,
        title,
        offre,
        description AS DESCRIPTION_ANNONCE,
        creationdt AS postdt,
        statut AS STATUT_ANNONCE,
        urgence,
        urgencedt,
        pseudo AS INITIATEUR_PSEUDO,
        uriavatar AS INITIATEUR_URIAVATAR
    FROM annonce
    INNER JOIN PREFERENCES_UTILISATEUR ON ANNONCE.INITIATEURID = PREFERENCES_UTILISATEUR.USERID
    INNER JOIN IDENTITE_UTILISATEUR ON ANNONCE.INITIATEURID = IDENTITE_UTILISATEUR.ID
), mei1 AS(
    SELECT req.annonceid,
        avg(cast(mise_en_relation.evaluation AS DOUBLE)) AS moyenneevaluation,
        count(mise_en_relation.annonceid) AS nbpartages
    FROM mise_en_relation
    RIGHT OUTER JOIN req ON mise_en_relation.annonceid = req.annonceid
    GROUP BY req.annonceid
), t1 AS(
    SELECT req.annonceid,
        req.feduserid as INITIATEUR_FEDID,
        title,
        offre,
        description_annonce,
        postdt,
        statut_annonce,
        urgence,
        urgencedt,
        initiateur_pseudo,
        initiateur_uriavatar,
        mei1.moyenneevaluation,
        mei1.nbpartages
    FROM req
    LEFT OUTER JOIN mei1 ON req.annonceid=mei1.annonceid
)
SELECT * FROM t1 ORDER BY urgence DESC, urgencedt ASC, postdt DESC;
