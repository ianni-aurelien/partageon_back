
CREATE SEQUENCE annonceid_seq AS BIGINT START WITH 50 INCREMENT BY 1;

CREATE SEQUENCE miseenrelationid_seq AS BIGINT START WITH 50 INCREMENT BY 1;

CREATE SEQUENCE themeid_seq AS BIGINT START WITH 50 INCREMENT BY 1;

create table THEME(
	id bigint primary key,
	libelle varchar(256),
	is_system int DEFAULT 0
);


create table STATUT(
	id bigint primary key,
	libelle varchar(256)
);

create table ANNONCE(
	 id bigint primary key,
	 title varchar(128),
	 initiateurid bigint,
	 offre int,
	 urgence int,
	 urgencedt date,
	 creationdt timestamp,
	 description varchar(1024),
	 statut int,
	 foreign key(initiateurid) references IDENTITE_UTILISATEUR(id) ON DELETE CASCADE,
	 foreign key(statut) references STATUT(id)
);

create table themes_annonce(
	themeid bigint,
	annonceid bigint,
	foreign key(themeid) references THEME(id),
	foreign key(annonceid) references annonce(id)
);

create table MISE_EN_RELATION(
	id bigint primary key,
	annonceid bigint,
	suiveurid bigint,
	lacheurid bigint,
	matchdt timestamp,
	evaluation int,
	commentaire_reponse varchar(1024),
	commentaire_evaluation varchar(1024),
	commentaire_abandon varchar(1024),
	statut int,
	foreign key(annonceid) references ANNONCE(id),
	foreign key(suiveurid) references IDENTITE_UTILISATEUR(id) ON DELETE CASCADE,
    foreign key(lacheurid) references IDENTITE_UTILISATEUR(id) ON DELETE CASCADE,
    foreign key(statut) references STATUT(id)
);

