
INSERT INTO annonce(id, title, initiateurid, offre, urgence, urgencedt, creationdt, description, statut)
VALUES(0, 'site web', 0, 1, 0,
       null, to_date('07/12/2017','DD/MM/YYYY'),
       'je propose des cours de développement web.',0);

INSERT INTO annonce(id, title, initiateurid, offre, urgence, urgencedt, creationdt, description, statut)
VALUES(1, 'conduite', 1, 0, 1,
       to_date('25/12/2017','DD/MM/YYYY'), to_date('07/12/2017','DD/MM/YYYY'),
       'je cherche des cours de conduite voiture. (c''est urgent, merci)',0);

INSERT INTO annonce(id, title, initiateurid, offre, urgence, urgencedt, creationdt, description, statut)
VALUES(2, 'site web', 1, 0, 0,
       null, to_date('07/12/2017','DD/MM/YYYY'),
       'je cherche des cours de angularjs.',0);

INSERT INTO themes_annonce(themeid, annonceid)VALUES(0,0);

INSERT INTO themes_annonce(themeid, annonceid)VALUES(1,0);

INSERT INTO themes_annonce(themeid, annonceid)VALUES(2,0);

INSERT INTO themes_annonce(themeid, annonceid)VALUES(3,1);

INSERT INTO themes_annonce(themeid, annonceid)VALUES(4,1);

INSERT INTO themes_annonce(themeid, annonceid)VALUES(5,1);

INSERT INTO mise_en_relation(id, annonceid, suiveurid, matchdt, commentaire_reponse,
                             evaluation, commentaire_evaluation, commentaire_abandon, statut)
VALUES(0,1,0,to_date('07/12/2017','DD/MM/YYYY'),
       'oui ça m''intéresse!!',5,
       'une très belle découverte',
       'faux commentaire abandon',0);
