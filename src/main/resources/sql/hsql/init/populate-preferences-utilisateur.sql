
DELETE FROM IDENTITE_UTILISATEUR;

INSERT INTO IDENTITE_UTILISATEUR(id,feduserid,pubkey,privkey)
VALUES(0,'Dark1','PUBKEY','PRIVKEY');

INSERT INTO IDENTITE_UTILISATEUR(id,feduserid,pubkey,privkey)
VALUES(1,'Red1','PUBKE1','PRIVKE1');

INSERT INTO IDENTITE_UTILISATEUR(id,feduserid,pubkey,privkey)
VALUES(2,'Blue1','PUBKE2','PRIVKE2');

INSERT INTO IDENTITE_UTILISATEUR(id,feduserid,pubkey,privkey)
VALUES(3,'Yellow1','PUBKE3','PRIVKE3');

DELETE FROM IMAGE_AVATAR;

INSERT INTO IMAGE_AVATAR(IMG_ID, IMG_TITLE, IMG_DATA)
VALUES (0,'dark',
        load_file('/home/aurelien/Project/partageon_back/src/main/resources/sql/hsql/init/image0.dat'));

INSERT INTO IMAGE_AVATAR(IMG_ID, IMG_TITLE, IMG_DATA)
VALUES (1,'red',
        load_file('/home/aurelien/Project/partageon_back/src/main/resources/sql/hsql/init/image1.dat'));

DELETE FROM PREFERENCES_UTILISATEUR;

INSERT INTO PREFERENCES_UTILISATEUR(userid, pseudo, telephone, mobile, indispodebut, indispofin,
                                    commentaire, anonyme, uriavatar, avatarid, mail)
VALUES(0,'Dark1', '0146086000', '0612345678',null, null,
         'Les Lundis & Mercredi entre 12h00 & 14h00.', 0,
         '/images/avatar/av1.png', 0, 'dark1@natixis.com');

INSERT INTO PREFERENCES_UTILISATEUR(userid, pseudo, telephone, mobile, indispodebut, indispofin,
                                    commentaire, anonyme, uriavatar, avatarid, mail)
VALUES(1,'Red1', '0146086001', '0612345671',null, null,
         'Les Lundis & Mercredi entre 12h00 & 14h00.', 0,
         '/images/avatar/av2.png', 1, 'red1@natixis.com');

INSERT INTO PREFERENCES_UTILISATEUR(userid, pseudo, telephone, mobile, indispodebut, indispofin,
                                    commentaire, anonyme, uriavatar, avatarid, mail)
VALUES(2,'Blue1', '0146086000', '0612345671',null, null,
         'Les Lundis & Mercredi entre 12h00 & 14h00.', 0,
         '/images/avatars/R1.jpg', 0, 'er1@natixis.com');

INSERT INTO PREFERENCES_UTILISATEUR(userid, pseudo, telephone, mobile, indispodebut, indispofin,
                                    commentaire, anonyme, uriavatar, avatarid, mail)
VALUES(3,'Yellow1', '0146086000', '0612345671',null, null,
         'Les Lundis & Mercredi entre 12h00 & 14h00.', 0,
         '/avatars/R2.jpg', 1, 'er2@natixis.com');

DELETE FROM media_utilisateur;

INSERT INTO media_utilisateur(userid, mediaid)VALUES(0,0);

INSERT INTO media_utilisateur(userid, mediaid)VALUES(0,1);

INSERT INTO media_utilisateur(userid, mediaid)VALUES(0,3);

INSERT INTO media_utilisateur(userid, mediaid)VALUES(1,3);

INSERT INTO media_utilisateur(userid, mediaid)VALUES(2,3);

INSERT INTO media_utilisateur(userid, mediaid)VALUES(2,2);

INSERT INTO media_utilisateur(userid, mediaid)VALUES(3,2);

DELETE FROM sites_utilisateur;

INSERT INTO sites_utilisateur(userid, siteid)VALUES(0,0);

INSERT INTO sites_utilisateur(userid, siteid)VALUES(0,2);

INSERT INTO sites_utilisateur(userid, siteid)VALUES(0,8);

INSERT INTO sites_utilisateur(userid, siteid)VALUES(0,1);

INSERT INTO sites_utilisateur(userid, siteid)VALUES(1,8);

INSERT INTO sites_utilisateur(userid, siteid)VALUES(1,1);

INSERT INTO sites_utilisateur(userid, siteid)VALUES(2,1);

INSERT INTO sites_utilisateur(userid, siteid)VALUES(3,8);
