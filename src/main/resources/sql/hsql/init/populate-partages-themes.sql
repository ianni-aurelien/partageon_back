
insert into theme(id,libelle,is_system)values(0,'Digital',1);

insert into theme(id,libelle,is_system)values(1,'Langues',1);

insert into theme(id,libelle,is_system)values(2,'Méthodologies',1);

insert into theme(id,libelle,is_system)values(3,'Métier',1);

insert into theme(id,libelle,is_system)values(4,'Outils bureautiques',1);

insert into theme(id,libelle,is_system)values(5,'Outils métiers',1);

insert into theme(id,libelle,is_system)values(6,'Support / Livres',1);

insert into theme(id,libelle,is_system)values(7,'Technologie',1);

insert into theme(id,libelle,is_system)values(8,'Autre',1);

insert into statut(id,libelle)values(0, 'disponible');

insert into statut(id,libelle)values(1, 'clôturé');

insert into statut(id,libelle)values(2, 'partagé');

insert into statut(id,libelle)values(3, 'évalué');

insert into statut(id,libelle)values(4, 'abandonné');

insert into statut(id,libelle)values(5, 'archivé');

insert into statut(id,libelle)values(6, 'désactivée');

