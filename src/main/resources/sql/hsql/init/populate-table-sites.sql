
delete from sites;

-- site: Hamelin
insert into SITES(id,site,region)values(0,'Hamelin','IDF');

-- site: 47 Quai
insert into SITES(id,site,region)values(1,'47 Quai','IDF');

-- site: Bercy
insert into SITES(id,site,region)values(2,'Bercy','IDF');

-- site: Kupka
insert into SITES(id,site,region)values(3,'Kupka','IDF');

-- site: Arc de Seine
insert into SITES(id,site,region)values(4,'Arc de Seine','IDF');

-- site: Coupole N
insert into SITES(id,site,region)values(5,'Coupole N','IDF');

-- site: Montessuy
insert into SITES(id,site,region)values(6,'Montessuy','IDF');

-- site: Grand Seine
insert into SITES(id,site,region)values(7,'Grand Seine','IDF');

-- site: Coupole S
insert into SITES(id,site,region)values(8,'Coupole S','IDF');

-- site: Lumière Seine
insert into SITES(id,site,region)values(9,'Lumière Seine','IDF');

-- site: Liberté
insert into SITES(id,site,region)values(10,'Liberté','IDF');

-- site: Lumière Garonne
insert into SITES(id,site,region)values(11,'Lumière Garonne','IDF');

-- site: Liberté 2
insert into SITES(id,site,region)values(12,'Liberté 2','IDF');

-- site: Lumière Sud
insert into SITES(id,site,region)values(13,'Lumière Sud','IDF');

-- site: Nouveau Bercy
insert into SITES(id,site,region)values(14,'Nouveau Bercy','IDF');

-- site: Montmartre
insert into SITES(id,site,region)values(15,'Montmartre','IDF');

-- site: Parc Avenue
insert into SITES(id,site,region)values(16,'Parc Avenue','IDF');

-- site: Quai de Bercy Seine
insert into SITES(id,site,region)values(17,'Quai de Bercy Seine','IDF');

-- site: Quai de Bercy Yonne
insert into SITES(id,site,region)values(18,'Quai de Bercy Yonne','IDF');

-- site: Rives de Seine
insert into SITES(id,site,region)values(19,'Rives de Seine','IDF');

-- site: Sequana
insert into SITES(id,site,region)values(20,'Sequana','IDF');

-- site: Bordeaux Lac
insert into SITES(id,site,region)values(21,'Bordeaux Lac','FR');

-- site: Lille Onix
insert into SITES(id,site,region)values(22,'Lille Onix','FR');

-- site: Bordeaux Millenium
insert into SITES(id,site,region)values(23,'Bordeaux Millenium','FR');

-- site: Lyon 6° sens
insert into SITES(id,site,region)values(24,'Lyon 6° sens','FR');

-- site: Bordeaux Pythagore
insert into SITES(id,site,region)values(25,'Bordeaux Pythagore','FR');

-- site: Marseille
insert into SITES(id,site,region)values(26,'Marseille','FR');

-- site: Caen Montgomery
insert into SITES(id,site,region)values(27,'Caen Montgomery','FR');

-- site: Marseille Le Sextant
insert into SITES(id,site,region)values(28,'Marseille Le Sextant','FR');

-- site: Caen Roquemonts
insert into SITES(id,site,region)values(29,'Caen Roquemonts','FR');

-- site: Nantes
insert into SITES(id,site,region)values(30,'Nantes','FR');

-- site: Grenoble
insert into SITES(id,site,region)values(31,'Grenoble','FR');

-- site: Nantes Le Cambridge
insert into SITES(id,site,region)values(32,'Nantes Le Cambridge','FR');

insert into SITES(id,site,region)values(33,'Porto','PT');

