pragma solidity ^0.4.0;

contract Partageon {
    address owner;

    mapping(address => uint) public deeps;

    //Constuctor
    function Partageon(){
        owner = msg.sender;
    }

    // Modifiers can be used to change
    // the body of a function.
    // If this modifier is used, it will
    // prepend a check that only passes
    // if the function is called from
    // a certain address.
    modifier onlyBy(address _account)
    {
        if (msg.sender != _account)
        throw;
    // Do not forget the "_;"! It will
    // be replaced by the actual function
    // body when the modifier is used.
        _;
    }

    // This function is called for all messages sent to
    // this contract (there is no other function).
    // Sending Ether to this contract will cause an exception,
    // because the fallback function does not have the "payable"
    // modifier.
    function() { throw; }

        function addDeep(address to, uint amount) onlyBy(owner) {
        deeps[to] += amount;
    }

    function getDeep(address belongTo) constant
        returns (uint) {
        return deeps[belongTo];
    }
}
