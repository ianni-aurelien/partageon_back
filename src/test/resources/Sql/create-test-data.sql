-- Cas de test

insert into IDENTITE_UTILISATEUR(id,feduserid,pubkey,privkey)values(0,'ausername','PUBKEY','PRIVKEY');

insert into IDENTITE_UTILISATEUR(id,feduserid,pubkey,privkey)values(1,'ausernam1','PUBKE1','PRIVKE1');

insert into preferences_utilisateur(userid, pseudo, telephone, mobile, indispodebut, indispofin,commentaire, anonyme, uriavatar,mail)values(0,'Dark1', '01.46.08.60.00', '06.12.34.56.78',null, null,'Les Lundis & Mercredi entre 12h00 & 14h00.', 0, '/avatars/Dark1.jpg','dark1@natixis.com');

insert into preferences_utilisateur(userid, pseudo, telephone, mobile, indispodebut, indispofin,commentaire, anonyme, uriavatar,mail)values(1,'Red1', '01.46.08.60.01', '06.12.34.56.71',null, null,'Les Lundis & Mercredi entre 12h00 & 14h00.', 0, '/avatars/Red1.jpg','red1@natixis.com');

insert into media_utilisateur(userid, mediaid)values(0,0);

insert into media_utilisateur(userid, mediaid)values(0,1);

insert into media_utilisateur(userid, mediaid)values(0,3);

insert into media_utilisateur(userid, mediaid)values(1,3);

insert into sites_utilisateur(userid, siteid)values(0,0);

insert into sites_utilisateur(userid, siteid)values(0,2);

insert into sites_utilisateur(userid, siteid)values(0,8);

insert into sites_utilisateur(userid, siteid)values(0,1);

insert into sites_utilisateur(userid, siteid)values(1,8);

insert into sites_utilisateur(userid, siteid)values(1,1);

insert into annonce(id, initiateurid, offre, urgence, urgencedt, creationdt, description, statut)values(0, 0, 1, 0, null, to_date('07/12/2017','DD/MM/YYYY'),'Je propose des cours de développement WEB.',0);

insert into annonce(id, initiateurid, offre, urgence, urgencedt, creationdt, description, statut)values(1, 1, 0, 1, to_date('25/12/2017','DD/MM/YYYY'), to_date('07/12/2017','DD/MM/YYYY'),'Je cherche des cours de conduite voiture. (c''est urgent, merci)',0);

insert into annonce(id, initiateurid, offre, urgence, urgencedt, creationdt, description, statut)values(2, 1, 0, 0, null, to_date('07/12/2017','DD/MM/YYYY'),'Je cherche des cours de AngularJS.',0);

insert into themes_annonce(themeid, annonceid)values(0,0);

insert into themes_annonce(themeid, annonceid)values(1,0);

insert into themes_annonce(themeid, annonceid)values(2,0);

insert into themes_annonce(themeid, annonceid)values(3,1);

insert into themes_annonce(themeid, annonceid)values(4,1);

insert into themes_annonce(themeid, annonceid)values(5,1);

insert into mise_en_relation(id,annonceid,suiveurid, matchdt,commentaire_reponse, evaluation, commentaire_evaluation, commentaire_abandon, statut)values(0,1,0,to_date('07/12/2017','DD/MM/YYYY'), 'Oui ça m''intéresse!!',5, 'une très belle découverte','faux commentaire abandon',0);

--insert into mise_en_relation(id,initiateurid,suiveurid, matchdt)values(0,0,2,sysdate);