package com.natixis.partageon.dao.impl;

import com.natixis.partageon.dao.ProfileDAO;
import com.natixis.partageon.model.system.KeyPair;
import com.natixis.partageon.model.system.Media;
import com.natixis.partageon.model.system.PreferencesUtilisateur;
import com.natixis.partageon.model.system.Site;
import com.thoughtworks.xstream.XStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

/**
 * @author chaigneauam
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestsPartageOnPartageDAOImpl {

	private static final Logger logger=LoggerFactory.getLogger(TestsPartageOnPartageDAOImpl.class);
	
	@Autowired
    ProfileDAO dao;

	@Test
	public void testListeSites(){
		List<Site> sites = dao.listSites();
		logger.debug("sites:"+sites);
	}
	
	@Test
	public void testListeMedia(){
		List<Media> media = dao.listMedia();
		logger.debug("media:"+media);
	}
	
	@Test
	public void testGetProfil(){
		PreferencesUtilisateur u = dao.getProfil(0L);
		logger.debug("u:"+u);
		XStream x=new XStream();
		logger.debug("u:"+x.toXML(u));
	}
	
	@Test
	public void testGetProfilByFedId(){
		String fedUserId="ausername";
		Long userid=dao.getUserIdByFederationId(fedUserId);
		logger.debug("userid:"+userid);
		KeyPair kp = dao.getKeyPairByFederationId(fedUserId);
		logger.debug("keypair"+kp);
	}
	
	@Test
	public void testListeUserSites(){
		List<Site> sites = dao.listSites(0L);
		logger.debug("sites:"+sites);
	}
	
	@Test
	public void testListeUserMedia(){
		List<Media> media = dao.listMedia(0L);
		logger.debug("media:"+media);
	}
	
	@Test
	public void testCreateNewIdentite(){
		String fedUserId="fedUserId";
		Long userid = dao.createUserIdentite(fedUserId, "pubKey1", "privKey1");
		logger.debug("userid:"+userid);
		PreferencesUtilisateur u=new PreferencesUtilisateur();
		u.setUserid(userid);
		u.setPseudo("pseudo");
		u.setIndispoDebut(new Date());
		u.setIndispoFin(new Date());
		u.setCommentaire("commentaire");
		u.setAnonyme(Boolean.FALSE);
		u.setUriAvatar("uriAvatar");
		u.setFeduserid(fedUserId);
		u.setTelephone("01.46.08.60.00");
		u.setMobile("06.12.34.56.78");
		{
			Media m=new Media();
			m.setId(0L);
			u.getMediaPrivilegies().add(m);
		}
		{
			Media m=new Media();
			m.setId(1L);
			u.getMediaPrivilegies().add(m);
		}
		{
			Media m=new Media();
			m.setId(2L);
			u.getMediaPrivilegies().add(m);
		}
		{
			Site s=new Site();
			s.setId(0L);
			u.getSitesAccessibles().add(s);
		}
		{
			Site s=new Site();
			s.setId(2L);
			u.getSitesAccessibles().add(s);
		}
		{
			Site s=new Site();
			s.setId(3L);
			u.getSitesAccessibles().add(s);
		}
		XStream x=new XStream();
		logger.debug(x.toXML(u));
		dao.createUserProfile(u);
		logger.debug("userid:"+userid);
	}

}
