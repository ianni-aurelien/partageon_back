package com.natixis.partageon.dao.impl;

import com.natixis.partageon.dao.PartageDAO;
import com.natixis.partageon.model.system.Annonce;
import com.natixis.partageon.model.system.AnnonceReponse;
import com.natixis.partageon.model.system.Partage;
import com.natixis.partageon.model.system.Theme;
import com.natixis.partageon.utils.FileTestUtils;
import com.thoughtworks.xstream.XStream;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @deprecated
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class TestsPartageOnProfileDAOImpl {

	private static final Logger logger = LoggerFactory
			.getLogger(TestsPartageOnProfileDAOImpl.class);

	@Autowired
    PartageDAO dao;
    
	private final XStream x=new XStream();

	@Test
	public void testListPartagesEtAnnoncesByUser() {
		List<Annonce> annonces = dao.listPartagesEtAnnonces(1L);
		String annoncesStr = x.toXML(annonces);
		logger.debug("annonces:" + annoncesStr);
		FileTestUtils
				.assertEquals(
						"com/natixis/partageon/dao/impl/liste-annonces-by-user-xstream.xml",
						annoncesStr, true);
	}

	@Test
	public void testListeAnnonces() {
		List<Annonce> annonces = dao.listAnnonces();
		String annoncesStr = x.toXML(annonces);
		logger.debug("annonces:" + annoncesStr);
		FileTestUtils.assertEquals(
				"com/natixis/partageon/dao/impl/liste-annonces-xstream.xml",
				annoncesStr, true);
	}

	@Test
	public void testGetAnnonceById() {
		Annonce annonce = dao.getAnnonceById(1L);
		String annonceStr = x.toXML(annonce);
		logger.debug("annonce:" + annonceStr);
		FileTestUtils.assertEquals(
				"com/natixis/partageon/dao/impl/get-annonce-xstream.xml",
				annonceStr, true);
	}

	@Test
	public void testGetMerById() {
		Partage annonce = dao.getMiseEnRelationById(0L);
		logger.debug("" + x.toXML(annonce));
		String annonceStr = x.toXML(annonce);
		logger.debug("annonce:" + annonceStr);
		FileTestUtils.assertEquals(
				"com/natixis/partageon/dao/impl/get-mer-by-id-xstream.xml",
				annonceStr, true);
	}

	@Test
	public void testListeThemes() {
		List<Theme> themes = dao.listThemes();
		String themesStr = x.toXML(themes);
		logger.debug("themes:" + themesStr);
		FileTestUtils.assertEquals(
				"com/natixis/partageon/dao/impl/liste-themes-xstream.xml",
				themesStr, true);
	}

	@Test
	public void testLieAnnonce() {
		AnnonceReponse reponse = new AnnonceReponse();
		reponse.setAnnonceid(0L);
		reponse.setCommentaire("Cette annonce tombe a point nomme, j'aimerais beaucoup apprendre a faire des sites internet!");
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(Calendar.DAY_OF_MONTH, 8);
		cal.set(Calendar.MONTH, 11);
		cal.set(Calendar.YEAR, 2016);
		cal.set(Calendar.HOUR_OF_DAY, 13);
		cal.set(Calendar.MINUTE, 28);
		reponse.setMatchdate(cal.getTime());
		reponse.setUserid(1L);
		Partage partage = dao.lieAnnonce(reponse);
		String partageStr = x.toXML(partage);
		logger.debug("partage:" + partageStr);
		FileTestUtils.assertEquals(
				"com/natixis/partageon/dao/impl/lie-annonce-xstream.xml",
				partageStr, true);
	}

	@Test
	public void testLieAnnonceEtAbandonneAnnonce() {
		AnnonceReponse reponse = new AnnonceReponse();
		reponse.setAnnonceid(0L);
		reponse.setCommentaire("Cette annonce tombe à point nommé, j'aimerais beaucoup apprendre à faire des sites internet!");
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(Calendar.DAY_OF_MONTH, 8);
		cal.set(Calendar.MONTH, 11);
		cal.set(Calendar.YEAR, 2016);
		cal.set(Calendar.HOUR_OF_DAY, 13);
		cal.set(Calendar.MINUTE, 28);
		reponse.setMatchdate(cal.getTime());
		reponse.setUserid(1L);
		Partage partage = dao.lieAnnonce(reponse);
		logger.debug("partage:" + x.toXML(partage));
		String commentaireAbandon = "je laisse tomber, le dev c'est trop compliqué et pas pour moi!!";
		partage.setCommentaireAbandon(commentaireAbandon);
		partage.setLacheurId(1L);
		dao.abandonneMiseEnRelation(partage);
	}

	@Test
	public void testLieAnnonceEtEvalueAnnonce() {
		AnnonceReponse reponse = new AnnonceReponse();
		reponse.setAnnonceid(0L);
		reponse.setCommentaire("Cette annonce tombe à point nommé, j'aimerais beaucoup apprendre à faire des sites internet!");
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(Calendar.DAY_OF_MONTH, 8);
		cal.set(Calendar.MONTH, 11);
		cal.set(Calendar.YEAR, 2016);
		cal.set(Calendar.HOUR_OF_DAY, 13);
		cal.set(Calendar.MINUTE, 28);
		reponse.setMatchdate(cal.getTime());
		reponse.setUserid(1L);
		Partage partage = dao.lieAnnonce(reponse);
		logger.debug("partage:" + partage);
		partage.setEvaluation(5);
		String commentaireAbandon = "C'était vraiment super!! Je sais à présent développer un site Internet! Merci!!";
		partage.setCommentaireEvaluation(commentaireAbandon);
		dao.evalueMiseEnRelation(partage);
	}

	@Test
	public void testClotureAnnonce() {
		logger.debug("");
		Annonce annonce = new Annonce();
		annonce.setAnnonceid(0L);
		dao.clotureAnnonce(annonce);
	}

	@Test
	public void testCreateAndModifyAnnonce() {
		InputStream in = TestsPartageOnPartageDAOImpl.class.getClassLoader()
				.getResourceAsStream(
						"com/natixis/partageon/dao/impl/annonce-xstream.xml");
		Annonce annonce = (Annonce) x.fromXML(in);
		Annonce annonce2 = dao.createOrModifyAnnonce(annonce);
		List<Theme> themes = new ArrayList<>();
		{
			Theme t=new Theme();
			t.setId(3L);
			themes.add(t);
		}
		annonce2.setAnnonce("annonce modifiée");
		annonce2.setThemes(themes);
		dao.createOrModifyAnnonce(annonce2);
	}

}
