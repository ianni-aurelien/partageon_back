package com.natixis.partageon.dao.impl;

import com.natixis.partageon.controller.ProfileControllerTest;
import com.natixis.partageon.model.client.User;
import com.natixis.partageon.model.client.WAvatar;
import com.natixis.partageon.model.system.Avatar;
import com.natixis.partageon.model.system.PreferencesUtilisateur;
import com.natixis.partageon.model.system.UserIdentity;
import org.dozer.DozerBeanMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by aurelien on 01/07/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProfileDAOImplTest {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ProfileDAOImplTest.class);
    
    @Autowired
    private ProfileDAOImpl profileDAO;
    
    @Autowired
    private DozerBeanMapper mapper;
    
    private Avatar avatar;
    
    private User user;
    
    private PreferencesUtilisateur userPref = new PreferencesUtilisateur();
    
    private WAvatar WAvatar = new WAvatar();
    
    @Before
    public void init() throws IOException {final File image = new File(ResourceUtils.getFile(
        ProfileControllerTest.class.getClassLoader().getResource("image/ringofakhaten.jpg"))
        .getAbsolutePath());
        Path path = Paths.get(image.getAbsolutePath());
        byte[] data = Files.readAllBytes(path);
    
        avatar = new Avatar();
        avatar.setImg_title(image.getName());
        avatar.setImg_data(data);
        
        mapper.map(avatar, WAvatar);
    
        user = new User();
        user.setFedUserId("test");
        user.setPseudo("test");
        user.setNeedAuthentication(false);
        user.setNom("test");
        user.setPrenom("test");
    }
    
    @Test
    public void updateUserAvatar() throws Exception {
    
    }
    
    @Test
    public void should_add_avatar() throws Exception {
        Long res = profileDAO.addAvatar(avatar);
        Assert.assertTrue(res >= 1);
    }
    
    @Test
    public void should_get_list_avatar() throws Exception {
        profileDAO.addAvatar(avatar);
    
        List<Avatar> res = profileDAO.findAllAvatar();
        
        Assert.assertTrue(!res.isEmpty());
        Assert.assertEquals(res.get(0).getImg_title(), avatar.getImg_title());
    }
    
    @Test
    public void should_get_avatar_by_id() throws Exception {
        Long id = profileDAO.addAvatar(avatar);
    
        Avatar res = profileDAO.findAvatarById(id);
        
        Assert.assertEquals(res.getImg_title(), avatar.getImg_title());
    }
    
    @Test
    public void should_get_avatar_by_title() throws Exception {
        profileDAO.addAvatar(avatar);
    
        Avatar res = profileDAO.findAvatarByTitle(avatar.getImg_title());
        
        Assert.assertEquals(res.getImg_title(), avatar.getImg_title());
    }
    
    @Test
    public void should_create_user_identity() {
        Long userId = profileDAO.createUserIdentite("test", "test", "test");
        Assert.assertTrue(userId != null);
    }
    
    @Test
    public void should_get_user_profile() {
        profileDAO.createUserIdentite("test0", "test0", "test0");
        UserIdentity userIdentity = profileDAO.findUserIdentityByFedId("test0");
        Assert.assertTrue(userIdentity != null);
        Assert.assertTrue(userIdentity.getFeduserid().equals("test0"));
    }
    
    @Test
    public void should_create_user_pref() {
        Long userId;
        profileDAO.createUserIdentite("test1", "test1", "test1");
        mapper.map(user, PreferencesUtilisateur.class);
        userId = profileDAO.getUserIdByFederationId("test1");
        userPref.setUserid(userId);
        profileDAO.createUserProfile(userPref);
        PreferencesUtilisateur result = profileDAO.getProfil(userId);
        Assert.assertTrue(result != null);
    }
    
    @Test
    public void should_insert_user_with_avatar() {
        Long userId;
        profileDAO.createUserIdentite("test", "test", "test");
        userPref = mapper.map(user, PreferencesUtilisateur.class);
        userPref.setAvatar(avatar);
        userId = profileDAO.getUserIdByFederationId("test");
        userPref.setUserid(userId);
        profileDAO.createUserProfile(userPref);
        profileDAO.setUserAvatarImg(userPref);
        PreferencesUtilisateur result = profileDAO.getProfil(userId);
        Assert.assertTrue(result != null);
        Assert.assertTrue(result.getAvatar() != null);
        Assert.assertTrue(result.getAvatar().getImg_title() != null);
        Assert.assertTrue(WAvatar.getFilename().equals(result.getAvatar().getImg_title()));
    }
}
