package com.natixis.partageon.controller;

import com.natixis.partageon.model.client.WAvatar;
import com.natixis.partageon.model.client.User;
import com.natixis.partageon.model.client.UserProfile;
import com.natixis.partageon.model.client.WAvatar;
import com.natixis.partageon.service.ProfileService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by aurelien on 01/07/17.
 */
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class ProfileControllerTest {
    
    private static final Logger logger= LoggerFactory.getLogger(ProfileControllerTest.class);
    
    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
        MediaType.APPLICATION_JSON.getSubtype(),
        Charset.forName("utf8"));
    
    @MockBean
    private ProfileService profileService;
    
    private HttpMessageConverter mappingJackson2HttpMessageConverter;
    
    @Autowired
    private MockMvc mvc;
    
    private WAvatar WAvatar;
    
    private UserProfile userProfile;
    
    private User user;
    
    @Before
    public void init() throws IOException {
        final File image = new File(ResourceUtils.getFile(
            ProfileControllerTest.class.getClassLoader().getResource("image/ringofakhaten.jpg"))
            .getAbsolutePath());
        Path path = Paths.get(image.getAbsolutePath());
        byte[] data = Files.readAllBytes(path);
        WAvatar = new WAvatar(data, image.getName());
        
        userProfile = new UserProfile();
        userProfile.setFedUserId("test");
        userProfile.setPseudo("prout");
        
        user = new User();
        user.setFedUserId(userProfile.getFedUserId());
        user.setPseudo(userProfile.getPseudo());
        user.setNeedAuthentication(false);
    }
    
    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
        
        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
            .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
            .findAny()
            .orElse(null);
        
        assertNotNull("the JSON message converter must not be null",
            this.mappingJackson2HttpMessageConverter);
    }
    
    @Test
    public void shouldListAllFiles() throws Exception {
        given(this.profileService.listAvatars())
            .willReturn(Collections.singletonList(WAvatar));
        
        this.mvc.perform(get("/user/listAvatars"))
            .andExpect(status().is2xxSuccessful());
    }
    
    @Test
    public void shouldSaveUploadedFile() throws Exception {
        MockMultipartFile multipartFile =
            new MockMultipartFile("file", "test.txt", "text/plain", "Spring Framework".getBytes());
        this.mvc.perform(fileUpload("/user/avatar").file(multipartFile))
            .andExpect(status().is2xxSuccessful());
        
        then(this.profileService).should().setAvatar(any());
    }
    
    @Test
    public void shouldGetUserAvatarByFedId() throws Exception {
        this.mvc.perform(post("/user/avatar/test"))
            .andExpect(status().is2xxSuccessful());
        then(this.profileService).should().getAvatarByUserFedId(any());
    }
    
    @Test
    public void should_update_profile_with_img_avatar() throws Exception {
        when(profileService.authenticate(any(),any())).thenReturn(user);
//        doNothing().when(profileService.createOrModifyUserProfile(any()));
//        when(profileService.createOrModifyUserProfile(any()));
        this.mvc.perform(post("/user/createOrModifyProfil")
                        .content(this.json(userProfile))
                        .contentType(contentType))
            .andExpect(status().is2xxSuccessful());
    
//        then(this.profileService).should().setAvatar(any());
    }
    
    private String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
            o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
}
