package com.natixis.partageon.test;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * @author chaigneauam
 *
 */
@Configuration
public class MockConfig {
	
	@Bean
	public PropertyPlaceholderConfigurer getProperty(){
		PropertyPlaceholderConfigurer conf = new PropertyPlaceholderConfigurer();
		Resource testProperties = new ClassPathResource("test-application.properties");
		Resource appProperties = new ClassPathResource("application.properties");
		Resource appProperties2 = new ClassPathResource("application-local.properties");
//		Resource appProperties=new ClassPathResource("application-local.properties");
		conf.setLocations(appProperties, appProperties2, testProperties);
		return conf;
	}

	@Bean
	public BeanPostProcessor getPostProc(){
        return new MockPostProcessor();
	}

}
