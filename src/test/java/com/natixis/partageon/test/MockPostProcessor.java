package com.natixis.partageon.test;

import lombok.Setter;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.support.BeanDefinitionReader;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.PostConstruct;

/**
 * @author chaigneauam
 * @deprecated
 */
@Ignore
@Setter
class MockPostProcessor implements BeanPostProcessor, ApplicationContextAware {

	private static Logger logger = LoggerFactory.getLogger(MockPostProcessor.class);
	
//	@Value("${test.applicationContext.xml}")
//	private String configuration;
	
	GenericApplicationContext appCtxt;
	GenericApplicationContext appCtxtDelegate;
	
	@PostConstruct
	public void init(){
		logger.debug("");
		appCtxt=new GenericApplicationContext();
		{
			BeanDefinitionReader reader=new XmlBeanDefinitionReader(appCtxt);
//			Resource resource=new ClassPathResource(configuration);
//			reader.loadBeanDefinitions(resource);
		}
		{
			BeanDefinitionReader reader=new XmlBeanDefinitionReader(appCtxtDelegate);
//			Resource resource=new ClassPathResource(configuration);
//			reader.loadBeanDefinitions(resource);
		}
		appCtxtDelegate.refresh();
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
	 */
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName)
			throws BeansException {
		if(appCtxt.containsBean(beanName)){
            return appCtxtDelegate.getBean(beanName);
		}
		return bean;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
	 */
	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName)
			throws BeansException {
		return bean;
	}

	/* (non-Javadoc)
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		appCtxtDelegate=new GenericApplicationContext(applicationContext);
	}

}
