package com.natixis.partageon.service.impl;

import com.natixis.partageon.blockchain.BlockChainService;
import com.natixis.partageon.model.system.Account;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * @author chaigneauam
 *
 */
public class BlockChainServiceMock implements BlockChainService {

	/* (non-Javadoc)
	 * @see com.natixis.partageon.service.BlockChainService#getBlockChainTime()
	 */
	@Override
	public <T> ResponseEntity<?> getBlockChainTime() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.natixis.partageon.service.BlockChainService#getBlockChainStatus()
	 */
	@Override
	public <T> ResponseEntity<?> getBlockChainStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.natixis.partageon.service.BlockChainService#getAccountId(java.lang.String)
	 */
	@Override
	public ResponseEntity<?> getAccountId(String publicKey) {
		Account account=new Account();
		return new ResponseEntity<>(account, HttpStatus.OK);
	}

	/* (non-Javadoc)
	 * @see com.natixis.partageon.service.BlockChainService#getAccountPublicKey(java.lang.String)
	 */
	@Override
	public ResponseEntity<?> getAccountPublicKey(String accountId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.natixis.partageon.service.BlockChainService#getAccountInfo(java.lang.String)
	 */
	@Override
	public ResponseEntity<?> getAccountInfo(String accountId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.natixis.partageon.service.BlockChainService#getAccountProperties(java.lang.String)
	 */
	@Override
	public ResponseEntity<?> getAccountProperties(String accountAddress) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.natixis.partageon.service.BlockChainService#getAccountBalance(java.lang.String)
	 */
	@Override
	public ResponseEntity<?> getAccountBalance(String accountAddress) {
		Account account=new Account();
		account.setBalanceNQT("123458");
		return new ResponseEntity<>(account, HttpStatus.OK);
	}

	/* (non-Javadoc)
	 * @see com.natixis.partageon.service.BlockChainService#getUnsignedTransaction(java.lang.String, java.lang.String, int)
	 */
	@Override
	public ResponseEntity<?> getUnsignedTransaction(String senderKey,
			String rcpKey, int amount) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.natixis.partageon.service.BlockChainService#sendTransaction(java.lang.String, java.lang.String, int)
	 */
	@Override
	public ResponseEntity<?> sendTransaction(String BANK_PASS,
			String aAccountRS, int amount) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean checkResponseStatus(ResponseEntity<?> aResp) {
		return false;
	}

}
