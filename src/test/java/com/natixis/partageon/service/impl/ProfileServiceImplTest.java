package com.natixis.partageon.service.impl;

import com.natixis.partageon.exception.AuthenticationRequiredException;
import com.natixis.partageon.exception.UnknownUserException;
import com.natixis.partageon.model.client.User;
import com.natixis.partageon.model.client.WAvatar;
import com.natixis.partageon.model.system.UtilisateurFederationIdentite;
import com.natixis.partageon.useridentity.FederationIdentiteService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * Created by aurelien on 04/07/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProfileServiceImplTest {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ProfileServiceImplTest.class);
    
    @Autowired
    private ProfileServiceImpl profileService;
    
    private User user;
    
    private UtilisateurFederationIdentite userFed;
    
    private WAvatar WAvatar_img;
    
    @MockBean
    FederationIdentiteService mockFederationIdentite;
    
    @Before
    public void init() throws IOException {
        user = new User();
        user.setFedUserId("test");
        user.setPseudo("test");
        user.setNeedAuthentication(false);
        user.setNom("test");
        user.setPrenom("test");
        
        userFed = new UtilisateurFederationIdentite();
        userFed.setEmail("test");
        userFed.setNom("test");
        userFed.setPrenom("test");
        
        final File image = new File(ResourceUtils.getFile(
            ProfileServiceImplTest.class.getClassLoader().getResource("image/ringofakhaten.jpg"))
            .getAbsolutePath());
        Path path = Paths.get(image.getAbsolutePath());
        byte[] data = Files.readAllBytes(path);
        
        WAvatar_img = new WAvatar();
        WAvatar_img.setFilename("test");
        WAvatar_img.setContent(data);
        
        profileService.setFederationIdentiteService(mockFederationIdentite);
    }
    
    
    @Test
    public void should_createProfile() throws AuthenticationRequiredException, UnknownUserException {
        when(mockFederationIdentite.getElementFromCache(any())).thenReturn(userFed);
        profileService.createOrModifyUserProfile(user);
        User result = profileService.getUserProfil(user.getFedUserId());
        
        Assert.assertTrue(result.getFedUserId() != null);
        Assert.assertTrue(result.getNom().equals(user.getNom()));
    }
    
    @Test
    public void should_modifyProfile() throws AuthenticationRequiredException, UnknownUserException {
        when(mockFederationIdentite.getElementFromCache(any())).thenReturn(userFed);
        
        profileService.createOrModifyUserProfile(user);
        User after_insert = profileService.getUserProfil(user.getFedUserId());
        
        User before_update = new User();
        before_update.setFedUserId(after_insert.getFedUserId());
        before_update.setNom("newName");
        before_update.setPrenom("newPrenom");
        before_update.setPseudo("newPseudo");
        before_update.setNeedAuthentication(false);
        profileService.createOrModifyUserProfile(before_update);
        User after_update = profileService.getUserProfil(user.getFedUserId());
        
        LOGGER.debug("AFTER INSERT " + String.valueOf(after_insert));
        LOGGER.debug("BEFORE UPDATE " + String.valueOf(before_update));
        LOGGER.debug("AFTER UPDATE " + String.valueOf(after_update));
        
        Assert.assertTrue(!after_insert.getPseudo().equals(after_update.getPseudo()));
    }
    
    @Test
    public void should_update_avatar_img() throws AuthenticationRequiredException, UnknownUserException {
        when(mockFederationIdentite.getElementFromCache(any())).thenReturn(userFed);
        
        profileService.setAvatarToUserByFedId(user.getFedUserId(), WAvatar_img);
        
        User result = profileService.getUserProfil("test");
        LOGGER.debug("RESULT " + String.valueOf(result.getAvatar_img()));
        Assert.assertTrue(result.getAvatar_img().getFilename() != null);
        Assert.assertTrue(result.getAvatar() == null);
    }
    
    @Test
    public void should_update_avatar_uri() throws AuthenticationRequiredException, UnknownUserException {
        when(mockFederationIdentite.getElementFromCache(any())).thenReturn(userFed);
    
        WAvatar_img.setContent(null);
        WAvatar_img.setFilename("/test/uri");
        profileService.setAvatarToUserByFedId(user.getFedUserId(), WAvatar_img);
        
        User result = profileService.getUserProfil("test");
        Assert.assertTrue(result.getAvatar_img().getFilename() == null);
        Assert.assertTrue(result.getAvatar() != null);
    }
}
