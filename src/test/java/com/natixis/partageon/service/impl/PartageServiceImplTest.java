package com.natixis.partageon.service.impl;

import com.natixis.partageon.dao.PartageDAO;
import com.natixis.partageon.dao.ProfileDAO;
import com.natixis.partageon.exception.UnknownUserException;
import com.natixis.partageon.model.client.User;
import com.natixis.partageon.model.client.WAnnonce;
import com.natixis.partageon.model.client.WAnnonceCreation;
import com.natixis.partageon.model.system.Annonce;
import com.natixis.partageon.model.system.KeyPair;
import com.natixis.partageon.utils.FileUtils;
import com.natixis.partageon.utils.impl.PropertiesServiceImpl;
import com.thoughtworks.xstream.XStream;
import org.dozer.DozerBeanMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@ImportResource("classpath*:annonce-xstream.xml")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PartageServiceImplTest {

	@InjectMocks
    PartageServiceImpl partageService;

	@Mock
	PropertiesServiceImpl properties;

	@InjectMocks
    ProfileServiceImpl mockUserService;

	@Mock
    PartageDAO mockPartageDao;

	
	@Autowired
    DozerBeanMapper mapper;

	private WAnnonceCreation wAnnonce;
    
    @Mock
    ProfileDAO profileDaoMock;

	@Before
	public void init() {
        XStream x = new XStream();
		wAnnonce = (WAnnonceCreation) x
				.fromXML(FileUtils
						.getFileContent("com/natixis/partageon/service/impl/annonce-xstream.xml"));
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void should_A_get_new_annonce() {
		WAnnonce result = null;
		wAnnonce.setFedUserId("fedUserId");
		Annonce annonce=new Annonce();
		annonce.setAnnonceid(0L);

		partageService.setPartageDao(mockPartageDao);
		partageService.setMapper(mapper);
		partageService.setProfileDao(profileDaoMock);
		partageService.setDateSource(new DateSourceMock());

		KeyPair kp=new KeyPair();
		Mockito.when(profileDaoMock.getKeyPairByFederationId(Mockito.anyString())).thenReturn(kp);
		Mockito.when(profileDaoMock.getUserIdByFederationId(Mockito.anyString())).thenReturn(Long.valueOf(0));
		Mockito.doAnswer(invocation -> {
			Annonce ann = invocation.getArgumentAt(0, Annonce.class);
			ann.setAnnonceid(7L);
			return ann;
		}).when(mockPartageDao).createOrModifyAnnonce(Mockito.any());
		try {
			result = partageService.createAnnonce(wAnnonce);
		} catch (UnknownUserException e) {
			e.printStackTrace();
		}
		Assert.assertNotNull(result != null ? result.getAnnonceId() : null);
	}

	@Test(expected = UnknownUserException.class)
	public void should_B_throw_userException() throws UnknownUserException {

		User unknownUser = new User();
		WAnnonceCreation test = wAnnonce;
		unknownUser.setFedUserId("testUnknownUser");
		unknownUser.setNeedAuthentication(true);
		test.setFedUserId("fedUserId");
		Mockito.when(profileDaoMock.getUserIdByFederationId(Mockito.anyString())).thenReturn(null);

		partageService.createAnnonce(test);

	}

	@SuppressWarnings("unused")
	@Test
	public void should_C_getAllAnnonceList() {
		List<Annonce> wAnnonce = new ArrayList<>();
		Mockito.when(mockPartageDao.listAnnonces()).thenReturn(wAnnonce);
		List<WAnnonce> res = partageService.listAnnonces();
	}
}
