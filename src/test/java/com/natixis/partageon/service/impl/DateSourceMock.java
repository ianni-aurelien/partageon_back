package com.natixis.partageon.service.impl;

import com.natixis.partageon.service.DateSource;

import java.util.Calendar;
import java.util.Date;

/**
 * @author chaigneauam
 *
 */
public class DateSourceMock implements DateSource {

	/* (non-Javadoc)
	 * @see com.natixis.partageon.service.DateSource#getDate()
	 */
	@Override
	public Date getDate() {
		Calendar cal=Calendar.getInstance();
		cal.clear();
		cal.set(Calendar.DAY_OF_MONTH, 12);
		cal.set(Calendar.MONTH, 11);
		cal.set(Calendar.YEAR, 2016);
		cal.set(Calendar.HOUR_OF_DAY, 11);
		cal.set(Calendar.MINUTE, 52);
		cal.set(Calendar.SECOND, 0);
        return cal.getTime();
	}

}
