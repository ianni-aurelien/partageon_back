package com.natixis.partageon.service.impl;

import com.natixis.partageon.model.client.User;
import com.natixis.partageon.model.system.Deeps;
import com.natixis.partageon.model.system.UtilisateurFederationIdentite;
import com.natixis.partageon.service.ProfileService;
import com.natixis.partageon.useridentity.FederationIdentiteService;
import com.natixis.partageon.utils.FileTestUtils;
import com.natixis.partageon.utils.FileUtils;
import com.thoughtworks.xstream.XStream;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chaigneauam
 *
 */

/* not fonctionnal */
@Ignore
public class ProfileServiceImplTests {

	private static final Logger logger = LoggerFactory.getLogger(ProfileServiceImplTests.class);

	private ProfileService front;
	private User userRef;
	private User user;
	private XStream x;
	
	private FederationIdentiteService fed;

	@Before
	public void init() throws Exception {
		x = new XStream();
		user = (User) x
				.fromXML(FileUtils
						.getFileContent("com/natixis/partageon/mapper/user-xstream.xml"));
		userRef = (User) x
				.fromXML(FileUtils
						.getFileContent("com/natixis/partageon/front/service/impl/user-xstream.xml"));
		UtilisateurFederationIdentite ufid=new UtilisateurFederationIdentite();
		ufid.setEmail("test-mail@natixis.com");
		Mockito.when(fed.retrieveUserInfo(Mockito.anyString(), Mockito.anyString())).thenReturn(ufid);
		Mockito.when(fed.generateHashId(Mockito.any(), Mockito.anyString())).thenReturn("ausername");
		
	}

	@Test
	public void testGetUserProfilNeedAuthent() {
		XStream x = new XStream();
		User profil;
		profil = front.getUserProfil("unknown");
		logger.debug(x.toXML(profil));
	}

	@Test
	public void testAuthent() throws Exception {
		XStream x = new XStream();
		User profil;
		profil = front.authenticate("ausername", "password");
		String profileStr=x.toXML(profil);
//		String userStr=x.toXML(userRef);
		logger.debug(profileStr);
//		Assert.assertEquals(userStr, profileStr);
//		Assert.assertEquals(null, profil.getFedUserId());
	}

	@Test
	public void testGetUserProfilExistant() throws Exception {
		XStream x = new XStream();
		User profil;
		profil = front.authenticate("ausername", "password");
		UtilisateurFederationIdentite pu = new UtilisateurFederationIdentite();
		pu.setNom("nom");
		pu.setPrenom("prenom");
		pu.setSite("Charenton le pont");
        pu.setEmail("monemail@natixis.com");
		Mockito.when(fed.getElementFromCache(Mockito.anyString())).thenReturn(pu);
		profil = front.getUserProfil("ausername");
		
		Assert.assertEquals("ausername", profil.getFedUserId());
		profil.setDeeps(Deeps.builder().total(123458L).monthly(5469L).build());
		String userStr = x.toXML(profil);
		logger.debug(userStr);
		FileTestUtils.assertEquals(
				"com/natixis/partageon/front/service/impl/user-xstream.xml",
				userStr, true);

	}

	@Test
	public void testCreateUserProfil() {
		try {
			front.createOrModifyUserProfile(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testModifyUserProfil() {
		try {
			front.createOrModifyUserProfile(user);
			user.setAvatar("nouvelAvatar");
			front.createOrModifyUserProfile(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
