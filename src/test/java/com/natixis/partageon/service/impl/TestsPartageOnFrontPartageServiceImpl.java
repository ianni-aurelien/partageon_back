package com.natixis.partageon.service.impl;

import com.natixis.partageon.blockchain.BlockChainBusiness;
import com.natixis.partageon.model.client.*;
import com.natixis.partageon.model.system.AnnonceType;
import com.natixis.partageon.model.system.Media;
import com.natixis.partageon.model.system.Theme;
import com.natixis.partageon.model.system.UtilisateurFederationIdentite;
import com.natixis.partageon.service.PartageService;
import com.natixis.partageon.service.ProfileService;
import com.natixis.partageon.useridentity.FederationIdentiteService;
import com.natixis.partageon.utils.FileTestUtils;
import com.thoughtworks.xstream.XStream;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @deprecated
 */
@Ignore
public class TestsPartageOnFrontPartageServiceImpl {

	private static final Logger logger = LoggerFactory
			.getLogger(TestsPartageOnFrontPartageServiceImpl.class);

	private ApplicationContext ctxt;
	private PartageService partages;
	private ProfileService profile;
	private XStream x;

	private FederationIdentiteService fed;

	private BlockChainBusiness blockchain;

	@Before
	public void init() {
		x = new XStream();
		blockchain = Mockito.mock(BlockChainBusiness.class);
		// Mockito.when(sender.getProperties()).thenReturn(new Properties());
	}

	@Test
	public void testAbandonneMiseEnRelation() throws Exception {
		WAnnonce annonce = null;
		{
			WAnnonceCreation a = new WAnnonceCreation();
			a.setCommentaire("commentaire");
			a.setFedUserId("ausername");
			a.setNature(AnnonceType.OFFRE);
			List<Theme> themes = new ArrayList<>();
			{
				Theme theme = new Theme();
				theme.setId(0L);
				themes.add(theme);
			}
			a.setThemes(themes);
			annonce = partages.createAnnonce(a);
		}

		List<WAnnonce> annonces = partages.listAnnonces();
		logger.debug("annonces:" + annonces);

		WAnnonceReponse reponse = new WAnnonceReponse();
		reponse.setAnnonceid(annonce.getAnnonceId());
		reponse.setCommentaire("je réponds!!");
		reponse.setFedUserid("ausernam1");
		WMiseEnRelation mer = partages.lieAnnonce(reponse);

		WMiseEnRelationEvaluation evaluation = new WMiseEnRelationEvaluation();
		evaluation.setCommentaireEvaluation("commentaireEvaluation");
		evaluation.setFedUserId("ausername");
		evaluation.setEvaluation(3);
		evaluation.setMiseEnRelationId(mer.getMiseEnRelationId());
		partages.evalueMiseEnRelation(evaluation);

		WMiseEnRelationAbandon merAbandon = new WMiseEnRelationAbandon();
		merAbandon.setCommentaireAbandon("commentaireAbandon");
		merAbandon.setFedUserId("ausername");
		merAbandon.setMiseEnRelationId(mer.getMiseEnRelationId());
		partages.abandonneMiseEnRelation(merAbandon);

	}

	@Test
	public void testEvaluation() throws Exception {
		WAnnonce annonce = null;
		{
			WAnnonceCreation a = new WAnnonceCreation();
			a.setCommentaire("commentaire");
			a.setFedUserId("ausername");
			a.setNature(AnnonceType.OFFRE);
			List<Theme> themes = new ArrayList<>();
			{
				Theme theme = new Theme();
				theme.setId(0L);
				themes.add(theme);
			}
			a.setThemes(themes);
			annonce = partages.createAnnonce(a);
		}
		{
			WAnnonceCreation a = new WAnnonceCreation();
			a.setCommentaire("commentaire2");
			a.setFedUserId("ausernam1");
			a.setNature(AnnonceType.DEMANDE);
			List<Theme> themes = new ArrayList<>();
			{
				Theme theme = new Theme();
				theme.setId(0L);
				themes.add(theme);
			}
			a.setThemes(themes);
			partages.createAnnonce(a);
		}
		List<WAnnonce> annonces = partages.listAnnonces();
		logger.debug("annonces:" + annonces);
		{
			WAnnonceReponse reponse = new WAnnonceReponse();
			reponse.setAnnonceid(annonce.getAnnonceId());
			reponse.setCommentaire("je réponds!!");
			reponse.setFedUserid("ausernam1");
			WMiseEnRelation mer = partages.lieAnnonce(reponse);
			WMiseEnRelationEvaluation evaluation = new WMiseEnRelationEvaluation();
			evaluation.setCommentaireEvaluation("commentaireEvaluation");
			evaluation.setFedUserId("ausername");
			evaluation.setEvaluation(3);
			evaluation.setMiseEnRelationId(mer.getMiseEnRelationId());
			partages.evalueMiseEnRelation(evaluation);
		}
		{
			WAnnonceReponse reponse = new WAnnonceReponse();
			reponse.setAnnonceid(annonce.getAnnonceId());
			reponse.setCommentaire("je réponds!!2");
			reponse.setFedUserid("ausernam2");
			WMiseEnRelation mer = partages.lieAnnonce(reponse);
			WMiseEnRelationEvaluation evaluation = new WMiseEnRelationEvaluation();
			evaluation.setCommentaireEvaluation("commentaireEvaluation");
			evaluation.setFedUserId("ausernam2");
			evaluation.setEvaluation(4);
			evaluation.setMiseEnRelationId(mer.getMiseEnRelationId());
			partages.evalueMiseEnRelation(evaluation);
		}
		{
			WAnnonceReponse reponse = new WAnnonceReponse();
			reponse.setAnnonceid(annonce.getAnnonceId());
			reponse.setCommentaire("je réponds!!3");
			reponse.setFedUserid("ausernam3");
			WMiseEnRelation mer = partages.lieAnnonce(reponse);
			logger.debug("");
		}
		annonces = partages.listAnnonces();
		logger.debug("annonces:" + annonces);
		for(WAnnonce ann:annonces){
			if(annonce.getAnnonceId().equals(ann.getAnnonceId())){
				logger.debug("ann:"+ann);
				Assert.assertEquals(Integer.valueOf(3), ann.getNombrePartages());
				Assert.assertEquals(Double.valueOf(3.5), ann.getMoyenneEvaluations());
			}
		}
		logger.debug("");
	}

	@Test
	public void testArchiveMiseEnRelation() throws Exception {
		WAnnonceCreation a = new WAnnonceCreation();
		a.setCommentaire("commentaire");
		a.setFedUserId("ausername");
		a.setNature(AnnonceType.OFFRE);
		List<Theme> themes = new ArrayList<>();
		{
			Theme theme = new Theme();
			theme.setId(0L);
			themes.add(theme);
		}
		a.setThemes(themes);
		WAnnonce annonce = partages.createAnnonce(a);

		WAnnonceReponse reponse = new WAnnonceReponse();
		reponse.setAnnonceid(annonce.getAnnonceId());
		reponse.setCommentaire("je réponds!!");
		reponse.setFedUserid("ausername");
		WMiseEnRelation mer = partages.lieAnnonce(reponse);

		WMiseEnRelationEvaluation evaluation = new WMiseEnRelationEvaluation();
		evaluation.setCommentaireEvaluation("commentaireEvaluation");
		evaluation.setFedUserId("ausername");
		evaluation.setEvaluation(3);
		evaluation.setMiseEnRelationId(mer.getMiseEnRelationId());
		partages.evalueMiseEnRelation(evaluation);

		WMiseEnRelationArchivage merArchivage = new WMiseEnRelationArchivage();
		merArchivage.setFedUserId("ausername");
		Long miseEnRelationId = mer.getMiseEnRelationId();
		merArchivage.setMiseEnRelationId(miseEnRelationId);
		partages.archiveMiseEnRelation(merArchivage);

	}

	@Test
	public void testClotureAnnonce() throws Exception {
		WAnnonceCreation a = new WAnnonceCreation();
		a.setCommentaire("commentaire");
		a.setFedUserId("ausername");
		a.setNature(AnnonceType.OFFRE);
		List<Theme> themes = new ArrayList<>();
		{
			Theme theme = new Theme();
			theme.setId(0L);
			themes.add(theme);
		}
		a.setThemes(themes);
		WAnnonce annonce = partages.createAnnonce(a);

		WAnnonceCloture cloture = new WAnnonceCloture();
		cloture.setAnnonceId(annonce.getAnnonceId());
		partages.clotureAnnonce(cloture);

	}

	@Test
	@Ignore
	public void testModifyAnnonce() throws Exception {
		Map<String, Integer> m1 = new HashMap<>();
		m1.put("INITIATEUR", 30);
		m1.put("SUIVEUR", 20);
		Mockito.when(
				blockchain.crediteEvenement(Mockito.any(), Mockito.anyString(),
						Mockito.anyString(), Mockito.any(),
						Mockito.anyBoolean())).thenReturn(m1);

		WAnnonceCreation a = new WAnnonceCreation();
		a.setCommentaire("commentaire");
		a.setFedUserId("ausername");
		a.setNature(AnnonceType.OFFRE);
		List<Theme> themes = new ArrayList<>();
		{
			Theme theme = new Theme();
			theme.setId(0L);
			themes.add(theme);
		}
		a.setThemes(themes);
		WAnnonce annonce = partages.createAnnonce(a);

		WAnnonceModification modification = new WAnnonceModification();
		modification.setAnnonceId(annonce.getAnnonceId());
		modification.setCommentaire("mon annonce modifiée");
		modification.setFedUserId("ausername");
		modification.setThemes(themes);
		WUrgence urgence = new WUrgence();
		modification.setUrgence(urgence);
		WAnnonce rannonce = partages.modifyAnnonce(modification);
		logger.debug("rannonce:" + x.toXML(rannonce));
		List<WAnnonce> annonces = partages.listAnnonces("ausername");
		String annoncesStr = x.toXML(annonces);
		logger.debug("annonces:" + annoncesStr);
		FileTestUtils
				.assertEquals(
						"com/natixis/partageon/service/impl/liste-annonces-xstream.xml",
						annoncesStr, true);
        annonces.stream().filter(wa -> wa.getAnnonceId().equals(rannonce.getAnnonceId())).forEachOrdered(wa -> logger.debug(""));
	}

	@Test
	@Ignore
	public void testCreateDATA() throws Exception {
		WAnnonce offreAnnonceAmael = null;
		WAnnonce demandeAnnonceAmael = null;
		WAnnonce offreAnnonceJordan = null;
		WAnnonce demandeAnnonceJordan = null;
		String feduseridAmael = "chaigneauam";
		String feduseridJordan = "croisicjo";
		Mockito.when(
				blockchain.crediteEvenement(Mockito.any(), Mockito.anyString(),
						Mockito.anyString(), Mockito.any(),
						Mockito.anyBoolean())).thenReturn(null);
		{
			User user = new User();

			user.setPseudo(feduseridAmael);
			user.setFedUserId(feduseridAmael);
			user.setAvatar("uriavatar");
			user.setAnonyme(Boolean.FALSE);
			user.setCommentaire("Je suis disponible 5 jours sur 7");
			List<Media> media = new ArrayList<>();
			{
				Media m = new Media();
				m.setId(0L);
				media.add(m);
			}
			user.setMedia(media);

			UtilisateurFederationIdentite ufid = new UtilisateurFederationIdentite();
			Mockito.when(
					fed.retrieveUserInfo(Mockito.anyString(),
							Mockito.anyString())).thenReturn(ufid);
			Mockito.when(fed.generateHashId(Mockito.any(), Mockito.anyString()))
					.thenReturn(feduseridAmael);
			Mockito.when(fed.getElementFromCache(Mockito.anyString()))
					.thenReturn(ufid);

			profile.createOrModifyUserProfile(user);
			{
				WAnnonceCreation annonce = new WAnnonceCreation();
				annonce.setCommentaire("Je propose des cours de développement WEB.");
				annonce.setNature(AnnonceType.OFFRE);
				annonce.setFedUserId(feduseridAmael);
				List<Theme> themes = new ArrayList<>();
				{
					Theme t = new Theme();
					t.setId(0L);
					themes.add(t);
				}
				annonce.setThemes(themes);
				partages.createAnnonce(annonce);
			}
			{
				WAnnonceCreation annonce = new WAnnonceCreation();
				annonce.setCommentaire("Je cherche des cours de Salsa.");
				annonce.setNature(AnnonceType.DEMANDE);
				WUrgence urgence = new WUrgence();
				urgence.setUrgence(Boolean.TRUE);
				annonce.setUrgence(urgence);
				annonce.setFedUserId(feduseridAmael);
				List<Theme> themes = new ArrayList<>();
				{
					Theme t = new Theme();
					t.setId(3L);
					themes.add(t);
				}
				annonce.setThemes(themes);
				partages.createAnnonce(annonce);
			}

			{
				WAnnonceCreation annonce = new WAnnonceCreation();
				annonce.setCommentaire("Je propose des cours de tango brésilien. Jordan sera sûrement intéressé. ;)");
				annonce.setNature(AnnonceType.OFFRE);
				annonce.setFedUserId(feduseridAmael);
				List<Theme> themes = new ArrayList<>();
				{
					Theme t = new Theme();
					t.setId(3L);
					themes.add(t);
				}
				annonce.setThemes(themes);
				offreAnnonceAmael = partages.createAnnonce(annonce);
			}
			{
				WAnnonceCreation annonce = new WAnnonceCreation();
				annonce.setCommentaire("Je cherche des cours de ping-pong. C'est bon pour toi Jordan?");
				annonce.setNature(AnnonceType.DEMANDE);
				WUrgence urgence = new WUrgence();
				urgence.setUrgence(Boolean.TRUE);
				annonce.setUrgence(urgence);
				annonce.setFedUserId(feduseridAmael);
				List<Theme> themes = new ArrayList<>();
				{
					Theme t = new Theme();
					t.setId(3L);
					themes.add(t);
				}
				annonce.setThemes(themes);
				demandeAnnonceAmael = partages.createAnnonce(annonce);
			}
		}
		{

			User user = new User();
			user.setPseudo(feduseridJordan);
			user.setFedUserId(feduseridJordan);
			user.setAvatar("avatar");
			user.setAnonyme(Boolean.FALSE);
			// user.setCommentaire("");
			List<Media> media = new ArrayList<>();
			{
				Media m = new Media();
				m.setId(0L);
				media.add(m);
			}
			user.setMedia(media);

			UtilisateurFederationIdentite ufid = new UtilisateurFederationIdentite();
			Mockito.when(
					fed.retrieveUserInfo(Mockito.anyString(),
							Mockito.anyString())).thenReturn(ufid);
			Mockito.when(fed.generateHashId(Mockito.any(), Mockito.anyString()))
					.thenReturn(feduseridJordan);
			Mockito.when(fed.getElementFromCache(Mockito.anyString()))
					.thenReturn(ufid);

			profile.createOrModifyUserProfile(user);
			{
				WAnnonceCreation annonce = new WAnnonceCreation();
				annonce.setCommentaire("J'offre des cours de chachacha.");
				annonce.setNature(AnnonceType.OFFRE);
				annonce.setFedUserId(feduseridJordan);
				List<Theme> themes = new ArrayList<>();
				{
					Theme t = new Theme();
					t.setId(3L);
					themes.add(t);
				}
				annonce.setThemes(themes);
				offreAnnonceJordan = partages.createAnnonce(annonce);
			}
			{
				WAnnonceCreation annonce = new WAnnonceCreation();
				annonce.setCommentaire("Je cherche des cours de batchata.");
				annonce.setNature(AnnonceType.DEMANDE);
				WUrgence urgence = new WUrgence();
				urgence.setUrgence(Boolean.TRUE);
				annonce.setUrgence(urgence);
				annonce.setFedUserId(feduseridJordan);
				List<Theme> themes = new ArrayList<>();
				{
					Theme t = new Theme();
					t.setId(3L);
					themes.add(t);
				}
				annonce.setThemes(themes);
				demandeAnnonceJordan = partages.createAnnonce(annonce);
			}
		}
		{
			WAnnonceReponse reponseOffreAmael = new WAnnonceReponse();
			reponseOffreAmael.setAnnonceid(offreAnnonceAmael.getAnnonceId());
			reponseOffreAmael
					.setCommentaire("J'aurais préféré de la batchata mais c'est déjà très bien. On va voir ce que cela donne.");
			reponseOffreAmael.setFedUserid(feduseridJordan);
			partages.lieAnnonce(reponseOffreAmael);
		}
		{
			WAnnonceReponse reponseDemandeAmael = new WAnnonceReponse();
			reponseDemandeAmael
					.setAnnonceid(demandeAnnonceAmael.getAnnonceId());
			reponseDemandeAmael
					.setCommentaire("Je vais te montrer comment on joue au ping pong...");
			reponseDemandeAmael.setFedUserid(feduseridJordan);
			partages.lieAnnonce(reponseDemandeAmael);
		}
		{
			WAnnonceReponse reponseDemandeJordan = new WAnnonceReponse();
			reponseDemandeJordan.setAnnonceid(demandeAnnonceJordan
					.getAnnonceId());
			reponseDemandeJordan
					.setCommentaire("J'ai déjà dansé la batchata. Sinon je t'apprendrai le rock, ça y ressemble.");
			reponseDemandeJordan.setFedUserid(feduseridAmael);
			partages.lieAnnonce(reponseDemandeJordan);
		}
		{
			WAnnonceReponse reponseOffreJordan = new WAnnonceReponse();
			reponseOffreJordan.setAnnonceid(offreAnnonceJordan.getAnnonceId());
			reponseOffreJordan
					.setCommentaire("Jordan, j'ai des problème de dictions. La batchata pourra sûrement m'aider!!");
			reponseOffreJordan.setFedUserid(feduseridAmael);
			partages.lieAnnonce(reponseOffreJordan);
		}

		{
			Map<String, Integer> m1 = new HashMap<>();
			m1.put("INITIATEUR", 30);
			m1.put("SUIVEUR", 20);
			Mockito.when(
					blockchain.crediteEvenement(Mockito.any(),
							Mockito.anyString(), Mockito.anyString(),
							Mockito.any(), Mockito.anyBoolean()))
					.thenReturn(m1);
			List<WAnnonce> annonces = partages.listAnnonces(feduseridAmael);
			String annoncesStr = x.toXML(annonces);
			logger.debug("annonces:" + annoncesStr);
			FileTestUtils
					.assertEquals(
							"com/natixis/partageon/service/impl/liste-annonces-2-xstream.xml",
							annoncesStr, true);
		}
	}

}
