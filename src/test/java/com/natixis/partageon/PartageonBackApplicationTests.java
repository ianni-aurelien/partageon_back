package com.natixis.partageon;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PartageonBackApplicationTests extends SpringBootServletInitializer {

	@Test
	public void contextLoads() {
        System.setProperty("textdb.allow_full_path", "true");
    }

}
