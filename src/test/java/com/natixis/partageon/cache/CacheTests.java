package com.natixis.partageon.cache;

import com.natixis.partageon.model.client.User;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.ConfigurationFactory;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

/**
 * @author chaigneauam
 *
 */
public class CacheTests {

	private static final Logger logger = LoggerFactory.getLogger(CacheTests.class);

	@Test
	public void testOne() {
		logger.debug("");
		InputStream in=CacheTests.class.getClassLoader().getResourceAsStream("ehcache.xml");
		Configuration conf=ConfigurationFactory.parseConfiguration(in);
		logger.debug("conf:"+conf);
		CacheManager cacheMgr=new CacheManager(conf);
		Cache cache=cacheMgr.getCache("athena");
		Object obj=cache.get("test");
		logger.debug("obj:"+obj);
		if(obj==null){
			User u=new User();
			u.setNom("Dupont");
			Element element=new Element("test", u);
			cache.put(element);
		}
		User u=(User)cache.get("test").getObjectValue();
		logger.debug("u"+u);
	}
}
