package com.natixis.partageon.mapper;

import com.natixis.partageon.model.system.Site;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.beans.PropertyDescriptor;
import java.math.BigDecimal;

/**
 * @author chaigneauam
 *
 */
public class MapperTests {
	
	private static final Logger logger = Logger.getLogger(MapperTests.class);

	private final Mapper mapper=new Mapper();
	
	@Test
	public void testMapper(){
		Assert.assertEquals(Boolean.FALSE, mapper.convert(0L,Boolean.class));
		Assert.assertEquals(Boolean.TRUE, mapper.convert(1L,Boolean.class));
	}
	
	@Test
	public void testConvert() throws Exception{
		Site s=new Site();
		ConvertUtilsBean c=new ConvertUtilsBean();
		PropertyDescriptor propDesc = PropertyUtils.getPropertyDescriptor(s, "id");
		logger.debug("propDesc:"+propDesc);
		Class<?> type = propDesc.getPropertyType();
		Object res = c.convert(BigDecimal.valueOf(10), type);
		logger.debug("res:"+res.getClass());
	}
}
