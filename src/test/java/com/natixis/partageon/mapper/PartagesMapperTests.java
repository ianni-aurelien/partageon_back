package com.natixis.partageon.mapper;

import com.natixis.partageon.model.client.*;
import com.natixis.partageon.model.system.*;
import com.natixis.partageon.utils.FileTestUtils;
import com.natixis.partageon.utils.FileUtils;
import com.thoughtworks.xstream.XStream;
import org.dozer.DozerBeanMapper;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author chaigneauam
 *
 */
public class PartagesMapperTests {
	
	private static final Logger logger = LoggerFactory.getLogger(PartagesMapperTests.class);

	private org.dozer.Mapper mapper;
	
	private XStream x;

	private Date dt;
	
	private List<Theme> themes;
	
	private WAnnonce wannonce;
	
	private Annonce annonce;
	
	private WAnnonceCreation wannoncecreation;
	
	private WAnnonceModification wAnnonceModification;
	
	private WAnnonceReponse wAnnonceReponse;
	
	private WAnnonceCloture wAnnonceCloture;
	
	private Partage partage;

	private String dtstr;
	
	@SuppressWarnings("unchecked")
	@Before
	public void init(){
		mapper=new DozerBeanMapper(Collections.singletonList("dozer/partageon-mapper.xml"));
		x=new XStream();
		
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(Calendar.DAY_OF_MONTH, 9);
		cal.set(Calendar.MONTH, 11);
		cal.set(Calendar.YEAR, 2016);
		cal.set(Calendar.HOUR_OF_DAY, 11);
		cal.set(Calendar.MINUTE, 53);
		cal.set(Calendar.SECOND, 52);
		
		dt=cal.getTime();
		DateFormat fmt=new SimpleDateFormat("dd/MM/yyyy");
		dtstr=fmt.format(dt);
		
		themes=(List<Theme>)x.fromXML(FileUtils.getFileContent("com/natixis/partageon/mapper/liste-themes-xstream.xml"));
		
		wannonce=(WAnnonce)x.fromXML(FileUtils.getFileContent("com/natixis/partageon/mapper/wannonce-xstream.xml"));
		
		annonce=(Annonce)x.fromXML(FileUtils.getFileContent("com/natixis/partageon/mapper/annonce-xstream.xml"));
		
		wannoncecreation=(WAnnonceCreation)x.fromXML(FileUtils.getFileContent("com/natixis/partageon/mapper/wannoncecreation-xstream.xml"));
		
		wAnnonceModification=(WAnnonceModification)x.fromXML(FileUtils.getFileContent("com/natixis/partageon/mapper/wannoncemodification-xstream.xml"));
		
		wAnnonceReponse=(WAnnonceReponse)x.fromXML(FileUtils.getFileContent("com/natixis/partageon/mapper/wAnnonceReponse-xstream.xml"));
		
		partage=(Partage)x.fromXML(FileUtils.getFileContent("com/natixis/partageon/mapper/partage-xstream.xml"));
		
		wAnnonceCloture=(WAnnonceCloture)x.fromXML(FileUtils.getFileContent("com/natixis/partageon/mapper/wAnnonceCloture-xstream.xml"));
		
	}
	
	@Test
	public void testXStreamWAnnonce(){
		WAnnonce a=new WAnnonce();
		a.setCommentaire("commentaire");
		a.setNature(AnnonceType.DEMANDE);
		a.setPostDate(dtstr);
		WUrgence urgence=new WUrgence();
		urgence.setUrgence(Boolean.TRUE);
		urgence.setUrgencedate(dtstr);
		User u=new User();
		u.setFedUserId("fedUserId");
		a.setInitiateur(u);
		a.setUrgence(urgence);
		
		logger.debug("u:"+x.toXML(a));
	}
	
	@Test
	@Ignore
	public void testWAnnonceCreationAnnonce(){
		Annonce annonce=mapper.map(wannoncecreation, Annonce.class);
		String annonceStr=x.toXML(annonce);
		logger.debug("annonceStr:"+annonceStr);
		FileTestUtils.assertEquals("com/natixis/partageon/mapper/wannoncecreation-annonce-xstream.xml", annonceStr, true);
	}
	
	
	
	@Test
	@Ignore
	public void testWAnnonceModificationAnnonce(){
		Annonce a=mapper.map(wAnnonceModification, Annonce.class);
		String str=x.toXML(a);
		logger.debug("a:"+str);
		FileTestUtils.assertEquals("com/natixis/partageon/mapper/WAnnonceModificationAnnonce-xstream.xml", str, true);
	}
	
	@Test
	public void testWAnnonceReponseAnnonceReponse(){
		AnnonceReponse reponse=mapper.map(wAnnonceReponse, AnnonceReponse.class);
		String str=x.toXML(reponse);
		logger.debug("a:"+str);
		FileTestUtils.assertEquals("com/natixis/partageon/mapper/WAnnonceReponseAnnonceReponse-xstream.xml", str, true);
	}
	
	@Test
	public void testWMiseEnRelationAbandonPartage(){
		WMiseEnRelationAbandon w=new WMiseEnRelationAbandon();
		w.setCommentaireAbandon("commentaireAbandon");
		w.setFedUserId("fedUserId");
		w.setMiseEnRelationId(8L);
		logger.debug(""+x.toXML(w));
		Partage p=mapper.map(w, Partage.class);
		String str=x.toXML(p);
		logger.debug("p:"+str);
		FileTestUtils.assertEquals("com/natixis/partageon/mapper/WMiseEnRelationAbandonPartage-xstream.xml", str, true);
	}
	
	@Test
	public void testWMiseEnRelationArchivagePartage(){	
		WMiseEnRelationArchivage w=new WMiseEnRelationArchivage();
		w.setFedUserId("fedUserId");
		w.setMiseEnRelationId(88L);
		Partage p=mapper.map(w, Partage.class);
		String str=x.toXML(p);
		logger.debug("p:"+str);
		FileTestUtils.assertEquals("com/natixis/partageon/mapper/WMiseEnRelationArchivagePartage-xstream.xml", str, true);
	}
	
	@Test
	public void testWMiseEnRelationEvaluationPartage(){
		WMiseEnRelationEvaluation w=new WMiseEnRelationEvaluation();
		w.setFedUserId("fedUserId");
		w.setMiseEnRelationId(88L);
		w.setEvaluation(5);
		w.setCommentaireEvaluation("commentaireEvaluation");
		Partage p=mapper.map(w, Partage.class);
		String str=x.toXML(p);
		logger.debug("p:"+str);
		FileTestUtils.assertEquals("com/natixis/partageon/mapper/WMiseEnRelationEvaluationPartage-xstream.xml", str, true);
	}
	
	@Test
	@Ignore
	public void testAnnonceWAnnonce(){
		WAnnonce wa=mapper.map(annonce, WAnnonce.class);
		String waStr=x.toXML(wa);
		logger.debug("waStr:"+waStr);
		FileTestUtils.assertEquals("com/natixis/partageon/mapper/annonce-wannonce-xstream.xml", waStr, true);
	}

	@Test
	@Ignore
	public void testPartageWMiseEnRelation(){
		WMiseEnRelation mer=mapper.map(partage, WMiseEnRelation.class);
		String str=x.toXML(mer);
		logger.debug("a:"+str);
		FileTestUtils.assertEquals("com/natixis/partageon/mapper/PartageWMiseEnRelation-xstream.xml", str, true);
	}
	
	@Test
	public void testClotureAnnonce(){
		Annonce a=mapper.map(wAnnonceCloture, Annonce.class);
		String str=x.toXML(a);
		logger.debug("a:"+str);
		FileTestUtils.assertEquals("com/natixis/partageon/mapper/wAnnonceCloture-annonce-xstream.xml", str, true);
	}
}
