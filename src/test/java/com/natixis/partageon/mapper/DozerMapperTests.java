package com.natixis.partageon.mapper;

import com.natixis.partageon.model.client.Dispo;
import com.natixis.partageon.model.client.User;
import com.natixis.partageon.model.system.*;
import com.natixis.partageon.utils.FileTestUtils;
import com.natixis.partageon.utils.FileUtils;
import com.thoughtworks.xstream.XStream;
import org.dozer.DozerBeanMapper;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;

/**
 * @author chaigneauam
 *
 */
public class DozerMapperTests {
	
	private static final Logger logger = LoggerFactory.getLogger(DozerMapperTests.class);

	private DozerBeanMapper mapper;
	
	private User user;
	private XStream x;
	
	@Before
	public void init(){
		mapper=new DozerBeanMapper(Collections.singletonList("dozer/partageon-mapper.xml"));
		x=new XStream();
		user=(User)x.fromXML(FileUtils.getFileContent("com/natixis/partageon/mapper/user-xstream.xml"));
	}
	
	@Test
	public void testMarshallUserFront(){
		User u=new User();
		u.setPrenom("prenom");
		u.setNom("nom");
		u.setFedUserId("fedUserId");
		u.setMobile("mobile");
		u.setIndispo(Dispo.builder().start("18/11/2016").end("28/02/2017").build());
		u.setDeeps(Deeps.builder().monthly(114L).total(115454L).build());
		u.setSite("site");
		u.setPseudo("pseudo");
		u.setTelephone("0146085800");
		u.setCommentaire("commentaire");
		u.setAvatar("avatar");
		u.setAnonyme(Boolean.TRUE);
		{
			Media m=new Media();
			m.setId(0L);
			u.getMedia().add(m);
		}
		{
			Media m=new Media();
			m.setId(1L);
			u.getMedia().add(m);
		}
		{
			Media m=new Media();
			m.setId(2L);
			u.getMedia().add(m);
		}
		{
			Site s=new Site();
			s.setId(0L);
			u.getSites().add(s);
		}
		{
			Site s=new Site();
			s.setId(2L);
			u.getSites().add(s);
		}
		{
			Site s=new Site();
			s.setId(3L);
			u.getSites().add(s);
		}
		logger.debug(x.toXML(u));
	}
	
	@Test
	public void testMapperPreferencesUtilisateurFrontUser(){
		PreferencesUtilisateur p = mapper.map(user, PreferencesUtilisateur.class);
		XStream x=new XStream();
		String xstreamStr=x.toXML(p);
		logger.debug(xstreamStr);
		FileTestUtils.assertEquals("com/natixis/partageon/mapper/preference-xstream.xml", xstreamStr, true);
	}
	
	@Test
	public void testMapperFedUserFrontUser(){
		UtilisateurFederationIdentite u = new UtilisateurFederationIdentite();
//		u.setMobile("0612345678");
//		u.setTelephone("0146086800");
		u.setPrenom("prenom");
		u.setNom("nom");
		u.setSite("Charenton le pont");
		User user = mapper.map(u, User.class);
		XStream x=new XStream();
		String xstreamStr=x.toXML(user);
		logger.debug(xstreamStr);
		FileTestUtils.assertEquals("com/natixis/partageon/mapper/user-from-feduser-xtsream.xml", xstreamStr, true);
	}
}
