package com.natixis.partageon.athena;

import com.natixis.partageon.useridentity.athena.AthenaConfiguration;
import com.natixis.partageon.useridentity.athena.AthenaConfigurationParser;
import com.thoughtworks.xstream.XStream;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import java.io.IOException;

/**
 * @author chaigneauam
 *
 */
public class AthenaConfigurationParserTests {

	private static final Logger logger = LoggerFactory
			.getLogger(AthenaConfigurationParserTests.class);
	
	@Test
	public void testOne() throws IOException, SAXException{
		logger.debug("");
		AthenaConfiguration conf = AthenaConfigurationParser.parseConfiguration("com/natixis/partageon/athena/athena-configuration.xml");
		XStream x=new XStream();
		logger.debug("conf:"+x.toXML(conf));
	}
}
