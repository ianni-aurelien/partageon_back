package com.natixis.partageon.blockchain;


import com.natixis.partageon.blockchain.impl.nxt.BlockChainBusinessNXTImpl;
import com.natixis.partageon.blockchain.impl.nxt.CryptoServiceNXT;
import com.natixis.partageon.exception.UnknownRuleException;
import com.natixis.partageon.exception.UnknownUserException;
import com.natixis.partageon.model.client.WAnnonce;
import com.natixis.partageon.model.client.WAnnonceCreation;
import com.natixis.partageon.model.system.*;
import com.natixis.partageon.service.PartageService;
import com.natixis.partageon.utils.FileUtils;
import com.natixis.partageon.utils.PartageOnConstantes;
import com.thoughtworks.xstream.XStream;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.SqlConfig.TransactionMode;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(
		scripts = {"classpath:Sql/delete-test-data.sql",
					"classpath:Sql/create-test-data.sql"},
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED)
		)
@Sql(
		scripts = "classpath:Sql/delete-test-data.sql",
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED),
		executionPhase = ExecutionPhase.AFTER_TEST_METHOD
		)
@Ignore
public class BlockchainBusinessNXTTest {

	private static final Logger logger = LoggerFactory.getLogger(BlockchainBusinessNXTTest.class);
	
	@Autowired
    BlockChainBusinessNXTImpl nxtService;
	
	@Autowired
    PartageService front;
	
	@Autowired
	CryptoServiceNXT cryptoService;
		
	KeyPair kpTest;
	
	XStream x;
    
    private Partage partage_demande;
	
	private List<GamificationEvent> eventList;
	
	@Before
	public void init() {
		kpTest = cryptoService.genereKeyPair();
		x = new XStream();
		eventList = new ArrayList<>();
		
		partage_demande = (Partage) x
				.fromXML(FileUtils.getFileContent("com/natixis/partageon/mapper/partage-link-demand-xstream.xml"));
	}
	
	@Test
	public void should_post_first_account_transaction() throws UnknownRuleException {
		eventList.add(GamificationEvent.CREATION_PROFIL);
		List<Event> account_creation = BlockchainUtils.get_rules_by_id(eventList);
		
		Map<String, Integer> result = nxtService.crediteEvenement(GamificationEvent.CREATION_PROFIL, kpTest.getPubKey(), null, null, false);
		
		Assert.assertTrue(account_creation.get(0).initiateur.deeps == result.get(PartageOnConstantes.INITIATEUR));
	}
	
	@Test(expected=UnknownUserException.class)
	public void should_throw_unknown_exception_for_an_account() throws UnknownUserException {
		throw new UnknownUserException("ezaeza");
	}
	
	@Test
	public void should_create_annonce() throws UnknownRuleException {
		eventList.add(GamificationEvent.CREATION_OFFRE);
		List<Event> offre_reponse = BlockchainUtils.get_rules_by_id(eventList);

		Map<String, Integer> result = nxtService.crediteEvenement(GamificationEvent.CREATION_OFFRE, kpTest.getPubKey(), null, null, false);
		
		Assert.assertTrue(offre_reponse.get(0).initiateur.deeps == result.get(PartageOnConstantes.INITIATEUR));
	}
	
	@Test
	public void should_link_offre() throws UnknownRuleException {
		eventList.add(GamificationEvent.REPONSE_OFFRE);
		List<Event> response_offre = BlockchainUtils.get_rules_by_id(eventList);
		
		Map<String, Integer> result = nxtService.crediteEvenement(GamificationEvent.REPONSE_OFFRE, kpTest.getPubKey(), null, null, false);
		
		Assert.assertTrue(response_offre.get(0).initiateur.deeps == result.get(PartageOnConstantes.INITIATEUR));
	}
	
	@Test
	public void should_link_new_urgent_demand() throws UnknownRuleException, UnknownUserException {
		WAnnonce annonce = create_a_test_annonce();
		partage_demande.setAnnonceid(annonce.getAnnonceId());
		partage_demande.setPostDt(partage_demande.getMatchDt());
		Calendar c = Calendar.getInstance(); 
		c.setTime(partage_demande.getMatchDt()); 
		c.add(Calendar.DATE, 1);
		partage_demande.setMatchDt(c.getTime());
		Urgence urgence=new Urgence();
		urgence.setUrgence(true);
		partage_demande.setUrgence(urgence);
		
		eventList = BlockchainUtils.define_event_list(GamificationEvent.REPONSE_DEMANDE, partage_demande);
		List<Event> link_demande_urgent_new = BlockchainUtils.get_rules_by_id(eventList);
		int deepAmountInitiateur = link_demande_urgent_new.stream()
				.filter(item -> item.initiateur != null)
				.mapToInt(item -> item.initiateur.deeps)
				.sum();
		
		
		Map<String, Integer> result = nxtService.crediteEvenement(GamificationEvent.REPONSE_DEMANDE, kpTest.getPubKey(), null, partage_demande, false);
		
		logger.info("should_post_first_account_transaction : "+deepAmountInitiateur+" AND it's: "+result.get(PartageOnConstantes.INITIATEUR)); 
		Assert.assertTrue(deepAmountInitiateur == result.get(PartageOnConstantes.INITIATEUR));
	}
	
//	private void waiting_forging_blockchain(int ellapse_time) {
//		try {
//			Thread.sleep(ellapse_time);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//	}
	
	private WAnnonce create_a_test_annonce() throws UnknownUserException {
		WAnnonceCreation a = new WAnnonceCreation();
		a.setCommentaire("commentaire");
		a.setFedUserId("ausername");
		a.setNature(AnnonceType.OFFRE);
		List<Theme> themes = new ArrayList<>();
		{
			Theme theme=new Theme();
			theme.setId(0L);
			themes.add(theme);
		}
		a.setThemes(themes);
		WAnnonce annonce = front.createAnnonce(a);
		return annonce;
	}

}
