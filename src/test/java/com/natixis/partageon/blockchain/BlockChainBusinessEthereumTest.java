package com.natixis.partageon.blockchain;

import com.natixis.partageon.blockchain.impl.ethereum.BlockChainBusinessEthereumImpl;
import com.natixis.partageon.blockchain.impl.ethereum.BlockChainServiceEthereumImpl;
import com.natixis.partageon.blockchain.impl.ethereum.CryptoServiceEthereum;
import com.natixis.partageon.blockchain.impl.ethereum.EthereumProperties;
import com.natixis.partageon.model.system.Deeps;
import com.natixis.partageon.model.system.KeyPair;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BlockChainBusinessEthereumTest {
    
    private static final Logger logger = LoggerFactory.getLogger(BlockChainBusinessEthereumTest.class);
    
    @Autowired
    private BlockChainServiceEthereumImpl blockchain;
    
    @Autowired
    private EthereumProperties ethereumProperties;
    
    @Autowired
    private BlockChainBusinessEthereumImpl businessEthereum;
    
    private static KeyPair test;
    
    @BeforeClass
    public static void init() {
        CryptoService cryptoServiceInit = new CryptoServiceEthereum();
        test = cryptoServiceInit.genereKeyPair();
    }
    
    @Test
    public void should_createTranscation() {
        logger.info("Test PubKey : "+test.getPubKey());
        logger.info("Test Adress : "+(blockchain.getAccountId(test.getPubKey())).getBody().toString());
        
        ResponseEntity<?> result = blockchain.sendTransaction(ethereumProperties.getBANK_PUB_KEY()
            , test.getPubKey()
            , 100);
        
        Assert.assertTrue(result.getStatusCode().is2xxSuccessful());
    }
    
    @Test
    public void should_getAccountBalance() {
        logger.info("Test PubKey : "+test.getPubKey());
        logger.info("Test Adress : "+(blockchain.getAccountId(test.getPubKey())).getBody().toString());
        
        ResponseEntity<?> result = blockchain.getAccountBalance(test.getPubKey());
        String balance = result.getBody().toString();
        logger.info("Test Balance : "+balance);
        
        Assert.assertTrue(result.getStatusCode().is2xxSuccessful());
    }
    
    @Test
    public void should_getAccountDeep() {
        logger.info("Test PubKey : "+test.getPubKey());
        
        Deeps result = businessEthereum.getDeeps(test.getPubKey());
        logger.info("Deeps : "+ result.getTotal());
        
        Assert.assertTrue(result.getTotal() >= 0);
    }
}
