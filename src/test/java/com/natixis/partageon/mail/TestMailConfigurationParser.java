package com.natixis.partageon.mail;

import com.thoughtworks.xstream.XStream;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chaigneauam
 *
 */
public class TestMailConfigurationParser {

	private static final Logger logger = LoggerFactory
			.getLogger(TestMailConfigurationParser.class);
	
	private XStream x;
	
	@Before
	public void init(){
		x=new XStream();
	}
	
	@Test
	public void testOne() throws Exception{
		logger.debug("");
		MailConfigurationRepository conf = MailConfigurationParser.parseConfiguration("local/mail-configuration.xml");
		logger.debug("conf:"+x.toXML(conf));
	}
}
