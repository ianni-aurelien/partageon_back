package com.natixis.partageon.mail;

import com.natixis.partageon.utils.FileUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Address;
import javax.mail.Message.RecipientType;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.InputStream;
import java.util.Properties;

/**
 * @deprecated
 * not fonctionnal */
@Ignore
public class TestMail {

	private static final Logger logger = LoggerFactory.getLogger(TestMail.class);
	
	@Test
	public void testOne() throws Exception{
		logger.debug("");
		Properties props=new AProperties();
		InputStream inStream=TestMail.class.getClassLoader().getResourceAsStream("com/natixis/partageon/mail/mail.properties");
		props.load(inStream);
		Session session=Session.getDefaultInstance(props);
		MimeMessage msg=new MimeMessage(session);
		InternetAddress emetteur=new InternetAddress("amael.chaigneau-ext@natixis.com");
		msg.addFrom(new Address[]{emetteur});
		InternetAddress destinataire=new InternetAddress("amael.chaigneau-ext@natixis.com");
		msg.addRecipients(RecipientType.TO, new Address[]{destinataire});
		msg.setSubject("subject");
		String text=FileUtils.getFileContent("com/natixis/partageon/mail/mailContent.html");
		msg.setText(text, "UTF8", "html");
//		Transport.send(msg);
	}
}
