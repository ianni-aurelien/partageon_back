package com.natixis.partageon.mail;

import java.io.*;
import java.util.Enumeration;
import java.util.Properties;

/**
 * @author chaigneauam
 *
 */
class AProperties extends Properties {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Properties props;
	
	/**
	 * 
	 */
	public AProperties() {
		this.props=new Properties();
	}

	/* (non-Javadoc)
	 * @see java.util.Properties#getProperty(java.lang.String)
	 */
	@Override
	public String getProperty(String key) {
		return props.getProperty(key);
	}

	/* (non-Javadoc)
	 * @see java.util.Properties#getProperty(java.lang.String, java.lang.String)
	 */
	@Override
	public String getProperty(String key, String defaultValue) {
		return props.getProperty(key, defaultValue);
	}

	/* (non-Javadoc)
	 * @see java.util.Properties#propertyNames()
	 */
	@Override
	public Enumeration<?> propertyNames() {
		return props.propertyNames();
	}

	/* (non-Javadoc)
	 * @see java.util.Properties#list(java.io.PrintStream)
	 */
	@Override
	public void list(PrintStream out) {
		props.list(out);
	}

	/* (non-Javadoc)
	 * @see java.util.Properties#list(java.io.PrintWriter)
	 */
	@Override
	public void list(PrintWriter out) {
		props.list(out);
	}

	/* (non-Javadoc)
	 * @see java.util.Properties#load(java.io.Reader)
	 */
	@Override
	public synchronized void load(Reader reader) throws IOException {
		props.load(reader);
	}

	/* (non-Javadoc)
	 * @see java.util.Properties#load(java.io.InputStream)
	 */
	@Override
	public synchronized void load(InputStream inStream) throws IOException {
		props.load(inStream);
	}

	/* (non-Javadoc)
	 * @see java.util.Properties#loadFromXML(java.io.InputStream)
	 */
	@Override
	public synchronized void loadFromXML(InputStream in) throws IOException {
		props.loadFromXML(in);
	}
	
	
}
