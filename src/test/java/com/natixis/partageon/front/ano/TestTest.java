package com.natixis.partageon.front.ano;

import com.natixis.partageon.model.client.User;
import com.natixis.partageon.model.system.UtilisateurFederationIdentite;
import com.natixis.partageon.service.PartageService;
import com.natixis.partageon.service.ProfileService;
import com.natixis.partageon.useridentity.FederationIdentiteService;
import com.thoughtworks.xstream.XStream;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class TestTest {
	
	@Autowired
	@InjectMocks
    ProfileService profil;
	
	@Autowired
    PartageService partages;
	
	private XStream x;
	
	@Mock
    FederationIdentiteService fed;
	
	private UtilisateurFederationIdentite ufi;
	
	@Before
	public void init() {
		x = new XStream();
		ufi=new UtilisateurFederationIdentite();
	}
	

	@Test
	public void testOne() throws Exception {
		Mockito.when(fed.getElementFromCache(Mockito.anyString())).thenReturn(ufi);
		User user=new User();
		user.setFedUserId("fedUserId");
		profil.createOrModifyUserProfile(user);
	}

	
}
