package com.natixis.partageon.front.ano;

import com.natixis.partageon.blockchain.BlockChainBusiness;
import com.natixis.partageon.mail.MailComposer;
import com.natixis.partageon.mail.MailSender;
import com.natixis.partageon.model.client.*;
import com.natixis.partageon.model.system.AnnonceType;
import com.natixis.partageon.model.system.Theme;
import com.natixis.partageon.model.system.UtilisateurFederationIdentite;
import com.natixis.partageon.service.PartageService;
import com.natixis.partageon.service.ProfileService;
import com.natixis.partageon.useridentity.FederationIdentiteService;
import com.thoughtworks.xstream.XStream;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chaigneauam
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestAnomalies {

	private static final Logger logger = LoggerFactory.getLogger(TestAnomalies.class);

	@Autowired
	@InjectMocks
    PartageService partage;

	@Autowired
	@InjectMocks
    ProfileService profil;

	@Mock
    FederationIdentiteService fed;

	@Mock
    BlockChainBusiness blockchain;
	
	@Autowired
	@InjectMocks
	MailComposer composer;
	
	@Autowired
	MailSender sender;

	private XStream x;

	@Before
	public void init() {
		x = new XStream();
		UtilisateurFederationIdentite fedId = new UtilisateurFederationIdentite();
		Mockito.when(fed.getElementFromCache(Mockito.anyString())).thenReturn(
				fedId);
//		Mockito.when(sender.getProperties()).thenReturn(new Properties());
	}

	@Test
    @Ignore
	public void testAno1() throws Exception {
		logger.debug("");
		String fedUserId1 = "fedUserId1";
		String fedUserId2 = "fedUserId2";
		Long merId = null;
		{
			{
				User u = new User();
				u.setAvatar("avatar");
				u.setPseudo("amael");
				u.setFedUserId(fedUserId1);
				profil.createOrModifyUserProfile(u);
			}
			{
				User u = new User();
				u.setAvatar("otheravatar");
				u.setPseudo("other");
				u.setFedUserId(fedUserId2);
				profil.createOrModifyUserProfile(u);
			}
		}
		WAnnonce rep = null;
		{
			// création de l'annonce
			WAnnonceCreation annonce = new WAnnonceCreation();
			annonce.setTitle("Formation scientifique");
			annonce.setFedUserId(fedUserId2);
			annonce.setCommentaire("Je propose des cours de Maths...");
			annonce.setNature(AnnonceType.OFFRE);
			rep = partage.createAnnonce(annonce);
		}
		{
			WAnnonceReponse liaison = new WAnnonceReponse();
			liaison.setAnnonceid(rep.getAnnonceId());
			liaison.setCommentaire("je réponds à l'offre " + rep.getAnnonceId());
			liaison.setFedUserid(fedUserId1);
			WMiseEnRelation mer = partage.lieAnnonce(liaison);
			merId = mer.getMiseEnRelationId();
			logger.debug("mer:" + x.toXML(mer));
			Assert.assertEquals("DISPONIBLE", mer.getStatut().toString());
		}
		{
			List<WAnnonce> annonces = partage.listAnnonces(fedUserId1);
			String annoncesStr = x.toXML(annonces);
			logger.debug("annonces:" + annoncesStr);
			boolean found = false;
			for (WAnnonce w : annonces) {
				if (w instanceof WMiseEnRelation) {
					WMiseEnRelation wmei = (WMiseEnRelation) w;
					if (wmei.getMiseEnRelationId().equals(merId)) {
						found = true;
						WMiseEnRelation mer = (WMiseEnRelation) w;
						Assert.assertEquals("amael", mer.getSuiveur()
								.getPseudo());
						Assert.assertEquals("DISPONIBLE", mer.getStatut()
								.toString());
						Assert.assertEquals("PARTAGE", mer
                            .getMiseEnRelationStatut());
					}
				}

			}
			Assert.assertTrue(found);
		}
		{
			WMiseEnRelationEvaluation evaluation = new WMiseEnRelationEvaluation();
			evaluation.setCommentaireEvaluation("commentaire de l'évaluation");
			evaluation.setEvaluation(3);
			evaluation.setFedUserId(fedUserId1);
			evaluation.setMiseEnRelationId(merId);
			partage.evalueMiseEnRelation(evaluation);
			{
				List<WAnnonce> annonces = partage.listAnnonces(fedUserId1);
				String annoncesStr = x.toXML(annonces);
				logger.debug("annonces:" + annoncesStr);
				boolean found = false;
				for (WAnnonce w : annonces) {
					if (w instanceof WMiseEnRelation) {
						WMiseEnRelation wmei = (WMiseEnRelation) w;
						if (wmei.getMiseEnRelationId().equals(merId)) {
							found = true;
							WMiseEnRelation mer = (WMiseEnRelation) w;
							Assert.assertEquals("amael", mer.getSuiveur()
									.getPseudo());
							Assert.assertEquals("DISPONIBLE", mer.getStatut()
									.toString());
							Assert.assertEquals("EVALUE", mer
                                .getMiseEnRelationStatut());
						}
					}

				}
				Assert.assertTrue(found);
			}
		}
	}

	@Test
    @Ignore
	public void testAno2() throws Exception {
		logger.debug("");
		String fedUserId1 = "fedUserId1";
		String fedUserId2 = "fedUserId2";
		Long merId = null;
		{
			{
				User u = new User();
				u.setAvatar("avatar");
				u.setPseudo("amael");
				u.setFedUserId(fedUserId1);
				profil.createOrModifyUserProfile(u);
			}
			{
				User u = new User();
				u.setAvatar("otheravatar");
				u.setPseudo("other");
				u.setFedUserId(fedUserId2);
				profil.createOrModifyUserProfile(u);
			}
		}
		WAnnonce rep = null;
		{
			// création de l'annonce
			WAnnonceCreation annonce = new WAnnonceCreation();
			annonce.setFedUserId(fedUserId2);
			annonce.setCommentaire("Je propose des cours de Maths...");
			annonce.setNature(AnnonceType.OFFRE);
			rep = partage.createAnnonce(annonce);
		}
		{
			WAnnonceReponse liaison = new WAnnonceReponse();
			liaison.setAnnonceid(rep.getAnnonceId());
			liaison.setCommentaire("je réponds à l'offre " + rep.getAnnonceId());
			liaison.setFedUserid(fedUserId1);
			WMiseEnRelation mer = partage.lieAnnonce(liaison);
			Assert.assertEquals(null, mer.getEvaluation());
			merId = mer.getMiseEnRelationId();
			logger.debug("mer:" + x.toXML(mer));
			Assert.assertEquals("DISPONIBLE", mer.getStatut().toString());
		}
		{
			List<WAnnonce> annonces = partage.listAnnonces(fedUserId1);
			String annoncesStr = x.toXML(annonces);
			logger.debug("annonces:" + annoncesStr);
			boolean found = false;
			for (WAnnonce w : annonces) {
				if (w instanceof WMiseEnRelation) {
					WMiseEnRelation wmei = (WMiseEnRelation) w;
					if (wmei.getMiseEnRelationId().equals(merId)) {
						found = true;
						WMiseEnRelation mer = (WMiseEnRelation) w;
						Assert.assertEquals("amael", mer.getSuiveur()
								.getPseudo());
						Assert.assertEquals("DISPONIBLE", mer.getStatut()
								.toString());
						Assert.assertEquals("PARTAGE", mer
                            .getMiseEnRelationStatut());
					}
				}

			}
			Assert.assertTrue(found);
		}
		{
			WMiseEnRelationAbandon abandon = new WMiseEnRelationAbandon();
			abandon.setCommentaireAbandon("commentaireAbandon");
			abandon.setFedUserId(fedUserId1);
			abandon.setMiseEnRelationId(merId);
			partage.abandonneMiseEnRelation(abandon);
			{
				List<WAnnonce> annonces = partage.listAnnonces(fedUserId1);
				String annoncesStr = x.toXML(annonces);
				logger.debug("annonces:" + annoncesStr);
				boolean found = false;
				for (WAnnonce w : annonces) {
					if (w instanceof WMiseEnRelation) {
						WMiseEnRelation wmei = (WMiseEnRelation) w;
						if (wmei.getMiseEnRelationId().equals(merId)) {
							found = true;
							WMiseEnRelation mer = (WMiseEnRelation) w;
							Assert.assertEquals("amael", mer.getSuiveur()
									.getPseudo());
							Assert.assertEquals("DISPONIBLE", mer.getStatut()
									.toString());
							Assert.assertEquals("ABANDONNE", mer
                                .getMiseEnRelationStatut());
						}
					}

				}
				Assert.assertTrue(found);
			}
		}
	}
	
	@Test
	public void testEvaluation() throws Exception {
		WAnnonce annonce = null;
		{
			WAnnonceCreation a = new WAnnonceCreation();
			a.setCommentaire("commentaire");
			a.setFedUserId("ausername");
			a.setNature(AnnonceType.OFFRE);
			a.setTitle("titre annonce offre");
			List<Theme> themes = new ArrayList<>();
			{
				Theme theme = new Theme();
				theme.setId(0L);
				themes.add(theme);
			}
			a.setThemes(themes);
			annonce = partage.createAnnonce(a);
		}
		{
			WAnnonceCreation a = new WAnnonceCreation();
			a.setCommentaire("commentaire2");
			a.setFedUserId("ausernam1");
			a.setNature(AnnonceType.DEMANDE);
			a.setTitle("titre annonce demande");
			List<Theme> themes = new ArrayList<>();
			{
				Theme theme = new Theme();
				theme.setId(0L);
				themes.add(theme);
			}
			a.setThemes(themes);
			partage.createAnnonce(a);
		}
		List<WAnnonce> annonces = partage.listAnnonces();
		logger.debug("annonces:" + annonces);
		{
			WAnnonceReponse reponse = new WAnnonceReponse();
			reponse.setAnnonceid(annonce.getAnnonceId());
			reponse.setCommentaire("je réponds!!");
			reponse.setFedUserid("ausernam1");
			WMiseEnRelation mer = partage.lieAnnonce(reponse);
			WMiseEnRelationEvaluation evaluation = new WMiseEnRelationEvaluation();
			evaluation.setCommentaireEvaluation("commentaireEvaluation");
			evaluation.setFedUserId("ausername");
			evaluation.setEvaluation(3);
			evaluation.setMiseEnRelationId(mer.getMiseEnRelationId());
			partage.evalueMiseEnRelation(evaluation);
		}
		{
			WAnnonceReponse reponse = new WAnnonceReponse();
			reponse.setAnnonceid(annonce.getAnnonceId());
			reponse.setCommentaire("je réponds!!2");
			reponse.setFedUserid("ausernam2");
			WMiseEnRelation mer = partage.lieAnnonce(reponse);
			WMiseEnRelationEvaluation evaluation = new WMiseEnRelationEvaluation();
			evaluation.setCommentaireEvaluation("commentaireEvaluation");
			evaluation.setFedUserId("ausernam2");
			evaluation.setEvaluation(4);
			evaluation.setMiseEnRelationId(mer.getMiseEnRelationId());
			partage.evalueMiseEnRelation(evaluation);
		}
		{
			WAnnonceReponse reponse = new WAnnonceReponse();
			reponse.setAnnonceid(annonce.getAnnonceId());
			reponse.setCommentaire("je réponds!!3");
			reponse.setFedUserid("ausernam3");
			WMiseEnRelation mer = partage.lieAnnonce(reponse);
			logger.debug("");
		}
		annonces = partage.listAnnonces();
		logger.debug("annonces:" + annonces);
		for(WAnnonce ann:annonces){
			if(annonce.getAnnonceId().equals(ann.getAnnonceId())){
				logger.debug("ann:"+ann);
				Assert.assertEquals(Integer.valueOf(3), ann.getNombrePartages());
				Assert.assertEquals(Double.valueOf(3.5), ann.getMoyenneEvaluations());
			}
		}
		logger.debug("");
	}

}
