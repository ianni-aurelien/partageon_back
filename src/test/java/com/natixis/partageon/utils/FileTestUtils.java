package com.natixis.partageon.utils;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;


/**
 * @author chaigneauam
 *
 */
public class FileTestUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(FileTestUtils.class);

	/**
	 * @param file
	 * @param strToCompare
	 */
	public static void assertEquals(String file, String strToCompare) {
		assertEquals(file, strToCompare, false);
	}

	/**
	 * @param file
	 * @param strToCompare
	 * @param filter
	 */
	public static void assertEquals(String file, String strToCompare, boolean filter) {
		try {
			String input=strToCompare;
//			String ref = FileUtils.getFileContent(file, true, "UTF-8");
			String ref = FileUtils.getFileContent(file, true, null);
			logger.debug("ref:\n"+ref);
			System.out.println("input:\n"+input);
			
			if(filter){
                if (ref != null) {
                    ref=ref.replaceAll("\\s+", "");
                }
                strToCompare=strToCompare.replaceAll("\\s+", "");
			}
			Assert.assertEquals(ref, strToCompare);
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

}
